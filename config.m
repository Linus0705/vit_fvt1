function config = config()

% Setup return structure
config = struct();

% Configuration
if ismac
    % Code to run on Mac platform
    config.PRJ = openProject(pwd + "\ViTFVT.prj");
    config.PRJ_PATH = config.PRJ.RootFolder;

    error('not yet implemented for OSX. Add your code here or use Windows or Linux!')

elseif isunix
    % Code to run on Linux platform -- HPC RWTH Cluster
    config.PRJ_PATH = pwd;

elseif ispc
    % Code to run on Windows platform

    try
        config.PRJ = currentProject;
    catch exception
        config.PRJ = openProject(pwd + "\ViTFVT.prj");
    end

    % Set Project Path
    config.PRJ_PATH = config.PRJ.RootFolder;
    config.PRJ_PATH = replace(config.PRJ_PATH, "\", "/");

else
    disp('Platform not supported')
end

% Set paths
config.SAFTSUITE_PATH = config.PRJ_PATH + "/saftsuite";
config.INPUT_PATH = config.PRJ_PATH + "/input";
config.OUTPUT_PATH = config.PRJ_PATH + "/output";
config.CONST_EQ_PATH = config.PRJ_PATH + "/constitutive-equations";
config.GEN_INPUT_PATH = config.PRJ_PATH + "/generate-input";
config.KINETIC_FIT_PATH = config.PRJ_PATH + "/kinetic-fit";
config.REGR_MDL_PATH = config.INPUT_PATH + "/regr-models";
config.HELPERFUNS_PATH = config.PRJ_PATH + "/helperfuns";
config.DEGASER_PATH = config.PRJ_PATH + "/degaser";

% Initialize SAFTSUITE
javaaddpath(config.SAFTSUITE_PATH + "/1_Tp-Flash/SAFTsuite_202012042120_r51.jar"); % adds .jar to dynamic javaclasspath
config.JAVA_SAFTSUITE.xml_name = config.SAFTSUITE_PATH + "/1_Tp-Flash/PCP-SAFT_2B_Schemes.xml";
config.JAVA_SAFTSUITE.eos = "PCP-SAFT";
config.JAVA_SAFTSUITE.path = config.SAFTSUITE_PATH + "/1_Tp-Flash/SAFTsuite_202012042120_r51.jar";

% Get PCP-SAFT instance
config.EOS = saftsuite.core.EOSRegistry.getInstance.getEOS(config.JAVA_SAFTSUITE.eos);

% Load PCP-SAFT project file
config.SAFTSUITE_PROJECTFILE = saftsuite.core.data.ProjectFile();
config.SAFTSUITE_PROJECTFILE.read(java.io.File(config.JAVA_SAFTSUITE.xml_name));

% Set up molecules
config.SAFTSUITE_MOLECULES = struct();
config.SAFTSUITE_MOLECULES.strname_h2 = "Ghosh_Chapman_2003_h2";
config.SAFTSUITE_MOLECULES.strname_co2 = "Khalifa_2018_co2_2B";
config.SAFTSUITE_MOLECULES.strname_oct = "Roman-Ramirez_2010_octanol_2B";
config.SAFTSUITE_MOLECULES.strname_h2o = "Rehner_2020_water_2B";
config.SAFTSUITE_MOLECULES.strname_meoh = "Khalifa_2018_MeOH_2B";
config.SAFTSUITE_MOLECULES.h2 = config.SAFTSUITE_PROJECTFILE.getComponent('h2').getMolecule(config.SAFTSUITE_MOLECULES.strname_h2);
config.SAFTSUITE_MOLECULES.co2 = config.SAFTSUITE_PROJECTFILE.getComponent('co2').getMolecule(config.SAFTSUITE_MOLECULES.strname_co2);
config.SAFTSUITE_MOLECULES.oct = config.SAFTSUITE_PROJECTFILE.getComponent('octanol').getMolecule(config.SAFTSUITE_MOLECULES.strname_oct);
config.SAFTSUITE_MOLECULES.h2o = config.SAFTSUITE_PROJECTFILE.getComponent('water').getMolecule(config.SAFTSUITE_MOLECULES.strname_h2o);
config.SAFTSUITE_MOLECULES.meoh = config.SAFTSUITE_PROJECTFILE.getComponent('methanol').getMolecule(config.SAFTSUITE_MOLECULES.strname_meoh);

% Add folders to search paths
addpath(genpath(config.SAFTSUITE_PATH));
addpath(genpath(config.INPUT_PATH));
addpath(genpath(config.OUTPUT_PATH));
addpath(genpath(config.CONST_EQ_PATH));
addpath(genpath(config.GEN_INPUT_PATH));
addpath(genpath(config.KINETIC_FIT_PATH));
addpath(genpath(config.REGR_MDL_PATH));
addpath(genpath(config.HELPERFUNS_PATH));
addpath(genpath(config.DEGASER_PATH));
