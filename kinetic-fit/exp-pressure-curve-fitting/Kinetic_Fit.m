clear all;
close all;
clc;

% %%one time only // delete header // if Octanol_H2O_Run1.txt is broken
% %changing , to .
% fid = fopen('Octanol_H2O_Run1.txt','r');
% rep=fread(fid,'*char')';
% fclose(fid);
% rep=strrep(rep,',','.');
% fid = fopen('Octanol_H2O_Run1.txt','w');
% fprintf(fid,'%s',rep);

%read file data
fid = fopen('Octanol_H2O_Run1.txt','r');
formatSpec = '%f %f';
sizeData = [2 Inf];
data = fscanf(fid,formatSpec,sizeData);
fclose(fid);


%preprocess data // measurements AFTER maximum
data=data';
dataOld=data;
[~,tmp] = max(data(:,2));
tmp=tmp+80;
%data=data(tmp:end,:);
datanew(:,1) = data(1:(end-(tmp-1)),1);
datanew(:,2) = data((tmp):end,2);
clear data;
data = datanew;
clear datanew;

% calculate bar to Pa
data(:,2) = data(:,2).*100000;
dataOld(:,2) = dataOld(:,2).*100000;

% %plot data
% fig = figure;
% plot(data(:,1),data(:,2));
% xlabel('time /s');
% ylabel('pressure/bar');
 
%fit to curve
fo = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',[6000000, 0, 0, -10,-100, -10000],...
    'Upper',[10000000, 100, 1, 50, 100, 500],...
    'StartPoint', [7500000, 40, 5e-05, 12, 0, 0],...
    'Display', 'final', 'TolFun', 1e-15, 'TolX', 1e-15);
% ft = fittype('exp2');   %//a*exp(b*x)+c*exp(d*x)
% %test1 // ft = fittype('a*(x+e)^3+b*(x+f)^2+c*(x+g)+d','options',fo);'SmoothingParam' , 0.5 , 
% %test2 // ft = fittype('a*(x+e)^2+b*(x+f)+c','options',fo);
ft = fittype('a+b*(exp(-(c*(x+e)) +d)+f)', 'dependent', {'y'}, 'independent',{'x'},...
    'coefficients',{'a','b', 'c', 'd', 'e', 'f'});
[curve1,gof1] = fit(data(3000:end,1),data(3000:end,2),ft, fo);
curve1
%create plottable data
xfit = linspace(-0.1e4,30e4);
xfit = xfit';
% yfit = curve1.a*exp(curve1.b*xfit(:)) + curve1.c*exp(curve1.d*xfit(:));
yfit = curve1.a+curve1.b*(exp(-(curve1.c*(xfit+curve1.e)) +curve1.d)+curve1.f);
a = 7.40e06;
b=40;
c=6e-5;
d=12;
y=a+b*exp(-(c*xfit) +d);

fo2 = fitoptions('Method','NonlinearLeastSquares',...
    'Display', 'final', 'TolFun', 1e-15, 'TolX', 1e-15);
ft2 = fittype('exp2');   %//a*exp(b*x)+c*exp(d*x)
% %test1 // ft = fittype('a*(x+e)^3+b*(x+f)^2+c*(x+g)+d','options',fo);'SmoothingParam' , 0.5 , 
% %test2 // ft = fittype('a*(x+e)^2+b*(x+f)+c','options',fo);
% ft = fittype('a+b*(exp(-(c*(x+e)) +d)+f)', 'dependent', {'y'}, 'independent',{'x'},...
%     'coefficients',{'a','b', 'c', 'd', 'e', 'f'});
[curve2,gof2] = fit(data(:,1),data(:,2),ft2, fo2);
% %test1 // yfit = curve1.a*(xfit(:)+curve1.e).^3+curve1.b*(xfit(:)+curve1.f).^2+curve1.c*(xfit(:)+curve1.g)+curve1.d;
% %test2 // yfit = curve1.a*(xfit(:)+curve1.e).^2+curve1.b*(xfit(:)+curve1.f)+curve1.c;
for i = 1:size(xfit)
    if xfit(i) < data(4000)
        y_comb(i,1) = curve2(xfit(i));
    else
        y_comb(i,1) = curve1(xfit(i));
    end
end

fo2 = fitoptions('Method','NonlinearLeastSquares',...
    'Display', 'final', 'TolFun', 1e-15, 'TolX', 1e-15);
ft2 = fittype('gauss8');   %//a*exp(b*x)+c*exp(d*x)
% %test1 // ft = fittype('a*(x+e)^3+b*(x+f)^2+c*(x+g)+d','options',fo);'SmoothingParam' , 0.5 , 
% %test2 // ft = fittype('a*(x+e)^2+b*(x+f)+c','options',fo);
% ft = fittype('a+b*(exp(-(c*(x+e)) +d)+f)', 'dependent', {'y'}, 'independent',{'x'},...
%     'coefficients',{'a','b', 'c', 'd', 'e', 'f'});
[curve3,gof3] = fit(xfit,y_comb,ft2, fo2);  


%plot fit vs. data
% fig = figure;
% plot(data(:,1),data(:,2),'r','DisplayName','data');
% hold on;
% plot(xfit,curve1(xfit),'b','DisplayName','exp');
% hold on;
% plot(xfit, curve2(xfit),'g','DisplayName','exp2');
% hold on;
% scatter(data(4000,1), data(4000,2));
% % plot(xfit,y,'g','DisplayName','test');
% xlabel('time /s');
% ylabel('pressure /Pa');
% legend;
fig = figure;
scatter(data(:,1),data(:,2),'r','DisplayName','data');
hold on;
plot(xfit,y_comb,'b');
plot(xfit,curve3(xfit), 'g');