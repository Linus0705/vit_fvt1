function funs = kinetic_fit_solver_funcs
  funs.dae_solve=@dae_solve;
  funs.rmse_pressure_fit=@rmse_pressure_fit;
end

function [p, T] = dae_solve(config,params,inputs,inits,dae, ...
        file_logger, df_sigma_T,nist_viscosity_gprMdls,K_gprMdls,rho_gprMdls, kinetic_fit)
    % ADDME  Starts dae_system.m
    %   
    % Inputs:
    %   config                : Configuration params
    %   params                : Overall model parameters
    %   inputs                : Input variables for CSTR model
    %   inits                 : Initial values for DAE-System solving
    %   dae                   : matlab script references
    %   file_logger           : Logger for logging
    %   df_sigma_T            : DataFrame with experimental interfacial tension values for Temperature
    %   nist_viscosity_gprMdls: Gaussian-Models for viscosity
    %   K_gprMdls             : Gaussian-Models for distribution coefficient
    %   rho_gprMdls           : Gaussian-Models for molar density
    %   kinetic_fit           : Structure with variables for kinetic fit
    %
    % Outputs:
    %   p       :   vector of pressure curve against t [Pa]
    %   t       :   vector of timesteps [sec]
    %

    % file_logger.info(['Starting dae_solve with k_fw: ' num2str(kinetic_fit.k_fw) ' and k_back: ' num2str(kinetic_fit.k_back)]);
    file_logger.info(['Starting dae_solve with k_fw: ' num2str(kinetic_fit.k_fw)]);


    % Initial values
    y0 = [inits.n_g_0
      inits.n_g_h2_0
      inits.n_aq_0
      inits.n_aq_h2_0
      inits.n_aq_co2_0
      inits.n_aq_oct_0
      inits.n_aq_meoh_0
      inits.n_o_0
      inits.n_o_h2_0
      inits.n_o_co2_0
      inits.n_o_h2o_0
      inits.n_o_meoh_0
      inits.p_0];

    % Mass Matrix describes lefthand side of dae system
    M = [1 0 0 0 0 0 0 0 0 0 0 0 0;... % n_g
        0 1 0 0 0 0 0 0 0 0 0 0 0; ... % n_h2
        0 0 1 0 0 0 0 0 0 0 0 0 0; ... % n_aq
        0 0 0 1 0 0 0 0 0 0 0 0 0; ... % n_aq_h2
        0 0 0 0 1 0 0 0 0 0 0 0 0; ... % n_aq_co2
        0 0 0 0 0 1 0 0 0 0 0 0 0; ... % n_aq_oct
        0 0 0 0 0 0 1 0 0 0 0 0 0; ... % n_aq_meoh
        0 0 0 0 0 0 0 1 0 0 0 0 0; ... % n_o
        0 0 0 0 0 0 0 0 1 0 0 0 0; ... % n_o_h2
        0 0 0 0 0 0 0 0 0 1 0 0 0; ... % n_o_co2
        0 0 0 0 0 0 0 0 0 0 1 0 0; ... % n_o_h2o
        0 0 0 0 0 0 0 0 0 0 0 1 0; ... % n_o_meoh
        0 0 0 0 0 0 0 0 0 0 0 0 0];    % p
    
    % Solver options
    opts = odeset('Mass',M); % 'InitialStep',1e-9, 'RelTol',1e-9,'AbsTol',1e-6);
%     tspan = [0 71000]; % Time of Schieweck (2020) Batch Experiment
    tspan = [0 120000];

    % Solver for DAE-System
    [T, Y] = ode23t(@(t,y)dae_system(t,y,config,params,inputs,inits,dae, ...
        file_logger, df_sigma_T,nist_viscosity_gprMdls,K_gprMdls,rho_gprMdls, kinetic_fit), tspan, y0, opts);
    
    % Return pressure curve against time
    p = Y(:,13);

    % file_logger.info(['... Finished dae_solve with k_fw: ' num2str(kinetic_fit.k_fw) ' and k_back: ' num2str(kinetic_fit.k_back)]);
    file_logger.info(['... Finished dae_solve with k_fw: ' num2str(kinetic_fit.k_fw)]);
end


function rmse = rmse_pressure_fit(k, pressure_curve_exp, config,params,inputs,inits,dae, ...
        file_logger, df_sigma_T,nist_viscosity_gprMdls,K_gprMdls,rho_gprMdls, kinetic_fit)

    % ADDME  Calculates RMSE between real pressure curve points and
    % numerically fitted pressure points using the current k_fw and k_back
    %   
    % Inputs:
    %   k                     : k_fw as forward reaction rate constant [m^3/(mol*s)] and k_back as backward reaction rate constant [m^3/(mol*s)]
    %   pressure_curve_exp    : Experimental pressure curve (already fitted) by Schieweck (2020)
    %   config                : Configuration params
    %   params                : Overall model parameters
    %   inputs                : Input variables for CSTR model
    %   inits                 : Initial values for DAE-System solving
    %   dae                   : matlab script references
    %   file_logger           : Logger for logging
    %   df_sigma_T            : DataFrame with experimental interfacial tension values for Temperature
    %   nist_viscosity_gprMdls: Gaussian-Models for viscosity
    %   K_gprMdls             : Gaussian-Models for distribution coefficient
    %   rho_gprMdls           : Gaussian-Models for molar density
    %   kinetic_fit           : Structure with variables for kinetic fit
    %
    % Outputs:
    %   p       :   vector of pressure curve against t [Pa]
    %   t       :   vector of timesteps [sec]
    %

    % Set independent variables
    kinetic_fit.k_fw = k(1);
    % kinetic_fit.k_back = k(2);

    % DAE-System needs K_R instead of k_back
    % kinetic_fit.K_R = kinetic_fit.k_fw / kinetic_fit.k_back;

    % solving DAE-system 
    [p, t] = dae_solve(config,params,inputs,inits,dae, ...
        file_logger, df_sigma_T,nist_viscosity_gprMdls,K_gprMdls,rho_gprMdls, kinetic_fit);
    p_exp = pressure_curve_exp(t);

    % RMSE (root mean squared error)
    % rmse_proper_calc = sqrt(mean((p_exp - p).^2)); % absolute
    rmse = sqrt(mean(((p_exp - p) ./ p_exp).^2)); % relative
    % disp('please stop');
end
