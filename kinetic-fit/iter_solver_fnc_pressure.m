function funs = iter_solver_fnc_pressure
  funs.delta_V=@delta_V;
end

function delta_V = delta_V(T, p, x_g_h2, x_g_co2, x_g_oct, x_g_h2o, ...
    x_g_meoh, x_aq_h2, x_aq_co2, x_aq_oct, x_aq_h2o, ...
    x_aq_meoh, x_o_h2, x_o_co2, x_o_oct, x_o_h2o, x_o_meoh, rho_gprMdls, ...
    n_g, n_aq, n_o, V_tot, dae)
    % ADDME  Computes deltaV = V_tot - (V_g + V_aq + V_o) for minimizing.
    %   
    % Inputs:
    %   T           :   temperature [K]
    %   p           :   pressure - optimizing parameter [Pa]
    %   rho_gprMdls :   Gauß-Models for molar density computation
    %   n_phase     :   mole of the phase [mol_phase]
    %   V_tot       :   total volume of CSTR [m^3]
    %   dae         :   structure with loaded scripts for dae_system.m
    %
    % Outputs:
    %   delta_V     :   Difference in volumes [m^3]
    %
    

    % Molar Density by constant pressure given in parameters [mol/m^3_phase]
    rho_M_g = dae.db.get_molar_density(T, p, ...
        x_g_h2, x_g_co2, x_g_oct, x_g_h2o, x_g_meoh, rho_gprMdls, state="vapor");
    rho_M_aq = dae.db.get_molar_density(T, p, ...
        x_aq_h2, x_aq_co2, x_aq_oct, x_aq_h2o, x_aq_meoh, rho_gprMdls, state="liquid_h2o");
    rho_M_o = dae.db.get_molar_density(T, p, ...
        x_o_h2, x_o_co2, x_o_oct, x_o_h2o, x_o_meoh, rho_gprMdls, state="liquid_oct");

    % Volume of the phases
    V_g = n_g / rho_M_g; % [m^3]
    V_aq = n_aq / rho_M_aq; % [m^3]
    V_o = n_o / rho_M_o; % [m^3]

    % Equation to be minimized
    delta_V = V_tot - (V_g + V_aq + V_o); % [m^3]
    delta_V = abs(delta_V);
end
