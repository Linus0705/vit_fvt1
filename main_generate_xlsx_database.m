close all;
clc;
clear;

% ADDME  Computes database for selected quantity.
%   
% Distribution Coefficient:
% generates distribution coefficients K using Tp-Flash (3Ph) SAFTSuite
% calculation and writes it in excel sheet 'distribution_coefficients_K.xlsx'
%
% works for Windows and Linux
% needs 'Optimization Toolbox' and 'DataFrame - TimeFrame'
% for 'Parallel Computing Toolbox' optimized, but not necessary
%
% Outputs:
%   'input\distribution_coefficients_K.xlsx'

% Import not official MATLAB packages (folders with a '+' sign)
import logging.*;

%number of variations of CSTR concentrations for K [datapoints=nCstrConc*90]
%nCstrConc=100;
nCstrConc=100;

% Set up Configuration
config = config();

%% Set up - Logging
% get the global LogManager
logManager = LogManager.getLogManager();
logManager.resetAll(); % Clean refresh, otherwise both loggers output on console

% add a console handler
consoleHandler = ConsoleHandler();
consoleHandler.setLevel(Level.INFO);
console_logger = Logger.getLogger('Console_Logger'); % creates a new logger object
console_logger.addHandler(consoleHandler);

% add a file handler to the root logger
fileHandler = FileHandler('./main_generate_xlsx_database.log');
fileHandler.setLevel(Level.INFO);
file_logger = Logger.getLogger('File_Logger');
file_logger.reset(); % delete RootConsoleHandler from our standard logger
file_logger.addHandler(fileHandler);

%logging
file_logger.info('Starting main_generate_xlsx_database.m ...');

%% User Input
% Get distribution coefficients or molar density or pressure
% run gen_K_distr_database(nCstrConc, config);
run gen_rho_database(nCstrConc, config, "liquid_oct");
%run gen_pressure_database(nCstrConc, config, "vapor");

%% Finalize
disp('Successfully finished!');
file_logger.info('... Finished main_generate_xlsx_database.m');