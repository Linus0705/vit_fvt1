function funs = degaser_Tpflash
    funs.Tpflash=@Tpflash;
end

%% Tp-flash
function flash_result = Tpflash(T, p, solver_result, molecules, eos, mole_fractions, V_decanter, V_degaser)
    
    % ADDME  Computes equilibrium fraction of gas and aqueous phase.
    %   
    % Inputs:
    %   T :                 degaser temperature [K]
    %   p :                 degaser pressure [Pa]
    %   solver_result :     ODE result
    %   molecules :         molecule definition PCP-Saft
    %   eos :               PCP-SAFT
    %   mole_fractions :    function to calculate mole fraction
    %
    % Outputs:
    %   x_eq :              mole fraction in vapor-liquid-equilibrium
    %   phi :               fraction of liquid/vapor phase
    %   flash_result :      moles of phase equilibrium
    %

    for i = 1:size(solver_result(:, 1))
        i
%         try
            % Time
            t = solver_result(i, 1);
    
            if t < 2
                t_degaser = 3000;
                decanter_t_dead = 1600;
            else
                % Residence time
                t_degaser = V_degaser/solver_result(i,19);
                decanter_t_dead = V_decanter/(solver_result(i,19)+solver_result(i,20));
            end
            
            % Moles of input stream
            n_aq = solver_result(i, 4);
            n_h2 = solver_result(i, 5);
            n_co2 = solver_result(i, 6);
            n_oct = solver_result(i, 7);
            n_meoh = solver_result(i, 8);
            n_h2o = n_aq - n_h2 - n_co2 - n_oct - n_meoh;
            N_dot_aq_out = solver_result(i, 15);
            V_dot_aq_out = solver_result(i, 19);
    
    
            % mole fraction
            x(1,1) = mole_fractions.mole_fraction(n_h2, n_aq);
            if x(1,1) == 0
                x(1,1) = 0.00001;
            elseif x(1,1) == 1
                x(1,1) = 0.999;
            end
            x(2,1) = mole_fractions.mole_fraction(n_co2, n_aq);
            if x(2,1) == 0
                x(2,1) = 0.00001;
            elseif x(2,1) == 1
                x(2,1) = 0.999;
            end
            x(3,1) = mole_fractions.mole_fraction(n_oct, n_aq);
            if x(3,1) == 0
                x(3,1) = 0.00001;
            elseif x(3,1) == 1
                x(3,1) = 0.999;
            end
            x(4,1) = mole_fractions.mole_fraction(n_h2o, n_aq);
            if x(4,1) == 0
                x(4,1) = 0.00001;
            elseif x(4,1) == 1
                x(4,1) = 0.999;
            end
            x(5,1) = mole_fractions.mole_fraction(n_meoh, n_aq);
            if x(5,1) == 0
                x(5,1) = 0.00001;
            elseif x(5,1) == 1
                x(5,1) = 0.999;
            end
            
            % Definition of initial composition
            frac(1,1) = 0.00001;
            frac(2,1) = 0.00001;
            frac(3,1) = 1 - x(4,1) - x(5,1);
            frac(4,1) = x(4,1);
            frac(5,1) = x(5,1);
            frac(1,2) = 0.5;
            frac(2,2) = 0.5;
            frac(3,2) = 0.0001;
            frac(4,2) = 0.0001;
            frac(5,2) = 0.0001;
        
            % initial composition
            x_start = create_comp(frac, molecules);
    
            % Tp-flash
            [x_eq, phi] = flashcalc(T, p, x_start, molecules, eos, x);
         
    
            % mole outflow
            n_aq_res = phi(1) * N_dot_aq_out;
            n_g_res = phi(2) * N_dot_aq_out;
            n_h2_aq = x_eq(1, 1) * n_aq_res;
            n_co2_aq = x_eq(2, 1) * n_aq_res;
            n_oct_aq = x_eq(3, 1) * n_aq_res;
            n_h2o_aq = x_eq(4, 1) * n_aq_res;
            n_meoh_aq = x_eq(5, 1) * n_aq_res;
            n_h2_g = x_eq(1, 2) * n_g_res;
            n_co2_g = x_eq(2, 2) * n_g_res;
            n_oct_g = x_eq(3, 2) * n_g_res;
            n_h2o_g = x_eq(4, 2) * n_g_res;
            n_meoh_g = x_eq(5, 2) * n_g_res;
            
            t_out = t + decanter_t_dead + t_degaser;
    
            flash_result(i, :) = [t_out,...
                x_eq(:,1).', ...
                x_eq(:,2).', ...
                n_aq_res, ...
                n_g_res, ...
                n_h2_aq, ...
                n_co2_aq, ...
                n_oct_aq, ...
                n_h2o_aq, ...
                n_meoh_aq, ...
                n_h2_g, ...
                n_co2_g, ...
                n_oct_g, ...
                n_h2o_g, ...
                n_meoh_g, ...
                phi(1), ...
                phi(2), ...
                ];
    
        end
%     catch
%         continue;
%     end
end

%% Tp-flash
function [x_eq, phi] = flashcalc(T, p, x_start, molecules, eos, x)
    
    % ADDME  Tp-flash calculation
    %   
    % Inputs:
    %   T :                 degaser temperature [K]
    %   p :                 degaser pressure [Pa]
    %   x_start :           initial values for solver
    %   molecules :         molecule definition PCP-Saft
    %   eos :               PCP-SAFT
    %   x :                 mole fraction oft input flow
    %
    % Outputs:
    %   x_eq :              mole fraction in vapor-liquid-equilibrium
    %   phi :               fraction of liquid/vapor phase
    %
    
    % create input composition
    z = create_comp(x, molecules);
    
    % Tp-flash
    [x_java, flag, phi] = flashTp(T, p, z(1), eos, x_start);
    
    for i = 1:2
        x_eq(1,i) = x_java(i).get(molecules.h2);
        x_eq(2,i) = x_java(i).get(molecules.co2);
        x_eq(3,i) = x_java(i).get(molecules.oct);
        x_eq(4,i) = x_java(i).get(molecules.h2o);
        x_eq(5,i) = x_java(i).get(molecules.meoh);
    end
    phi(1) = phi(1);
    phi(2) = 1- phi(1);

end

%% create composition
function comp = create_comp(frac, molecules)

    % ADDME  creates composition 
    %   
    % Inputs:
    %   frac :              mole fraction of composition
    %   molecules :         molecule definition PCP-Saft
    %
    % Outputs:
    %   comp :              SAFT-composition
    %

    for i = 1:size(frac,2)
    
        comp(i) = saftsuite.core.Composition();
        comp(i).put(molecules.h2,frac(1,i));
        comp(i).put(molecules.co2,frac(2,i));
        comp(i).put(molecules.oct,frac(3,i));
        comp(i).put(molecules.h2o,frac(4,i));
        comp(i).put(molecules.meoh,frac(5,i));
        comp(i).normalize;
    end
end
