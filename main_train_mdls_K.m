clear;
clc;
close all;

%% initialize

% Import not official MATLAB packages (folders with a '+' sign)
import logging.*;
import frames.*;

% load config
config = config();

%% Set up - Logging
% get the global LogManager
logManager = LogManager.getLogManager();
logManager.resetAll(); % Clean refresh, otherwise both loggers output on console

% add a console handler
consoleHandler = ConsoleHandler();
consoleHandler.setLevel(Level.FINE);
console_logger = Logger.getLogger('Console_Logger'); % creates a new logger object
console_logger.addHandler(consoleHandler);

% add a file handler to the root logger
fileHandler = FileHandler('./main_train_mdls.log');
fileHandler.setLevel(Level.FINE);
file_logger = Logger.getLogger('File_Logger');
file_logger.reset(); % delete RootConsoleHandler from our standard logger
file_logger.addHandler(fileHandler);

console_logger.info('Starting main_train_mdls_K.m ...');


%% Setup
% load scripts
db = database;

% get dataframes
[df_K_wo_h2, df_K_wo_co2, df_K_wo_oct, df_K_wo_h2o, ...
    df_K_wo_meoh, df_K_gw_h2, df_K_gw_co2, df_K_gw_oct, ...
    df_K_gw_h2o, df_K_gw_meoh] = db.get_df_K_wo_gw(config.PRJ_PATH);

% Create loopable arrays
struct_dfs = struct();
struct_dfs.df_K_wo_h2 = df_K_wo_h2;
struct_dfs.df_K_wo_co2 = df_K_wo_co2;
struct_dfs.df_K_wo_oct = df_K_wo_oct;
struct_dfs.df_K_wo_h2o = df_K_wo_h2o;
struct_dfs.df_K_wo_meoh = df_K_wo_meoh;
struct_dfs.df_K_gw_h2 = df_K_gw_h2;
struct_dfs.df_K_gw_co2 = df_K_gw_co2;
struct_dfs.df_K_gw_oct = df_K_gw_oct;
struct_dfs.df_K_gw_h2o = df_K_gw_h2o;
struct_dfs.df_K_gw_meoh = df_K_gw_meoh;
struct_dfs_names = fieldnames(struct_dfs);

col_names =  ["K_wo_h2 [c_org/c_water]", ...
            "K_wo_co2 [c_org/c_water]", ...
            "K_wo_oct [c_org/c_water]", ...
            "K_wo_h2o [c_org/c_water]", ...
            "K_wo_meoh [c_org/c_water]"  ...
            "K_gw_h2 [c_gas/c_water]", ...
            "K_gw_co2 [c_gas/c_water]", ...
            "K_gw_oct [c_gas/c_water]", ...
            "K_gw_h2o [c_gas/c_water]", ...
            "K_gw_meoh [c_gas/c_water]"];


n = 10;  %folds of crossvalidation

%% Main-Loop
console_logger.info('Starting main-loop ...');
for df_i = 1:(numel(struct_dfs_names)-3) % Just relevant K's
    
    df = struct_dfs.(struct_dfs_names{df_i});
    str = col_names(df_i);

    console_logger.info("Training for " + str);

    %% crossvalidate gpr (one at a time)
    idx=randperm(size(df.t,1))';
    df=df(idx);
    df.rows=1:size(df.t,1);
    
    % calculate splitrule for dataset
    split = size(df,1)/n;
    section(1)=0;
    for i=1:n
        section(i+1) = [round(i*split)];
    end
    
    % Train model on complete data
    console_logger.info("Train complete model ...");
    complete_mdl = fitrgp(df.t, str, ...
    'KernelFunction','ardsquaredexponential',...
    'FitMethod','sr','PredictMethod','fic','Standardize',1);
    save(config.INPUT_PATH +"/regr-models-K/gprMdl_"+extractBefore(str," ")+"_complete"+".mat", "complete_mdl")
    console_logger.info("... Trained complete model");
    

    %% evaluate processes
    console_logger.info("Starting inner for-loop ...");
    for i=1:n
        % split dataset
        df_train = df(setdiff(1:end,(section(i)+1):section(i+1)));
        df_test = df((section(i)+1):section(i+1));
        % train gaussian processes
        tmp = fitrgp(df_train.t, str, ...
            'KernelFunction','ardsquaredexponential',...
            'FitMethod','sr','PredictMethod','fic','Standardize',1);
        
        % predict testset
        clear y_pred_test;
        y_pred_test((section(i)+1):section(i+1),1) = predict(tmp,df_test.t(:,1:7));
    
        %predict
        clear y_pred_train;
        y_pred_train(:,1) = predict(tmp,df_train.t(:,1:7));
    
        % calculate RMSE for each fold
        rmse_test(i,1) = sum((y_pred_test((section(i)+1):end)-table2array(df_test.t(:,8))).^2,'all')/size(df_test,1);
        rmse_train(i,1) = sum((y_pred_train-table2array(df_train.t(:,8))).^2,'all')/size(df_train,1);
    
        %parity plot
        fig(i) = figure;
        scatter(df_train.data(:,8),y_pred_train,'x');
        hold on;
        scatter(df_test.data(:,8),y_pred_test((section(i)+1):section(i+1),1),'x');
    
        % save model
        save(config.INPUT_PATH +"/regr-models-K/gprMdl_rmse_test_"+extractBefore(str," ")+"_"+i+".mat","rmse_test");
        save(config.INPUT_PATH +"/regr-models-K/gprMdl_rmse_train_"+extractBefore(str," ")+"_"+i+".mat","rmse_train");

        save(config.INPUT_PATH +"/regr-models-K/gprMdl_y_pred_test_"+extractBefore(str," ")+"_"+i+".mat","y_pred_test");
        save(config.INPUT_PATH +"/regr-models-K/gprMdl_y_pred_train_"+extractBefore(str," ")+"_"+i+".mat","y_pred_train");

        save(config.INPUT_PATH +"/regr-models-K/gprMdl_"+extractBefore(str," ")+"_"+i+".mat", 'tmp')
        writetable(df_train.t, config.INPUT_PATH +"/regr-models-K/gprMdl_dataFrame" + ".xlsx", 'Sheet', 'df_train');
        writetable(df_test.t, config.INPUT_PATH +"/regr-models-K/gprMdl_dataFrame" + ".xlsx", 'Sheet', 'df_test');


        disp(i+"/"+n)
    end
    console_logger.info("... Finished inner for-loop");
    
    %% save data and finalize
    % save best model to another folder
    worstMdl = find(max(rmse_test)==rmse_test);
    tmp = load(config.INPUT_PATH + "/regr-models-K/gprMdl_" + extractBefore(str," ") + "_" + worstMdl + ".mat");
    movefile(config.INPUT_PATH + "/regr-models-K/gprMdl_" + extractBefore(str," ") + "_" + worstMdl + ".mat", config.INPUT_PATH + "/regr-models-K/worst-models");
    exportgraphics(fig(worstMdl), config.INPUT_PATH + "/regr-models-K/worst-models/plot" + extractBefore(str," ") + "_" + worstMdl + ".jpg");
    
    % print parity plot data
    df_train = df(setdiff(1:end,(section(worstMdl)+1):section(worstMdl+1)));
    df_test = df((section(worstMdl)+1):section(worstMdl+1));
    
    % predict testset
    clear y_pred_test;
    y_pred_test((section(worstMdl)+1):section(worstMdl+1),1) = predict(tmp.tmp,df_test.t(:,1:7));
    
    %predict
    clear y_pred_train;
    y_pred_train(:,1) = predict(tmp.tmp,df_train.t(:,1:7));
end
console_logger.info('... Finished main-loop');
console_logger.info('... Finished main_train_mdls_K.m');
