clear;
clc;
close all;

%% Load Configurations (If error --> Please run config.m manually beforehand)
% load config
config = config();

%% User Input
phase = "liquid_oct"; % String 'vapor', 'liquid_h2o', 'liquid_oct'
folds = 10; % folds of crossvalidation


%% Initialize
% load scripts
db = database;

% get dataframes
df = db.get_df_rho(config.PRJ_PATH, phase);

% Set parameters depending on selected phase by user
n = folds;
if phase=="vapor"
    str = "rho_vap [mol/m^3_phase]";
elseif phase=="liquid_h2o" || phase=="liquid_oct"
    str = "rho_liq [mol/m^3_phase]";
end


%% crossvalidate gpr (one at a time)
idx=randperm(size(df.t,1))';
df=df(idx);
df.rows=1:size(df.t,1);

% calculate splitrule for dataset
split = size(df,1)/n;
section(1)=0;
for i=1:n
    section(i+1) = [round(i*split)];
end

disp("Train complete model ...")
complete_mdl = fitrgp(df.t, str, ...
'KernelFunction','ardsquaredexponential',...
'FitMethod','sr','PredictMethod','fic','Standardize',1);
save(config.INPUT_PATH +"/regr-models-rho/gprMdl_"+"rho"+phase+"_complete"+".mat", "complete_mdl")
disp("... Trained complete model")

%% evaluate processes
for i=1:n
    % split dataset
    df_train = df(setdiff(1:end,(section(i)+1):section(i+1)));
    df_test = df((section(i)+1):section(i+1));
    % train gaussian processes
    tmp = fitrgp(df_train.t, str, ...
        'KernelFunction','ardsquaredexponential',...
        'FitMethod','sr','PredictMethod','fic','Standardize',1);
    
    % predict testset
    clear y_pred_test;
    y_pred_test((section(i)+1):section(i+1),1) = predict(tmp,df_test.t(:,1:7));

    %predict
    clear y_pred_train;
    y_pred_train(:,1) = predict(tmp,df_train.t(:,1:7));

    % calculate RMSE for each fold
    rmse_test(i,1) = sum((y_pred_test((section(i)+1):end)-table2array(df_test.t(:,8))).^2,'all')/size(df_test,1);
    rmse_train(i,1) = sum((y_pred_train-table2array(df_train.t(:,8))).^2,'all')/size(df_train,1);

    %pairity plot
    fig(i) = figure;
    scatter(df_train.data(:,8),y_pred_train,'x');
    hold on;
    scatter(df_test.data(:,8),y_pred_test((section(i)+1):section(i+1),1),'x');

    % save model
    save(config.INPUT_PATH +"/regr-models-rho/gprMdl_"+ extractBefore(str," ") + "_"  + i + phase + ".mat", 'tmp')
    
    disp(i+"/"+n)
end

%% save data and finalize
% save best model to another folder
bestMdl = find(max(rmse_test)==rmse_test);
tmp = load(config.INPUT_PATH + "/regr-models-rho/gprMdl_" + extractBefore(str," ") + "_" + bestMdl + phase + ".mat");
movefile(config.INPUT_PATH + "/regr-models-rho/gprMdl_" + extractBefore(str," ") + "_" + bestMdl + phase + ".mat", config.INPUT_PATH + "/regr-models-rho/worst-models");
exportgraphics(fig(bestMdl), config.INPUT_PATH + "/regr-models-rho/worst-models/plot_" + extractBefore(str," ") + "_" + bestMdl + phase + ".jpg");

% print parity plot data
df_train = df(setdiff(1:end,(section(bestMdl)+1):section(bestMdl+1)));
df_test = df((section(bestMdl)+1):section(bestMdl+1));

% predict testset
clear y_pred_test;
y_pred_test((section(bestMdl)+1):section(bestMdl+1),1) = predict(tmp.tmp,df_test.t(:,1:7));

%predict
clear y_pred_train;
y_pred_train(:,1) = predict(tmp.tmp,df_train.t(:,1:7));

disp("Successfully finished!")
