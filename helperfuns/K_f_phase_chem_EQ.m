%% Set up - General
% Clear workspace and command window
clear all;
clc;

%% load workspace and initialize PCP-Saft
% load base workspace
config = config();
load base_workspace.mat;
import mole_fractions.*;
import flashTp3Ph.*;
mole_fractions = mole_fractions;


%% initial settings
% temperature
T0 = 298.15;    % K
T = 393.15;     % K

% pressure
p = 3000000;    % Pa
p0 = 100000;    % Pa



% strname_h2 = "Ghosh_Chapman_2003_h2";
% strname_co2 = "Khalifa_2018_co2_2B";
% strname_oct = "Roman-Ramirez_2010_octanol_2B";
% strname_h2o = "Rehner_2020_water_2B";
% strname_meoh = "Khalifa_2018_MeOH_2B";
% molecules = struct();
% molecules.h2 = projectfile.getComponent('h2').getMolecule(strname_h2);
% molecules.co2 = projectfile.getComponent('co2').getMolecule(strname_co2);
% molecules.oct = projectfile.getComponent('octanol').getMolecule(strname_oct);
% molecules.h2o = projectfile.getComponent('water').getMolecule(strname_h2o);
% molecules.meoh = projectfile.getComponent('methanol').getMolecule(strname_meoh);

%% initial moles and mole fractions 
% inits = struct();
% 
% % Moles at t0
% inits.n_g_0 = 0.02882;                      % [mol] % 0.028028 for 149 bar
% inits.n_g_h2_0 = inits.n_g_0 * 3/4;          % [mol]
% inits.n_g_co2_0 = ...
%     inits.n_g_0 - inits.n_g_h2_0;            % [mol]
% inits.n_aq_0 = 0.11;                         % [mol] Order of Magnitude by Schieweck (2020)
% inits.n_aq_h2_0 = 0;                         % [mol]
% inits.n_aq_co2_0 = 0;                        % [mol]
% inits.n_aq_oct_0 = 0.0023 * inits.n_aq_0;    % [mol] Consider saturation of phases before experimental start
% inits.n_aq_meoh_0 = 0;                       % [mol]
% inits.n_aq_h2o_0 = inits.n_aq_0 - ...
%     (inits.n_aq_h2_0 + inits.n_aq_co2_0 + ...
%     inits.n_aq_oct_0 + inits.n_aq_meoh_0);   % [mol]
% inits.n_o_0 = 0.0126;                        % [mol] Order of Magnitude by Schieweck (2020)
% inits.n_o_h2_0 = 0;                          % [mol]
% inits.n_o_co2_0 = 0;                         % [mol]
% inits.n_o_h2o_0 = 0.22218 * inits.n_o_0;     % [mol] Consider saturation of phases before experimental start
% inits.n_o_meoh_0 = 0;                        % [mol]
% inits.n_o_oct_0 = inits.n_o_0 - ...
%     (inits.n_o_h2_0 + inits.n_o_co2_0 + ...
%     inits.n_o_h2o_0 + inits.n_o_meoh_0);  % [mol]
% inits.n_ges = inits.n_o_0 + inits.n_g_0 + inits.n_aq_0;
% inits.n_h2_ges = inits.n_g_h2_0 + inits.n_aq_h2_0 + inits.n_o_h2_0;
% inits.n_h2o_ges = inits.n_aq_h2o_0 + inits.n_o_h2o_0;
% inits.n_co2_ges = inits.n_g_co2_0 + inits.n_aq_co2_0 + inits.n_o_co2_0;
% inits.n_oct_ges = inits.n_aq_oct_0 + inits.n_o_oct_0;
% inits.n_meoh_ges = inits.n_aq_meoh_0 + inits.n_o_meoh_0;

% Mole Fractions
% init_frac = struct();
% init_frac.x_o = mole_fractions.mole_fraction(inits.n_o_0, inits.n_ges);
% init_frac.x_aq = mole_fractions.mole_fraction(inits.n_aq_0, inits.n_ges);
% init_frac.x_g = mole_fractions.mole_fraction(inits.n_g_0, inits.n_ges);
% init_frac.x_h2 = mole_fractions.mole_fraction(inits.n_h2_ges, inits.n_ges);
% init_frac.x_h2o = mole_fractions.mole_fraction(inits.n_h2o_ges, inits.n_ges);
% init_frac.x_co2 = mole_fractions.mole_fraction(inits.n_co2_ges, inits.n_ges);
% init_frac.x_oct = mole_fractions.mole_fraction(inits.n_oct_ges, inits.n_ges);
% init_frac.x_meoh = mole_fractions.mole_fraction(inits.n_meoh_ges, inits.n_ges);
% init_frac.x_g_h2 = mole_fractions.mole_fraction(inits.n_g_h2_0, inits.n_g_0);
% init_frac.x_g_co2 = mole_fractions.mole_fraction(inits.n_g_co2_0, inits.n_g_0);
% init_frac.x_g_oct = 0; % no octanol in gas phase
% init_frac.x_g_h2o = 0; % no water in gas phase
% init_frac.x_g_meoh = 0; % no MeOH in gas phase
% init_frac.x_aq_h2 = mole_fractions.mole_fraction(inits.n_aq_h2_0, inits.n_aq_0);
% init_frac.x_aq_co2 = mole_fractions.mole_fraction(inits.n_aq_co2_0, inits.n_aq_0);
% init_frac.x_aq_oct = mole_fractions.mole_fraction(inits.n_aq_oct_0, inits.n_aq_0);
% init_frac.x_aq_h2o = mole_fractions.mole_fraction(inits.n_aq_h2o_0, inits.n_aq_0);
% init_frac.x_aq_meoh = mole_fractions.mole_fraction(inits.n_aq_meoh_0, inits.n_aq_0);
% init_frac.x_o_h2 = mole_fractions.mole_fraction(inits.n_o_h2_0, inits.n_o_0);
% init_frac.x_o_co2 = mole_fractions.mole_fraction(inits.n_o_co2_0, inits.n_o_0);
% init_frac.x_o_oct = mole_fractions.mole_fraction(inits.n_o_oct_0, inits.n_o_0);
% init_frac.x_o_h2o = mole_fractions.mole_fraction(inits.n_o_h2o_0, inits.n_o_0);
% init_frac.x_o_meoh = mole_fractions.mole_fraction(inits.n_o_meoh_0, inits.n_o_0);

%% create random bounded feed composition
% create random matrix
rn = rand(10000,5);

% define boundary
lb = [0    0    0.05 0.75 0   ];
ub = [0.15 0.1  0.15 0.9  0.05];
db = ub-lb;

% get random values in boundary
rn= lb+db.*rn;

% check sum of mole fractions
csum = sum(rn,2);

% kill rowsums below 0.95 and above 1.05
[hit,~] = find(csum>0.95 & csum<1.05);
rn=rn(hit,:);
csum = sum(rn,2);
size(rn,1);

% normalize mole fraction to rowsum of 1
rn= rn./csum;

x = [0.1 0.1 0.19 0.6 0.01]; % H2 in mol/mol

frac(1,3) = 0.5;
frac(2,3) = 0.5;
frac(3,3) = 0.0001; % no octanol in gas phase
frac(4,3) = 0.0001; % no water in gas phase
frac(5,3) = 0.0001; % no MeOH in gas phase
frac(1,1) = 0.00001;
frac(2,1) = 0.0004;
frac(3,1) = 0.0004;
frac(4,1) = 0.999;
frac(5,1) = 0.0001;
frac(1,2) = 0.004;
frac(2,2) = 0.08;
frac(3,2) = 0.8;
frac(4,2) = 0.0004;
frac(5,2) = 0.2;
frac

%% chemical equilibrium with Gibbs Energy

% stoichometric coefficients
theta(1)= -1;       % carbon dioxide
theta(2)= -3;       % hydrogen
theta(3)= 1;        % methanol
theta(4)= 1;        % water

% Coefficients for DIPPR 107 to calculate IG heat capacity 
% -> DIPPR database DIADEM
C = [29370 34540 1428 26400 588;...             % carbon dioxide
     27617 9560 2466 3760 567.6;...             % hydrogen
     39252 87900 1916.5 53654 896.7;...         % methanol
     33363 26790 2610.5 8896 1169];             % water

% IG-standard enthalpy and absolute entropy at 298.15K, 1bar. Source:
% DIPPR-Database (CODATA Task Group [Cox, Wagman, et al., 1984] + NIST
% database
H0 = [-393.51e3 0 -200.94e3 -241.818e3];      %J/mol
S0 = [213.677 130.571 239.88 188.825];        %J/mol K

% Calculation of Gibbs Energy for every component
for i = 1:size(theta,2)
%numerical integration
% f_H = @(a) (C(i,1)+ C(i,2)*(C(i,3)/a/sinh(C(i,3)/a))^2+C(i,4)*(C(i,5)/a/cosh(C(i,5)/a))^2)*10^(-3); %J/molK
% f_S = @(b) (C(i,1)+ C(i,2)*(C(i,3)/b/sinh(C(i,3)/b))^2+C(i,4)*(C(i,5)/b/cosh(C(i,5)/b))^2)*10^(-3)/b; %J/molK^2
% delta_Gr(i) = theta(i)*(integral(f_H,T0,T,'ArrayValued',true)-T*(integral(f_S,T0,T,'ArrayValued',true))...
%     +H0(i)-T*S0(i));

%analytic integration
integral_1 = (C(i,1)*T + C(i,2)*C(i,3)*coth(C(i,3)/T)-C(i,4)*C(i,5)*tanh(C(i,5)/T)...
    - (C(i,1)*T0 + C(i,2)*C(i,3)*coth(C(i,3)/T0)-C(i,4)*C(i,5)*tanh(C(i,5)/T0)))*10^(-3);
integral_2 = (C(i,1)*log(T) + C(i,2)*C(i,3)*coth(C(i,3)/T)/T-C(i,2)*log(sinh(C(i,3)/T))-C(i,4)*C(i,5)*tanh(C(i,5)/T)/T+C(i,4)*log(cosh(C(i,5)/T))...
    - (C(i,1)*log(T0) + C(i,2)*C(i,3)*coth(C(i,3)/T0)/T0-C(i,2)*log(sinh(C(i,3)/T0))-C(i,4)*C(i,5)*tanh(C(i,5)/T0)/T0+C(i,4)*log(cosh(C(i,5)/T0))))*10^(-3);
delta_Gr(i) = theta(i)* (integral_1 - T * integral_2 + H0(i) - T * S0(i));
end

% Calculation of chemical equilibrium
K_f_gibbs = exp(-sum(delta_Gr)/8.314/T);

%% solver for fitting K_c
% initial K_c
K_c_0 = 0.5;
lsqopt = optimoptions(@lsqnonlin,'Display', 'iter', 'FunctionTolerance', 1e-9, 'StepTolerance', 1e-9);%, 'FunctionTolerance', 1e-20, 'StepTolerance', min(x0)*1e-6);
% lsq nonlinear solver
K_c_neu = lsqnonlin(@(K_c)calc_K_f(K_c, T, p, frac, config.SAFTSUITE_MOLECULES, config.EOS, x, K_f_gibbs, p0),K_c_0,[0.001],[10], lsqopt);
K_c_neu
% K_f_diff = calc_K_f(K_c_neu, T, p, frac, molecules, config.EOS, x, K_f_gibbs, p0)

%% calculation 
function K_f_diff = calc_K_f(K_c, T, p, frac, molecules, eos, x, K_f_gibbs, p0)
    K_c
    x_eq = calc_iter(T, p, K_c, frac, molecules, eos, x);
    frac = x_eq;
    %nonpolar phase (=light phase) - Octanol-Phase (Reaktionphase)
    x_o = saftsuite.core.Composition();
    x_o.put(molecules.h2,frac(1,2));
    x_o.put(molecules.co2,frac(2,2));
    x_o.put(molecules.oct,frac(3,2));
    x_o.put(molecules.h2o,frac(4,2));
    x_o.put(molecules.meoh,frac(5,2));
    x_o.normalize;

    rho_m_diff = eos.rhoLiquid(T, p, x_o);
    f_h2 = eos.f(T, rho_m_diff, x_o, molecules.h2);
    f_co2 = eos.f(T, rho_m_diff, x_o, molecules.co2);
    f_meoh = eos.f(T, rho_m_diff, x_o, molecules.meoh);
    f_h2o = eos.f(T, rho_m_diff, x_o, molecules.h2o);

    K_f_pcpsaft = (f_meoh/p0)^(1)*(f_h2o/p0)^(1)* (f_co2/p0)^(-1) * (f_h2/p0)^(-3)
    
    K_f_diff = abs((K_f_pcpsaft - K_f_gibbs)/K_f_gibbs);
end
    


function x_eq = calc_iter(T, p, K_c, frac, molecules, eos, x)

    frac_old = frac;
    dx = 1;
    x_eq = frac;
    while dx > 0.001
        
        [x_eq, phi] = flashcalc(T, p, x_eq, molecules, eos, x);
        x_eq
        [x_eq, x] = reaction_eq(T, p, K_c, x_eq, molecules, eos, phi);
        x_eq
%         abs(x_eq - frac_old)
        dx = max(max(abs(x_eq - frac_old)));
        dx
        frac_old = x_eq;
    end
end

function [x_reaction_eq, x] = reaction_eq(T, p, K_c, frac, molecules, eos, phi)
    
    %polar phase organic Phase
    x1 = saftsuite.core.Composition();
    x1.put(molecules.h2,frac(1,2));
    x1.put(molecules.co2,frac(2,2));
    x1.put(molecules.oct,frac(3,2));
    x1.put(molecules.h2o,frac(4,2));
    x1.put(molecules.meoh,frac(5,2));
    x1.normalize;

    rho_m = eos.rhoLiquid(T, p, x1);

    c(1) = frac(1,2) * rho_m;
    c(2) = frac(2,2) * rho_m;
%     c(3) = frac(3,2) * rho_m;
    c(4) = frac(4,2) * rho_m;
    c(5) = frac(5,2) * rho_m;
    
    syms n;
    eqn_K = ((c(5)+n) * (c(4) + n))/((c(1) - 3*n)^3 * (c(2) - n)) == K_c;
    n = solve(eqn_K, n);
    
    x_reaction_eq = frac;
    x_reaction_eq(1,2) = (c(1) - 3*n(1))/ rho_m;
    x_reaction_eq(2,2) = (c(2) - n(1))/ rho_m;
%     x_reaction_eq(1,2) = (c(1) - 3*n)/ rho_m;
    x_reaction_eq(4,2) = (c(4) + n(1))/ rho_m;
    x_reaction_eq(5,2) = (c(5) + n(1))/ rho_m;
    x_reaction_eq(3,2) = 1 - x_reaction_eq(1,2) - x_reaction_eq(2,2) - x_reaction_eq(4,2) - x_reaction_eq(5,2);
    
    x(1) = x_reaction_eq(1,1)*phi(1) + x_reaction_eq(1,2)*phi(2) + x_reaction_eq(1,1)*phi(3);
    x(2) = x_reaction_eq(2,1)*phi(1) + x_reaction_eq(2,2)*phi(2) + x_reaction_eq(2,1)*phi(3);
    x(3) = x_reaction_eq(3,1)*phi(1) + x_reaction_eq(3,2)*phi(2) + x_reaction_eq(3,1)*phi(3);
    x(4) = x_reaction_eq(4,1)*phi(1) + x_reaction_eq(4,2)*phi(2) + x_reaction_eq(4,1)*phi(3);
    x(5) = x_reaction_eq(5,1)*phi(1) + x_reaction_eq(5,2)*phi(2) + x_reaction_eq(5,1)*phi(3);
    

end


function [x_eq, phi] = flashcalc(T, p, frac, molecules, eos, x)

    %% Definition of Feed
    z = saftsuite.core.Composition();
%     
%     z.put(molecules.h2,frac.x_h2);
%     z.put(molecules.co2,frac.x_co2);
%     z.put(molecules.oct,frac.x_oct);  
%     z.put(molecules.h2o,frac.x_h2o);
%     z.put(molecules.meoh,frac.x_meoh);
%     z.normalize;

    z.put(molecules.h2,x(1));
    z.put(molecules.co2,x(2));
    z.put(molecules.oct,x(3));  
    z.put(molecules.h2o,x(4));
    z.put(molecules.meoh,x(5));
    z.normalize;
    
    %% Definition of initial values
    
    %polar phase (=heavy phase) - Water-Phase
    xstart(1) = saftsuite.core.Composition();
    xstart(1).put(molecules.h2,frac(1,1));
    xstart(1).put(molecules.co2,frac(2,1));
    xstart(1).put(molecules.oct,frac(3,1));
    xstart(1).put(molecules.h2o,frac(4,1));
    xstart(1).put(molecules.meoh,frac(5,1));
    xstart(1).normalize;
    
    %nonpolar phase (=light phase) - Octanol-Phase (Reaktionphase)
    xstart(2) = saftsuite.core.Composition();
    xstart(2).put(molecules.h2,frac(1,2));
    xstart(2).put(molecules.co2,frac(2,2));
    xstart(2).put(molecules.oct,frac(3,2));
    xstart(2).put(molecules.h2o,frac(4,2));
    xstart(2).put(molecules.meoh,frac(5,2));
    xstart(2).normalize;
    
    % gas phase
    xstart(3) = saftsuite.core.Composition();
    xstart(3).put(molecules.h2,frac(1,3));
    xstart(3).put(molecules.co2,frac(2,3));
    xstart(3).put(molecules.oct,frac(3,3));
    xstart(3).put(molecules.h2o,frac(4,3));
    xstart(3).put(molecules.meoh,frac(5,3));
    xstart(3).normalize;
    
    %% Flash-Calculation and processing results
%     rhoLstart(1) = eos.rhoLiquid(T,p,xstart(1)); % [mol/m^3]
%     rhoLstart(2) = eos.rhoLiquid(T,p,xstart(2));
%     % rhoLstart(3) = eos.rhoVapor(T,p,xstart(3)) %vapor 
%     rhoLstart(3) = eos.rhoLiquid(T,p,xstart(3)) % supercritical density behaves like liquid density

    

    [x_java, flag, phi] = flashTp3Ph(T, p, z, eos, xstart);
    
    for i = 1:3
        x_eq(1,i) = x_java(i).get(molecules.h2);
        x_eq(2,i) = x_java(i).get(molecules.co2);
        x_eq(3,i) = x_java(i).get(molecules.oct);
        x_eq(4,i) = x_java(i).get(molecules.h2o);
        x_eq(5,i) = x_java(i).get(molecules.meoh);
    end
    phi(1) = phi(1);
    phi(2) = phi(2);
    phi(3) = 1 - phi(1) - phi(2);
end
