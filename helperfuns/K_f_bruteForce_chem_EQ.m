%% load workspace and initialize PCP-Saft
clear;
clc;
close all;
% load base workspace
load base_workspace.mat

% Set paths
SAFTSUITE_PATH = PRJ_PATH + "/saftsuite";
INPUT_PATH = PRJ_PATH + "/input";
OUTPUT_PATH = PRJ_PATH + "/output";
CONST_EQ_PATH = PRJ_PATH + "/constitutive-equations";
GEN_INPUT_PATH = PRJ_PATH + "/generate-input";
REGR_MDL_PATH = INPUT_PATH + "/regr-models";

% Initialize SAFTSUITE
javaaddpath(SAFTSUITE_PATH + "/1_Tp-Flash/SAFTsuite_202012042120_r51.jar"); % adds .jar to dynamic javaclasspath
java_saftsuite.xml_name = SAFTSUITE_PATH + "/1_Tp-Flash/PCP-SAFT_2B_Schemes.xml";
java_saftsuite.eos = "PCP-SAFT";
java_saftsuite.path = SAFTSUITE_PATH + "/1_Tp-Flash/SAFTsuite_202012042120_r51.jar";


% get PCP-SAFT instance
eos = saftsuite.core.EOSRegistry.getInstance.getEOS(java_saftsuite.eos);
projectfile = saftsuite.core.data.ProjectFile();
% saftsuite.core.data.ProjectFile();
projectfile.read(java.io.File(java_saftsuite.xml_name));

%% initial settings
% temperature
T0 = 298.15;    % K
T = 393.15;     % K

% pressure
p = 7000000;    % Pa
p0 = 100000;    % Pa

%number of checked concentrations
nCheck = 20000;

strname_h2 = "Ghosh_Chapman_2003_h2";
strname_co2 = "Khalifa_2018_co2_2B";
strname_oct = "Roman-Ramirez_2010_octanol_2B";
strname_h2o = "Rehner_2020_water_2B";
strname_meoh = "Khalifa_2018_MeOH_2B";
h2 = projectfile.getComponent('h2').getMolecule(strname_h2);
co2 = projectfile.getComponent('co2').getMolecule(strname_co2);
oct = projectfile.getComponent('octanol').getMolecule(strname_oct);
h2o = projectfile.getComponent('water').getMolecule(strname_h2o);
meoh = projectfile.getComponent('methanol').getMolecule(strname_meoh);


%% chemical equilibrium with Gibbs Energy

% stoichometric coefficients
theta(1)= -1;       % carbon dioxide
theta(2)= -3;       % hydrogen
theta(3)= 1;        % methanol
theta(4)= 1;        % water

% Coefficients for DIPPR 107 to calculate IG heat capacity 
% -> DIPPR database DIADEM in J/(kmol*K)
C = [29370 34540 1428 26400 588;...             % carbon dioxide
     27617 9560 2466 3760 567.6;...             % hydrogen
     39252 87900 1916.5 53654 896.7;...         % methanol
     33363 26790 2610.5 8896 1169];             % water

% IG-standard enthalpy and absolute entropy at 298.15K, 1bar. Source:
% DIPPR-Database (CODATA Task Group [Cox, Wagman, et al., 1984] + NIST
% database
H0 = [-393.51e3 0 -200.94e3 -241.818e3];      %J/mol
S0 = [213.677 130.571 239.88 188.825];        %J/mol K

% Calculation of Gibbs Energy for every component
for i = 1:size(theta,2)
%numerical integration
% f_H = @(a) (C(i,1)+ C(i,2)*(C(i,3)/a/sinh(C(i,3)/a))^2+C(i,4)*(C(i,5)/a/cosh(C(i,5)/a))^2)*10^(-3); %J/molK
% f_S = @(b) (C(i,1)+ C(i,2)*(C(i,3)/b/sinh(C(i,3)/b))^2+C(i,4)*(C(i,5)/b/cosh(C(i,5)/b))^2)*10^(-3)/b; %J/molK^2
% delta_Gr(i) = theta(i)*(integral(f_H,T0,T,'ArrayValued',true)-T*(integral(f_S,T0,T,'ArrayValued',true))...
%     +H0(i)-T*S0(i));

%analytic integration
% integral_1 = (C(i,1)*T + C(i,2)*C(i,3)*coth(C(i,3)/T)-C(i,4)*C(i,5)*tanh(C(i,5)/T)...
%     - (C(i,1)*T0 + C(i,2)*C(i,3)*coth(C(i,3)/T0)-C(i,4)*C(i,5)*tanh(C(i,5)/T0)))*10^(-3);
integral_1 = (C(i,1)*T + C(i,2)*C(i,3)/tanh(C(i,3)/T)-C(i,4)*C(i,5)*tanh(C(i,5)/T)...
    - (C(i,1)*T0 + C(i,2)*C(i,3)/tanh(C(i,3)/T0)-C(i,4)*C(i,5)*tanh(C(i,5)/T0)))*10^(-3);
% integral_2 = (C(i,1)*log(T) + C(i,2)*C(i,3)*coth(C(i,3)/T)/T-C(i,2)*log(sinh(C(i,3)/T))-C(i,4)*C(i,5)*tanh(C(i,5)/T)/T+C(i,4)*log(cosh(C(i,5)/T))...
%     - (C(i,1)*log(T0) + C(i,2)*C(i,3)*coth(C(i,3)/T0)/T0-C(i,2)*log(sinh(C(i,3)/T0))-C(i,4)*C(i,5)*tanh(C(i,5)/T0)/T0+C(i,4)*log(cosh(C(i,5)/T0))))*10^(-3);
integral_2 = (C(i,1)*log(T) + C(i,2)*C(i,3)/(tanh(C(i,3)/T)*T)-C(i,2)*log(sinh(C(i,3)/T))-C(i,4)*C(i,5)*tanh(C(i,5)/T)/T+C(i,4)*log(cosh(C(i,5)/T))...
    - (C(i,1)*log(T0) + C(i,2)*C(i,3)/(tanh(C(i,3)/T0)*T0)-C(i,2)*log(sinh(C(i,3)/T0))-C(i,4)*C(i,5)*tanh(C(i,5)/T0)/T0+C(i,4)*log(cosh(C(i,5)/T0))))*10^(-3);
delta_Gr(i) = theta(i)* (integral_1 - T * integral_2 + H0(i) - T * S0(i));
end

% Calculation of chemical equilibrium
K_f_gibbs = exp(-sum(delta_Gr)/8.314/T);

%% create random concentrations

% %test wo oct
% % create random matrix
% rn = rand(1000000,4);
% 
% % define boundary
% lb = [0.00    0.00     0.000     0.000];       %[0.00    0.00  0.6   0.000     0.000];
% ub = [0.05    0.5      0.35      0.35];       %[0.001     0.1    1    0.01      0.01 ];
% %test end wo oct

% create random matrix
rn = rand(1000000,5);

% define boundary
lb = [0.00     0.00  0.9   0.000     0.000];
ub = [0.0005     0.03    1    0.01      0.01];

db = ub-lb;

% get random values in boundary
rn= lb+db.*rn;

% check sum of mole fractions
csum = sum(rn,2);

% kill rowsums below 0.95 and above 1.05
[hit,~] = find(csum>0.95 & csum<1.05);
rn=rn(hit,:);
csum = sum(rn,2);
size(rn,1);

% normalize mole fraction to rowsum of 1
rn= rn./csum;

% % test
% rn(1,:) = [0.0000001 0.0000001 0.9999996 0.0000001 0.0000001];

%% calculate fugacities
for i=1:nCheck
    % compositions
    x_try = saftsuite.core.Composition();
    x_try.put(h2, rn(i,1));
    x_try.put(co2, rn(i,2));
%     %test start w/o oct
%     x_try.put(h2o, rn(i,3));
%     x_try.put(meoh, rn(i,4));
%     %test end w/o oct // old:
    x_try.put(oct, rn(i,3));
    x_try.put(h2o, rn(i,4));
    x_try.put(meoh, rn(i,5));
    x_try.normalize;
    % densities
    rhom = eos.rhoLiquid(T, p, x_try);
    % fugacity
    f_h2 = eos.f(T, rhom, x_try, h2);
    f_co2 = eos.f(T, rhom, x_try, co2);
    f_meoh = eos.f(T, rhom, x_try, meoh);
    f_h2o = eos.f(T, rhom, x_try, h2o);
    % calculate K_f and error
    % octanol isn't part of the reaction, therefore theta_oct = 0
    K_f = (f_h2/p0)^theta(2) * (f_co2/p0)^theta(1) * (f_meoh/p0)^theta(3) * (f_h2o/p0)^theta(4);
    err =  abs(K_f - K_f_gibbs);
    %create array
    result(i,:) = [err K_f f_h2 f_co2 f_meoh f_h2o rn(i,1) rn(i,2) rn(i,3) rn(i,4) rn(i,5)]; %with octanol: [err K_f f_h2 f_co2 f_meoh f_h2o rn(i,1) rn(i,2) rn(i,3) rn(i,4) rn(i,5)];
    disp(i);
end

[~,idx] = sort(result(:,1),'ascend');
result = result(idx,:);

% [~,idx] = min(result(:,1));

disp('starting result iteration ...');
for i=1:100
    % compositions
    x_eq = saftsuite.core.Composition();
    x_eq.put(h2, result(i,7));
    x_eq.put(co2, result(i,8));
%     %test wo oct
%     x_eq.put(h2o, result(i,9));
%     x_eq.put(meoh, result(i,10));
%     %test end wo oct // old:
    x_eq.put(oct, result(i,9));
    x_eq.put(h2o, result(i,10));
    x_eq.put(meoh, result(i,11));
    x_eq.normalize;
    
    % densities
    rhom = eos.rhoLiquid(T, p, x_eq);
    
%     %test wo oct
%     c_h2 = result(i,7) * rhom;
%     c_co2 = result(i,8) * rhom;
%     c_h2o = result(i,9) * rhom;
%     c_meoh = result(i,10) *rhom;
%     %test end wo oct // old:
    c_h2 = result(i,7) * rhom;
    c_co2 = result(i,8) * rhom;
    c_h2o = result(i,10) * rhom;
    c_meoh = result(i,11) * rhom;
    
    K_c = c_h2^theta(2) * c_co2^theta(1) * c_h2o^theta(4) * c_meoh^theta(3);
    
    % K_f = K_phi * K_x * p^sumTheta
    % get fugacity coefficients
    phi_h2 = exp(eos.ln_phi(T, rhom, x_eq, h2));
    phi_co2 = exp(eos.ln_phi(T, rhom, x_eq, co2));
    phi_h2o = exp(eos.ln_phi(T, rhom, x_eq, h2o));
    phi_meoh = exp(eos.ln_phi(T, rhom, x_eq, meoh));
    K_phi = phi_h2^theta(2) * phi_co2^theta(1) * phi_h2o^theta(4) * phi_meoh^theta(3);
    

    
%     p_h2 = p * result(1,7)
%     p_co2 = p * result(1,8)
%     p_h2o = p * result(1,10)
%     p_meoh = p* result(1,11)
%     K_p = (p_h2/p0)^theta(2) * (p_co2/p0)^theta(1) * (p_h2o/p0)^theta(4) * (p_meoh/p0)^theta(3)

%     %test wo oct
%     K_x = result(i,7)^theta(2) * result(i,8)^theta(1) * result(i,9)^theta(4) * result(i,10)^theta(3);
%     %test end wo oct // old:
    K_x = result(i,7)^theta(2) * result(i,8)^theta(1) * result(i,10)^theta(4) * result(i,11)^theta(3);

    sumTheta = (theta(1) + theta(2) + theta(3) + theta(4));
    pTheta = (p/p0)^sumTheta;

    K_f_fug = K_phi * K_x * pTheta;
    
%     %test wo oct
%     result(i,11) = K_f_fug;
%     result(i,12) = K_c;
%     result(i,13) = c_h2;
%     result(i,14) = c_co2;
%     result(i,15) = c_h2o;
%     result(i,16) = c_meoh;
%     %test end wo oct // old:
    % write to result matrix
    result(i,12) = K_f_fug;
    result(i,13) = K_c;
    result(i,14) = c_h2;
    result(i,15) = c_co2;
    result(i,16) = c_h2o;
    result(i,17) = c_meoh;
end
disp('... finished result iteration');

relErr = abs(1-(result(1,2) / K_f_gibbs));

%test wo oct
% disp("script finished with K_c=" + result(1,12) + " and error of " + result(1,1) + " //relative Error =" + relErr);
%test end wo oct
disp("script finished with K_c=" + result(1,13) + " and error of " + result(1,1) + " //relative Error =" + relErr);


% %% postcheck
for i=10000:20000
    %compositions
    x_eq = saftsuite.core.Composition();
    x_eq.put(h2, result(i,7));
    x_eq.put(co2, result(i,8));
%     %test wo oct
%     x_eq.put(h2o, result(i,9));
%     x_eq.put(meoh, result(i,10));
%     %test end wo oct // old:
    x_eq.put(oct, result(i,9));
    x_eq.put(h2o, result(i,10));
    x_eq.put(meoh, result(i,11));
    x_eq.normalize;
    
    %densities
    rhom = eos.rhoLiquid(T, p, x_eq);
    
%     %test wo oct
%     c_h2 = result(i,7) * rhom;
%     c_co2 = result(i,8) * rhom;
%     c_h2o = result(i,9) * rhom;
%     c_meoh = result(i,10) *rhom;
%     %test end wo oct // old:
    c_h2 = result(i,7) * rhom;
    c_co2 = result(i,8) * rhom;
    c_h2o = result(i,10) * rhom;
    c_meoh = result(i,11) * rhom;
    
    K_c = c_h2^theta(2) * c_co2^theta(1) * c_h2o^theta(4) * c_meoh^theta(3);
    
    %K_f = K_phi * K_x * p^sumTheta;
    %get fugacity coefficients
    phi_h2 = exp(eos.ln_phi(T, rhom, x_eq, h2));
    phi_co2 = exp(eos.ln_phi(T, rhom, x_eq, co2));
    phi_h2o = exp(eos.ln_phi(T, rhom, x_eq, h2o));
    phi_meoh = exp(eos.ln_phi(T, rhom, x_eq, meoh));
    K_phi = phi_h2^theta(2) * phi_co2^theta(1) * phi_h2o^theta(4) * phi_meoh^theta(3);
    

    
    p_h2 = p * result(1,7);
    p_co2 = p * result(1,8);
    p_h2o = p * result(1,10);
    p_meoh = p* result(1,11);
    K_p = (p_h2/p0)^theta(2) * (p_co2/p0)^theta(1) * (p_h2o/p0)^theta(4) * (p_meoh/p0)^theta(3);

%     %test wo oct
%     K_x = result(i,7)^theta(2) * result(i,8)^theta(1) * result(i,9)^theta(4) * result(i,10)^theta(3);
%     %test end wo oct // old:
    K_x = result(i,7)^theta(2) * result(i,8)^theta(1) * result(i,10)^theta(4) * result(i,11)^theta(3);

    sumTheta = (theta(1) + theta(2) + theta(3) + theta(4));
    pTheta = (p/p0)^sumTheta;

    K_f_fug = K_phi * K_x * pTheta;
    
%     %test wo oct
%     result(i,11) = K_f_fug;
%     result(i,12) = K_c;
%     result(i,13) = c_h2;
%     result(i,14) = c_co2;
%     result(i,15) = c_h2o;
%     result(i,16) = c_meoh;
%     %test end wo oct // old:
    %write to result matrix
    result(i,12) = K_f_fug;
    result(i,13) = K_c;
    result(i,14) = c_h2;
    result(i,15) = c_co2;
    result(i,16) = c_h2o;
    result(i,17) = c_meoh;
end

%% postcheck end