function [inits, phi_cstr_g_is, phi_liq_o_is] = it_inits(dae, params, inits, optim, rho_gprMdls)
    % ADDME  Computes the initial values for n_i.
    %   
    % iterates initial values to ensure correct phase ratios in CSTR with
    % correct initial moles to get the desired pressure
    %
    
    maxIter = 1000;
    tol(1) = 1e-7;
    tol(2) = 1e4;
    epsilon = 1;
    i=1;
    while epsilon > tol(1) && i < maxIter
        % Mole Fractions
        x_g_h2 = dae.mole_fractions.mole_fraction(inits.n_g_h2_0, inits.n_g_0);
        x_g_co2 = dae.mole_fractions.mole_fraction(inits.n_g_co2_0, inits.n_g_0);
        x_g_oct = 0;
        x_g_h2o = 0;
        x_g_meoh = 0;
        x_aq_h2 = dae.mole_fractions.mole_fraction(inits.n_aq_h2_0, inits.n_aq_0);
        x_aq_co2 = dae.mole_fractions.mole_fraction(inits.n_aq_co2_0, inits.n_aq_0);
        x_aq_oct = dae.mole_fractions.mole_fraction(inits.n_aq_oct_0, inits.n_aq_0);
        x_aq_h2o = dae.mole_fractions.mole_fraction(inits.n_aq_h2o_0, inits.n_aq_0);
        x_aq_meoh = dae.mole_fractions.mole_fraction(inits.n_aq_meoh_0, inits.n_aq_0);
        x_o_h2 = dae.mole_fractions.mole_fraction(inits.n_o_h2_0, inits.n_o_0);
        x_o_co2 = dae.mole_fractions.mole_fraction(inits.n_o_co2_0, inits.n_o_0);
        x_o_oct = dae.mole_fractions.mole_fraction(inits.n_o_oct_0, inits.n_o_0);
        x_o_h2o = dae.mole_fractions.mole_fraction(inits.n_o_h2o_0, inits.n_o_0);
        x_o_meoh = dae.mole_fractions.mole_fraction(inits.n_o_meoh_0, inits.n_o_0);
 
        % Molar Density by constant pressure given in parameters
        inits.rho_M_g_0 = dae.db.get_molar_density(params.T, params.p, ...
            x_g_h2, x_g_co2, x_g_oct, x_g_h2o, x_g_meoh, rho_gprMdls, state="vapor");
        inits.rho_M_aq_0 = dae.db.get_molar_density(params.T, params.p, ...
            x_aq_h2, x_aq_co2, x_aq_oct, x_aq_h2o, x_aq_meoh, rho_gprMdls, state="liquid_h2o");
        inits.rho_M_o_0 = dae.db.get_molar_density(params.T, params.p, ...
            x_o_h2, x_o_co2, x_o_oct, x_o_h2o, x_o_meoh, rho_gprMdls, state="liquid_oct");
    
        % Volumes
        inits.V_aq_0 = dae.volumes.volume(inits.n_aq_0, inits.rho_M_aq_0); % non-ideal mixture
        inits.V_o_0 = dae.volumes.volume(inits.n_o_0, inits.rho_M_o_0); % non-ideal mixture
        inits.V_g_0 = dae.volumes.volume(inits.n_g_0, inits.rho_M_g_0); % non-ideal mixture
        
        % next n_i
        inits.n_g_0 = inits.n_g_0*((1-params.phi_cstr_l)*params.V_tot)/inits.V_g_0;
        inits.n_o_0 = inits.n_o_0*(params.phi_cstr_l*(optim.phi_liq_o)*params.V_tot)/inits.V_o_0;
        inits.n_aq_0 = inits.n_aq_0*(params.phi_cstr_l*(1-optim.phi_liq_o)*params.V_tot)/inits.V_aq_0;
        inits.n_aq_h2o_0 = inits.n_aq_0;
        inits.n_o_oct_0 = inits.n_o_0;
        inits.V_g_0 = dae.volumes.volume(inits.n_g_0, inits.rho_M_g_0); % non-ideal mixture        
        inits.V_o_0 = dae.volumes.volume(inits.n_o_0, inits.rho_M_o_0); % non-ideal mixture
        inits.V_aq_0 = dae.volumes.volume(inits.n_aq_0, inits.rho_M_aq_0); % non-ideal mixture
     
        %calc error
        epsilon = abs(optim.phi_liq_o - (inits.V_o_0 / (inits.V_aq_0 + inits.V_o_0))) ...
            + abs(params.phi_cstr_l - ((inits.V_o_0 + inits.V_aq_0) / params.V_tot));
        i = i + 1;
    end
    
    inits.n_g_h2_0 = inits.n_g_0 * optim.h2_input_vapor_fraction;
    inits.n_g_co2_0 = inits.n_g_0 * (1 - optim.h2_input_vapor_fraction);

    epsilon = 100e5;
    i = 1;
    while epsilon > tol(2) && i < maxIter
        p_solverinit = params.p; % current p is initial value
        lsqopt = optimset('Display','notify', ...
            'TolFun',1e-7);
        p = fminsearch(@(p)dae.iter_solver_p.delta_V(params.T, ...
            p, x_g_h2, x_g_co2, x_g_oct, x_g_h2o, ...
            x_g_meoh, x_aq_h2, x_aq_co2, x_aq_oct, ...
            x_aq_h2o, x_aq_meoh, x_o_h2, x_o_co2, ...
            x_o_oct, x_o_h2o, x_o_meoh, rho_gprMdls, ...
            inits.n_g_0, inits.n_aq_0, inits.n_o_0, params.V_tot, dae), ...
            p_solverinit, lsqopt);

        inits.n_g_0 = inits.n_g_0 * params.p / p;
        inits.n_g_h2_0 = inits.n_g_0 * optim.h2_input_vapor_fraction;
        inits.n_g_co2_0 = inits.n_g_0 * (1 - optim.h2_input_vapor_fraction);
        

        epsilon = abs(params.p - p);
    end
    % Mole Fractions
    x_g_h2 = dae.mole_fractions.mole_fraction(inits.n_g_h2_0, inits.n_g_0);
    x_g_co2 = dae.mole_fractions.mole_fraction(inits.n_g_co2_0, inits.n_g_0);
%     x_aq_h2 = dae.mole_fractions.mole_fraction(inits.n_aq_h2_0, inits.n_aq_0);
%     x_aq_co2 = dae.mole_fractions.mole_fraction(inits.n_aq_co2_0, inits.n_aq_0);
%     x_aq_oct = dae.mole_fractions.mole_fraction(inits.n_aq_oct_0, inits.n_aq_0);
%     x_aq_h2o = dae.mole_fractions.mole_fraction(inits.n_aq_h2o_0, inits.n_aq_0);
%     x_aq_meoh = dae.mole_fractions.mole_fraction(inits.n_aq_meoh_0, inits.n_aq_0);
%     x_o_h2 = dae.mole_fractions.mole_fraction(inits.n_o_h2_0, inits.n_o_0);
%     x_o_co2 = dae.mole_fractions.mole_fraction(inits.n_o_co2_0, inits.n_o_0);
%     x_o_oct = dae.mole_fractions.mole_fraction(inits.n_o_oct_0, inits.n_o_0);
%     x_o_h2o = dae.mole_fractions.mole_fraction(inits.n_o_h2o_0, inits.n_o_0);
%     x_o_meoh = dae.mole_fractions.mole_fraction(inits.n_o_meoh_0, inits.n_o_0);
      
    % Molar Density by constant pressure given in parameters
    inits.rho_M_g_0 = dae.db.get_molar_density(params.T, params.p, ...
        x_g_h2, x_g_co2, x_g_oct, x_g_h2o, x_g_meoh, rho_gprMdls, state="vapor");
    inits.rho_M_aq_0 = dae.db.get_molar_density(params.T, params.p, ...
        x_aq_h2, x_aq_co2, x_aq_oct, x_aq_h2o, x_aq_meoh, rho_gprMdls, state="liquid_h2o");
    inits.rho_M_o_0 = dae.db.get_molar_density(params.T, params.p, ...
        x_o_h2, x_o_co2, x_o_oct, x_o_h2o, x_o_meoh, rho_gprMdls, state="liquid_oct");

    % Volumes
    inits.V_aq_0 = dae.volumes.volume(inits.n_aq_0, inits.rho_M_aq_0); % non-ideal mixture
    inits.V_o_0 = dae.volumes.volume(inits.n_o_0, inits.rho_M_o_0); % non-ideal mixture
    inits.V_g_0 = dae.volumes.volume(inits.n_g_0, inits.rho_M_g_0); % non-ideal mixture

    phi_cstr_g_is = inits.V_g_0/inits.V_aq_0;
    phi_liq_o_is = inits.V_o_0/inits.V_aq_0;

end