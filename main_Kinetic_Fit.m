%% Set up - General
% Clear workspace and command window
clear;
clc;

% Import not official MATLAB packages (folders with a '+' sign)
import logging.*;
import frames.*;

% Package needed by Add-ons Installation
%
% Already in folder with '+'-prefix: (No installation needed)
% 'DataFrame - TimeFrame' by Benjamin Gaudin V3.1.1
% 'Logging'
%
% To be installed:
% 'Symbolic Math Toolbox' Version R2021b
% 'CurveFitting Toolbox' Version R2021b

% Set up Configuration
config = config();

% Set KineticFit structure
kinetic_fit = struct();
kinetic_fit.isKineticFit = true;
kinetic_fit.k_fw = NaN;
kinetic_fit.k_back = NaN;

% Load Parameters into Workspace
optim = optim_parameters;
params = parameter(kinetic_fit.isKineticFit, optim);

%% Set up - Logging
% get the global LogManager
logManager = LogManager.getLogManager();
logManager.resetAll(); % Clean refresh, otherwise both loggers output on console

% add a console handler
consoleHandler = ConsoleHandler();
consoleHandler.setLevel(Level.FINE);
console_logger = Logger.getLogger('Console_Logger'); % creates a new logger object
console_logger.addHandler(consoleHandler);

% add a file handler to the root logger
fileHandler = FileHandler('./main_Kinetic_Fit.log');
fileHandler.setLevel(Level.FINE);
file_logger = Logger.getLogger('File_Logger');
file_logger.reset(); % delete RootConsoleHandler from our standard logger
file_logger.addHandler(fileHandler);

% Test
console_logger.info('Starting main.m ...');
console_logger.fine('Test for fine log');
file_logger.info('Starting main.m ...');
file_logger.info('Hi, this is info!');
file_logger.warning('Hi, this is warning!');
file_logger.fine('Hi, this is fine!');

%% Load Scripts
dae = struct();
dae.mole_fractions = mole_fractions;
dae.concentrations = concentrations;
dae.volumes = volumes;
dae.reaction_kinetics = reaction_kinetics;
dae.reaction_equilibrium = reaction_equilibrium;
dae.liquid_level = liquid_level;
dae.diffusion = diffusion_eqs;
dae.densities = densities;
dae.viscosity = viscosity;
dae.db = database;
dae.iter_solver_p = iter_solver_fnc_pressure;
dae.kin_fit_solver_funcs = kinetic_fit_solver_funcs;

%% Load variables
struct_pressure_curve_exp = load(config.KINETIC_FIT_PATH + "/FittedPressureCurve.mat");
pressure_curve_exp = struct_pressure_curve_exp.curve3;
clear struct_pressure_curve_exp; % Struct not needed anymore

%% Initial Conditions
inits = struct();

% Moles at t0
inits.n_g_0 = 0.07192;                      % [mol] % 0.028028 for 149 bar %0.06692
inits.n_g_h2_0 = inits.n_g_0 * 3/4;          % [mol]
inits.n_g_co2_0 = ...
    inits.n_g_0 - inits.n_g_h2_0;            % [mol]
inits.n_aq_0 = ...
    config.EOS.rhoLiquid(298.15, 101350, config.SAFTSUITE_MOLECULES.h2o) * 2e-06;                         % [mol] Order of Magnitude by Schieweck (2020)
inits.n_aq_h2_0 = 0;                         % [mol]
inits.n_aq_co2_0 = 0;                        % [mol]
inits.n_aq_oct_0 = 0.0023 * inits.n_aq_0;    % [mol] Consider saturation of phases before experimental start
inits.n_aq_meoh_0 = 0;                       % [mol]
inits.n_aq_h2o_0 = inits.n_aq_0 - ...
    (inits.n_aq_h2_0 + inits.n_aq_co2_0 + ...
    inits.n_aq_oct_0 + inits.n_aq_meoh_0);   % [mol]
inits.n_o_0 = ...
    config.EOS.rhoLiquid(298.15, 101350, config.SAFTSUITE_MOLECULES.oct) * 2e-06;                        % [mol] Order of Magnitude by Schieweck (2020)
inits.n_o_h2_0 = 0;                          % [mol]
inits.n_o_co2_0 = 0;                         % [mol]
inits.n_o_h2o_0 = 0.22218 * inits.n_o_0;     % [mol] Consider saturation of phases before experimental start
inits.n_o_meoh_0 = 0;                        % [mol]
inits.n_o_oct_0 = inits.n_o_0 - ...
    (inits.n_o_h2_0 + inits.n_o_co2_0 + ...
    inits.n_o_h2o_0 + inits.n_o_meoh_0);  % [mol]


% Mole Fractions
inits.x_g_h2_0 = dae.mole_fractions.mole_fraction(inits.n_g_h2_0, inits.n_g_0);
inits.x_g_co2_0 = dae.mole_fractions.mole_fraction(inits.n_g_co2_0, inits.n_g_0);
inits.x_g_oct_0 = 0; % no octanol in gas phase
inits.x_g_h2o_0 = 0; % no water in gas phase
inits.x_g_meoh_0 = 0; % no MeOH in gas phase
inits.x_aq_h2_0 = dae.mole_fractions.mole_fraction(inits.n_aq_h2_0, inits.n_aq_0);
inits.x_aq_co2_0 = dae.mole_fractions.mole_fraction(inits.n_aq_co2_0, inits.n_aq_0);
inits.x_aq_oct_0 = dae.mole_fractions.mole_fraction(inits.n_aq_oct_0, inits.n_aq_0);
inits.x_aq_h2o_0 = dae.mole_fractions.mole_fraction(inits.n_aq_h2o_0, inits.n_aq_0);
inits.x_aq_meoh_0 = dae.mole_fractions.mole_fraction(inits.n_aq_meoh_0, inits.n_aq_0);
inits.x_o_h2_0 = dae.mole_fractions.mole_fraction(inits.n_o_h2_0, inits.n_o_0);
inits.x_o_co2_0 = dae.mole_fractions.mole_fraction(inits.n_o_co2_0, inits.n_o_0);
inits.x_o_oct_0 = dae.mole_fractions.mole_fraction(inits.n_o_oct_0, inits.n_o_0);
inits.x_o_h2o_0 = dae.mole_fractions.mole_fraction(inits.n_o_h2o_0, inits.n_o_0);
inits.x_o_meoh_0 = dae.mole_fractions.mole_fraction(inits.n_o_meoh_0, inits.n_o_0);

% Initial Pressure of Batch in Schieweck (2020)
inits.p_0 = 153.073 * 1e5; % [Pa]
inits.k_fw = 3.6836 * 1e-7; % [m^3/(mol*s)]
% inits.k_back = 1 * 1e-4; % [m^3/(mol*s)]

%% CSTR Inputs - Set to zero for batch kinetic fit
inputs = struct();

% To be optimized
inputs.x_g_h2_in = 0;                      % [mol/mol]
inputs.x_g_co2_in = 0;                     % [mol/mol]

% Actually quantities from refluxes
inputs.x_aq_h2_in = 0;                     % [mol/mol]
inputs.x_aq_co2_in = 0;                    % [mol/mol]
inputs.x_aq_oct_in = 0;                    % [mol/mol]
inputs.x_aq_h2o_in = 0;                    % [mol/mol]
inputs.x_aq_meoh_in = 0;                   % [mol/mol]

inputs.x_o_h2_in = 0;                      % [mol/mol]
inputs.x_o_co2_in = 0;                     % [mol/mol]
inputs.x_o_h2o_in = 0;                     % [mol/mol]
inputs.x_o_meoh_in = 0;                    % [mol/mol]
inputs.x_o_oct_in = 0;                     % [mol/mol]

%% More passing arguments computed before solving due to high CPU

% Interfacial Tension
df_sigma_T = dae.db.get_df_sigma_T(config.PRJ_PATH);

% Viscosity
nist_viscosity_gprMdls = dae.db.load_nist_viscosity_gprMdls(config.REGR_MDL_PATH);

% Partition/Distribution Coefficient K models
K_gprMdls = dae.db.load_K_gprMdls(config.REGR_MDL_PATH); 

% Molar Density models
rho_gprMdls = dae.db.load_rho_gprMdls(config.REGR_MDL_PATH);


%% Solver for pressure fitting
file_logger.info('Starting pressure fitting ...');
console_logger.info('Starting pressure fitting ...');

% Array for different K values
K_try = [40];

% Initial values
% k0 = [inits.k_fw, inits.k_back];
k0 = [inits.k_fw];
%lsqnonlin_lb = [1e-8, 1e-11];
%lsqnonlin_ub = [1e-3, 1e-3];
lsqnonlin_lb = 1e-12;
lsqnonlin_ub = 1e-1;

% Solver options
lsqopt = optimoptions(@lsqnonlin,'Display', 'final', 'StepTolerance', 1e-12); %'FiniteDifferenceStepSize', [k0(1)*1e-2, k0(2)*1e-2]);

for i = 1:size(K_try, 1)

    file_logger.info(['Start loop for K_R: ' num2str(K_try(i))]);
    console_logger.info(['Start loop for K_R: ' num2str(K_try(i))]);

    % Set new K_R
    params.K_R = K_try(i);
    
    % Minimization via lsqnonlin
    k = lsqnonlin(@(k)dae.kin_fit_solver_funcs.rmse_pressure_fit(k, pressure_curve_exp, config,params,inputs,inits,dae, ...
            file_logger, df_sigma_T,nist_viscosity_gprMdls,K_gprMdls,rho_gprMdls, kinetic_fit),k0,lsqnonlin_lb,lsqnonlin_ub, lsqopt);
    kinetic_fit.k_fw = k(1);
    % kinetic_fit.k_back = k(2);
    % kinetic_fit.K_R = kinetic_fit.k_fw / kinetic_fit.k_back;
    
    file_logger.info("k_fw: " + num2str(kinetic_fit.k_fw));
    % file_logger.info("k_back: " + num2str(kinetic_fit.k_back));
    console_logger.info("k_fw: " + num2str(kinetic_fit.k_fw));
    % console_logger.info("k_back: " + num2str(kinetic_fit.k_back));
    
    % Reproduce pressure curve
    rmse_end = dae.kin_fit_solver_funcs.rmse_pressure_fit(k, pressure_curve_exp, config,params,inputs,inits,dae, ...
            file_logger, df_sigma_T,nist_viscosity_gprMdls,K_gprMdls,rho_gprMdls, kinetic_fit);
    [P_pfit, T_pfit] = dae.kin_fit_solver_funcs.dae_solve(config,params,inputs,inits,dae, ...
            file_logger, df_sigma_T,nist_viscosity_gprMdls,K_gprMdls,rho_gprMdls, kinetic_fit);
    
    file_logger.info("rmse_end: " + num2str(rmse_end));
    console_logger.info("rmse_end: " + num2str(rmse_end));
    
    % Fit data of the experimental pressure curve
    p_exp = pressure_curve_exp(T_pfit);
    
    file_logger.info('... Finished pressure fitting');
    console_logger.info('... Finished pressure fitting');
    
    %% Plot fitted pressure curve against experimental curve
    fig = figure;
    plot(T_pfit, p_exp);
    hold on;
    scatter(T_pfit, P_pfit,'x');
    legend('Experimental' , 'Fitted Curve with RMSE: ' + string(rmse_end));
    title("kfw: " + kinetic_fit.k_fw + " K_R: " + params.K_R);
    save(config.OUTPUT_PATH + "/kinetic_fit_K_R_" + params.K_R + ".mat", 'fig');

    solver_params = [T_pfit, ...
        p_exp, ...
        P_pfit];

    df_solver_params = frames.DataFrame(solver_params ,[], ...
        [" time [sec]" ...
        "p_exp [Pa]" ...
        "p_fit [Pa]" ]);
    writetable(df_solver_params.t, config.OUTPUT_PATH + "/kinetic_fit_K_R_" + params.K_R + ".xlsx");
end

%% Shutdown

% Close all handlers
console_logger.info('... Finished main.m');
file_logger.info('... Finished main.m');
logManager.resetAll();