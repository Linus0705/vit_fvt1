%% Set up - General
% Clear workspace and command window
clear all;
clc;

%% load workspace and initialize PCP-Saft
% load base workspace
config = config();
% load base_workspace.mat;
% import mole_fractions.*;
% import flashTp.*;
mole_fractions = mole_fractions;


%% initial settings
% temperature
T0 = 298.15;    % K
T = 323.15;     % K

% pressure
p =  500000;    % Pa
p0 = 100000;    % Pa

% H2, CO2, Oct, H2O, MeOH
x = [0.00031 0.00605 0.00211 0.9785 0.01303]; % H2 in mol/mol


frac(1,1) = 0.00001;
frac(2,1) = 0.00001;
frac(3,1) = 0.001;
frac(4,1) = 0.98;
frac(5,1) = 0.19;
frac(1,2) = 0.5;
frac(2,2) = 0.5;
frac(3,2) = 0.0001;
frac(4,2) = 0.0001;
frac(5,2) = 0.0001;
frac
[x_eq, phi] = flashcalc(T, p, frac, config.SAFTSUITE_MOLECULES, config.EOS, x);
x_eq

c_aq_meoh = x_eq(5,1)*config.EOS.rhoLiquid(T,p,config.SAFTSUITE_MOLECULES.h2o)

function [x_eq, phi] = flashcalc(T, p, frac, molecules, eos, x)

    %% Definition of Feed
    z = saftsuite.core.Composition();
%     
%     z.put(molecules.h2,frac.x_h2);
%     z.put(molecules.co2,frac.x_co2);
%     z.put(molecules.oct,frac.x_oct);  
%     z.put(molecules.h2o,frac.x_h2o);
%     z.put(molecules.meoh,frac.x_meoh);
%     z.normalize;

    z.put(molecules.h2,x(1));
    z.put(molecules.co2,x(2));
    z.put(molecules.oct,x(3));  
    z.put(molecules.h2o,x(4));
    z.put(molecules.meoh,x(5));
    z.normalize;
    
    %% Definition of initial values
    
    % Product Phase
    xstart(1) = saftsuite.core.Composition();
    xstart(1).put(molecules.h2,frac(1,1));
    xstart(1).put(molecules.co2,frac(2,1));
    xstart(1).put(molecules.oct,frac(3,1));
    xstart(1).put(molecules.h2o,frac(4,1));
    xstart(1).put(molecules.meoh,frac(5,1));
    xstart(1).normalize;
    
    % gas Phase
    xstart(2) = saftsuite.core.Composition();
    xstart(2).put(molecules.h2,frac(1,2));
    xstart(2).put(molecules.co2,frac(2,2));
    xstart(2).put(molecules.oct,frac(3,2));
    xstart(2).put(molecules.h2o,frac(4,2));
    xstart(2).put(molecules.meoh,frac(5,2));
    xstart(2).normalize;
    
    
    %% Flash-Calculation and processing results
%     rhoLstart(1) = eos.rhoLiquid(T,p,xstart(1)); % [mol/m^3]
%     rhoLstart(2) = eos.rhoLiquid(T,p,xstart(2));
%     % rhoLstart(3) = eos.rhoVapor(T,p,xstart(3)) %vapor 
%     rhoLstart(3) = eos.rhoLiquid(T,p,xstart(3)) % supercritical density behaves like liquid density

    

    [x_java, flag, phi] = flashTp(T, p, z, eos, xstart);
    
    for i = 1:2
        x_eq(1,i) = x_java(i).get(molecules.h2);
        x_eq(2,i) = x_java(i).get(molecules.co2);
        x_eq(3,i) = x_java(i).get(molecules.oct);
        x_eq(4,i) = x_java(i).get(molecules.h2o);
        x_eq(5,i) = x_java(i).get(molecules.meoh);
    end
    phi(1) = phi(1);
    phi(2) = 1- phi(1);
end