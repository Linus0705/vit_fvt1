clear;
close all;
clc;
%% load environment and functions
config = config();
mole_fractions = mole_fractions;
degaser = degaser_Tpflash;
optim = optim_parameters;
params = parameter(false, optim);

%% User input
% Set excel spreadsheet to evaluate
evalFold = strcat(config.OUTPUT_PATH,"/Limitation/20_1_2022_16_34_benchmark");
evalSheet = strcat(evalFold,"/solver_result_20_1_2022_16_34.xlsx");

%% mole fraction in equilibrium
Tab = readtable(evalSheet);
 solver_result = Tab{end,:};

%% initial values
T = 298.15;
p = 500000;
molecules = config.SAFTSUITE_MOLECULES;
eos = config.EOS;

% solver initial
x0 = [T , p];

% save result in table
T_solver = [];
p_solver = [];
x_aq_h2 = [];
x_aq_co2 = [];
n_g_meoh = [];
optim_res = table(T_solver, p_solver, x_aq_h2, x_aq_co2, n_g_meoh);
writetable(optim_res, evalFold + '/degaser_optimization_result.xlsx','WriteRowNames',true);
%% non linear least squares solver
% solver options
lsqopt = optimoptions(@lsqnonlin,'Display', 'final', 'FunctionTolerance', 1e-20, 'StepTolerance', min(x0)*1e-20, 'FiniteDifferenceStepSize', [0.1, 0.1])

% minimization via lsqnonlin
x = lsqnonlin(@(Tp)optim_flash(Tp, solver_result, molecules, eos, mole_fractions, params.V_decanter, params.V_degaser, degaser, evalFold),x0,[298.15, 100000],[373.15, 5000000], lsqopt);

%% compute equilibrium
function n_ges = optim_flash(Tp, solver_result, molecules, eos, mole_fractions, V_decanter, V_degaser, degaser, path)
    T = Tp(1)
    p = Tp(2)
    flash_result = degaser.Tpflash(T, p, solver_result, molecules, eos, mole_fractions, V_decanter, V_degaser);
    
    
    x_aq_h2 = flash_result(2);
    x_aq_co2 = flash_result(3);
    n_g_meoh = flash_result(23);
    n_aq_h2 = flash_result(14);
    n_aq_co2 = flash_result(15);

    optim_nr = table(T, p, x_aq_h2, x_aq_co2, n_g_meoh);
    writetable(optim_nr, path + '/degaser_optimization_result.xlsx','WriteMode','Append',...
    'WriteVariableNames',false,'WriteRowNames',true);
    
    x_aq_h2_n = x_aq_h2/mole_fractions.mole_fraction(solver_result(1, 5),solver_result(1, 4));
    x_aq_co2_n = x_aq_co2/mole_fractions.mole_fraction(solver_result(1, 6),solver_result(1, 4));
    n_g_meoh_n = n_g_meoh/(solver_result(1, 8)*flash_result(25));
    n_aq_h2_n = n_aq_h2/(solver_result(1, 5)*flash_result(24));
    n_aq_co2_n = n_aq_co2/(solver_result(1, 6)*flash_result(24));
%     flash_result(25)
    n_ges = n_aq_co2_n + n_aq_h2_n + 2* n_g_meoh_n
end
