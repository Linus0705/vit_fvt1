function funs = database
    funs.get_df_sigma_T=@get_df_sigma_T;
    funs.get_sigma=@get_sigma;
    funs.get_df_eta_T_p=@get_df_eta_T_p;
    funs.train_regr_mdls=@train_regr_mdls;
    funs.load_nist_viscosity_gprMdls=@load_nist_viscosity_gprMdls;
    funs.get_eta_h2_co2_h2o=@get_eta_h2_co2_h2o;

    funs.get_df_K_wo_gw=@get_df_K_wo_gw;
    funs.train_regr_mdls_K=@train_regr_mdls_K;
    funs.train_final_regr_mdls_K=@train_final_regr_mdls_K;
    funs.load_K_gprMdls=@load_K_gprMdls;
    funs.get_K_i= @get_K_i;

    funs.get_df_rho=@get_df_rho;
    funs.train_regr_mdls_rho=@train_regr_mdls_rho;
    funs.train_final_regr_mdls_rho=@train_final_regr_mdls_rho;
    funs.load_rho_gprMdls=@load_rho_gprMdls;
    funs.get_molar_density=@get_molar_density;
    funs.cross_validation_rho=@cross_validation_rho;

    funs.get_df_pressure=@get_df_pressure;
    funs.load_pressure_gprMdls=@load_pressure_gprMdls;
    funs.get_pressure=@get_pressure;
    

    % For cross validation of K or rho models
    %cross_validation_K;

end

%% interfacial tension
function df_sigma_T = get_df_sigma_T(PRJ_PATH)

    % ADDME  Imports the interfacial tension database
    % 
    % Important info: Needs package 'DataFrame - TimeFrame' by Benjamin
    % Gaudin V3.1.1
    %   
    % Inputs:
    %   PRJ_PATH   : project-path                       [-]
    %
    % Outputs:
    %   df_sigma_T : data frame with temperature and interfacial tension data
    %   
    
    % load excel sheet
    sigma_arr = readmatrix(PRJ_PATH + "/input/interfacial_tension.xlsx");

    % transform temperature and tension data in DataFrame
    df_sigma_T = frames.DataFrame(sigma_arr, [], ...
        ["temperature [°C]", "interfacial_tension [mN/m]"]);

end


function sigma = get_sigma(df_sigma_T, T) 

    % ADDME  Interpolates the interfacial tension from database
    %   
    % Inputs:
    %   df_sigma_T : data frame with temperature and interfacial tension data
    %   T          : temperature                    [K]
    %
    % Outputs:
    %   
    %   sigma      : interfacial tension            [N/m]

    % Adjust units to the ones in database
    T = T - 273.15; % [°C]

    % interpolation of interfacial tension
    sigma = interp1(df_sigma_T.data(:, 1), df_sigma_T.data(:, 2), T); % [mN/m]
    if isnan(sigma)
        % Set sigma to last entry of database since extrapolating
        sigma = df_sigma_T.data(end, 2);
    end

    % Adjust unit to SI-unit
    sigma = sigma * 10^(-3); % [N/m]
end


%% viscosity
function [df_eta_h2, df_eta_co2, df_eta_h2o] = get_df_eta_T_p(PRJ_PATH)

    % ADDME  Imports the interfacial tension database from NIST_viscosity.xlsx
    % 
    % Important info: Needs package 'DataFrame - TimeFrame' by Benjamin
    % Gaudin V3.1.1
    %   
    % Inputs:
    %   PRJ_PATH   : project-path                       [-]
    %
    % Outputs:
    %   df_eta_T_p : data frame with temperature, pressure and dynamic
    %                viscosity data for H2, CO2, Octanol, H2O and Methanol
    %                [kg/(m*s)]
    %   
    
    % load excel sheet
    eta_arr = readmatrix(PRJ_PATH + '/input/NIST_viscosity.xlsx');

    % Remove columns whith 'phase' info
    eta_arr(:,[4 6 8]) = [];
    
    % Transform temperature and tension data in DataFrame
    df_eta_T_p = frames.DataFrame(eta_arr, [], ...
        ["temperature [°C]", "pressure [bar]", ...
        "eta_H2 [kg/(m*s)]", "eta_CO2 [kg/(m*s)]", ...
        "eta_H2O [kg/(m*s)]"]);

    % Return
    df_eta_h2 = df_eta_T_p.loc(:,["temperature [°C]", ...
        "pressure [bar]", "eta_H2 [kg/(m*s)]"]);
    df_eta_co2 = df_eta_T_p.loc(:,["temperature [°C]", ...
        "pressure [bar]", "eta_CO2 [kg/(m*s)]"]);
    df_eta_h2o = df_eta_T_p.loc(:,["temperature [°C]", ...
        "pressure [bar]", "eta_H2O [kg/(m*s)]"]);

end


function train_regr_mdls(df_eta_h2, df_eta_co2, df_eta_h2o) 

    % ADDME  Trains and saves Gauß Process Regression (GPR) models for
    % viscosity data of H2, CO2, H2O. This function needs to be executed
    % only when updating NIST_viscosity.xlsx. Unit for eta is
    % [kg/(m*s)*1e5]. Requires 'Statistics and Machine Learning Toolbox
    % R2021b'
    %   
    % Inputs:
    %   df_eta_h2   : data frame with T, p, eta_h2 [K, bar, kg/(m*s)]
    %   df_eta_co2  : data frame with T, p, eta_co2 [K, bar, kg/(m*s)]
    %   df_eta_h2o  : data frame with T, p, eta_h2o [K, bar, kg/(m*s)]
    %
    % Outputs:
    %   none

    % Adjust unit since GPR cannot handle such small numbers
    df_eta_h2.data(:,3) = df_eta_h2.data(:,3) * 1e5; % [kg/(m*s)*10^5]
    df_eta_co2.data(:,3) = df_eta_co2.data(:,3) * 1e5; % [kg/(m*s)*10^5]
    df_eta_h2o.data(:,3) = df_eta_h2o.data(:,3) * 1e5; % [kg/(m*s)*10^5]

    % Train a GPR model for h2, co2 and h2o viscosity
    gprMdl_h2 = fitrgp(df_eta_h2.t, "eta_H2 [kg/(m*s)]", ...
        'KernelFunction','ardsquaredexponential',...
      'FitMethod','sr','PredictMethod','fic','Standardize',1);
    gprMdl_co2 = fitrgp(df_eta_co2.t, "eta_CO2 [kg/(m*s)]", ...
        'KernelFunction','ardsquaredexponential',...
      'FitMethod','sr','PredictMethod','fic','Standardize',1);
    gprMdl_h2o = fitrgp(df_eta_h2o.t, "eta_H2O [kg/(m*s)]", ...
        'KernelFunction','ardsquaredexponential',...
      'FitMethod','sr','PredictMethod','fic','Standardize',1);
    
    % Save trained models to project path
    save('nist_viscosity_gprMdl_h2.mat', 'gprMdl_h2');
    save('nist_viscosity_gprMdl_co2.mat', 'gprMdl_co2');
    save('nist_viscosity_gprMdl_h2o.mat', 'gprMdl_h2o');

    % For debugging and plotting
%     ypred = resubPredict(gprMdl_h2o);
%     figure();
%     plot(df_eta_h2o.data(:,3),'r.');
%     hold on
%     plot(ypred,'b.');
%     xlabel('x');
%     ylabel('y');
%     legend({'data','predictions'},'Location','Best');
%     axis([0 400 21 23]);
%     hold off;
%     eta_h2o = predict(gprMdl_h2o, [393, 136])

end

function nist_viscosity_gprMdls = load_nist_viscosity_gprMdls(REGR_MDL_PATH) 

    % ADDME  Uses GPR models trained for NIST_viscosity.xlsx to get 
    % viscosity of H2, CO2 and H2O for given temperature and pressure
    %   
    % Inputs:
    %   REGR_MDL_PATH           :   Path of GPR models [-]
    %
    % Outputs:   
    %   nist_viscosity_gprMdls  :   Structure with Gauß models for NIST viscosity [-]

    % Setup return structure
    nist_viscosity_gprMdls = struct();

    % Load GPR models
    load(REGR_MDL_PATH +'/nist_viscosity_gprMdl_h2.mat','gprMdl_h2');
    load(REGR_MDL_PATH +'/nist_viscosity_gprMdl_co2.mat','gprMdl_co2');
    load(REGR_MDL_PATH +'/nist_viscosity_gprMdl_h2o.mat','gprMdl_h2o');

    % Save to structure
    nist_viscosity_gprMdls.gprMdl_h2 = gprMdl_h2;
    nist_viscosity_gprMdls.gprMdl_co2 = gprMdl_co2;
    nist_viscosity_gprMdls.gprMdl_h2o = gprMdl_h2o;

end


function [eta_h2, eta_co2, eta_h2o] = get_eta_h2_co2_h2o(T, p, nist_viscosity_gprMdls) 

    % ADDME  Uses GPR models trained for NIST_viscosity.xlsx to get 
    % viscosity of H2, CO2 and H2O for given temperature and pressure
    %   
    % Inputs:
    %   T                     :   Temperature [K]
    %   p                     :   Pressure [Pa]
    %   nist_viscosity_gprMdls:   Structure with Gauß models for NIST viscosity [-]
    %
    % Outputs:   
    %   eta_h2          :   dynamic viscosity of H2 [kg/(m*s)]
    %   eta_co2         :   dynamic viscosity of CO2 [kg/(m*s)]
    %   eta_h2o         :   dynamic viscosity of H2O [kg/(m*s)]

    % Adjust units to the ones in database
    p = p * 1e-5; % [bar]

    % Predict
    eta_h2 = predict(nist_viscosity_gprMdls.gprMdl_h2, [T, p]);
    eta_co2 = predict(nist_viscosity_gprMdls.gprMdl_co2, [T, p]);
    eta_h2o = predict(nist_viscosity_gprMdls.gprMdl_h2o, [T, p]);

    % Adjust unit back to SI
    eta_h2 = eta_h2 * 1e-5; % [kg/(m*s)]
    eta_co2 = eta_co2 * 1e-5; % [kg/(m*s)]
    eta_h2o = eta_h2o * 1e-5; % [kg/(m*s)]

end

%% distribution coefficients K_wo_gw_i
function [df_K_wo_h2, df_K_wo_co2, df_K_wo_oct, df_K_wo_h2o, df_K_wo_meoh, ...
    df_K_gw_h2, df_K_gw_co2, df_K_gw_oct, df_K_gw_h2o, df_K_gw_meoh] = get_df_K_wo_gw(PRJ_PATH)

    % ADDME  Imports the distribution coefficient database from distribution_coefficients_K.xlsx
    % 
    % Important info: Needs package 'DataFrame - TimeFrame' by Benjamin
    % Gaudin V3.1.1
    %   
    % Inputs:
    %   PRJ_PATH   : project-path                       [-]
    %
    % Outputs:
    %   df_K_... : data frame with temperature, pressure, mole fractions and 
    %                   data for H2, CO2, Octanol, H2O and Methanol
    %                   
    %   
    
    % load excel sheet
    K_arr = readmatrix(PRJ_PATH + "/input/distribution_coefficients_K.xlsx");
   
    % remove -1 and NaN values
    K_arr(any(isnan(K_arr), 2), :) = [];
    rowidx = (K_arr(:,8) < 0);
    K_arr = K_arr(~rowidx,:);

    
    % Transform temperature and tension data in DataFrame
    df_K_T_p = frames.DataFrame(K_arr, [], ...
        ["temperature [K]", "pressure [Pa]", ...
        "x_cstr_h2 [mol/mol]", "x_cstr_co2 [mol/mol]", ...
        "x_cstr_oct [mol/mol]", "x_cstr_h2o [mol/mol]", "x_cstr_meoh [mol/mol]", ...
        "K_wo_h2 [c_org/c_water]", "K_wo_co2 [c_org/c_water]", ...
        "K_wo_oct [c_org/c_water]", "K_wo_h2o [c_org/c_water]", "K_wo_meoh [c_org/c_water]"  ...
        "K_gw_h2 [c_gas/c_water]", "K_gw_co2 [c_gas/c_water]", ...
        "K_gw_oct [c_gas/c_water]", "K_gw_h2o [c_gas/c_water]", "K_gw_meoh [c_gas/c_water]"]);

    % Return K_wo
    df_K_wo_h2 = df_K_T_p.loc(:,["temperature [K]", ...
        "pressure [Pa]", "x_cstr_h2 [mol/mol]", "x_cstr_co2 [mol/mol]", ...
        "x_cstr_oct [mol/mol]", "x_cstr_h2o [mol/mol]", "x_cstr_meoh [mol/mol]", ...
        "K_wo_h2 [c_org/c_water]"]);
    df_K_wo_co2 = df_K_T_p.loc(:,["temperature [K]", ...
        "pressure [Pa]", "x_cstr_h2 [mol/mol]", "x_cstr_co2 [mol/mol]", ...
        "x_cstr_oct [mol/mol]", "x_cstr_h2o [mol/mol]", "x_cstr_meoh [mol/mol]", ...
        "K_wo_co2 [c_org/c_water]"]);
    df_K_wo_oct = df_K_T_p.loc(:,["temperature [K]", ...
        "pressure [Pa]", "x_cstr_h2 [mol/mol]", "x_cstr_co2 [mol/mol]", ...
        "x_cstr_oct [mol/mol]", "x_cstr_h2o [mol/mol]", "x_cstr_meoh [mol/mol]", ...
        "K_wo_oct [c_org/c_water]"]);
    df_K_wo_h2o = df_K_T_p.loc(:,["temperature [K]", ...
        "pressure [Pa]", "x_cstr_h2 [mol/mol]", "x_cstr_co2 [mol/mol]", ...
        "x_cstr_oct [mol/mol]", "x_cstr_h2o [mol/mol]", "x_cstr_meoh [mol/mol]", ...
        "K_wo_h2o [c_org/c_water]"]);
    df_K_wo_meoh = df_K_T_p.loc(:,["temperature [K]", ...
        "pressure [Pa]", "x_cstr_h2 [mol/mol]", "x_cstr_co2 [mol/mol]", ...
        "x_cstr_oct [mol/mol]", "x_cstr_h2o [mol/mol]", "x_cstr_meoh [mol/mol]", ...
        "K_wo_meoh [c_org/c_water]"]);

    % Return K_gw
    df_K_gw_h2 = df_K_T_p.loc(:,["temperature [K]", ...
        "pressure [Pa]", "x_cstr_h2 [mol/mol]", "x_cstr_co2 [mol/mol]", ...
        "x_cstr_oct [mol/mol]", "x_cstr_h2o [mol/mol]", "x_cstr_meoh [mol/mol]", ...
        "K_gw_h2 [c_gas/c_water]"]);
    df_K_gw_co2 = df_K_T_p.loc(:,["temperature [K]", ...
        "pressure [Pa]", "x_cstr_h2 [mol/mol]", "x_cstr_co2 [mol/mol]", ...
        "x_cstr_oct [mol/mol]", "x_cstr_h2o [mol/mol]", "x_cstr_meoh [mol/mol]", ...
        "K_gw_co2 [c_gas/c_water]"]);
    df_K_gw_oct = df_K_T_p.loc(:,["temperature [K]", ...
        "pressure [Pa]", "x_cstr_h2 [mol/mol]", "x_cstr_co2 [mol/mol]", ...
        "x_cstr_oct [mol/mol]", "x_cstr_h2o [mol/mol]", "x_cstr_meoh [mol/mol]", ...
        "K_gw_oct [c_gas/c_water]"]);
    df_K_gw_h2o = df_K_T_p.loc(:,["temperature [K]", ...
        "pressure [Pa]", "x_cstr_h2 [mol/mol]", "x_cstr_co2 [mol/mol]", ...
        "x_cstr_oct [mol/mol]", "x_cstr_h2o [mol/mol]", "x_cstr_meoh [mol/mol]", ...
        "K_gw_h2o [c_gas/c_water]"]);
    df_K_gw_meoh = df_K_T_p.loc(:,["temperature [K]", ...
        "pressure [Pa]", "x_cstr_h2 [mol/mol]", "x_cstr_co2 [mol/mol]", ...
        "x_cstr_oct [mol/mol]", "x_cstr_h2o [mol/mol]", "x_cstr_meoh [mol/mol]", ...
        "K_gw_meoh [c_gas/c_water]"]);

end

function K_gprMdls = load_K_gprMdls(REGR_MDL_PATH) 

    % ADDME  Uses GPR models trained for distribution_coefficients_K.xlsx to get 
    % distribution coefficient of H2, CO2 and H2O in specific phase for given temperature and pressure
    %   
    % Inputs:
    %   REGR_MDL_PATH   :   Path of GPR models [-]
    %
    % Outputs:   
    %   K_gprMdls       :   Structure with Gauß models for partition coefficient K [-]

    % Setup return structure
    K_gprMdls = struct();

    % Load models
    K_gw_gprMdl_h2 = load(REGR_MDL_PATH +"/gprMdl_K_gw_h2.mat");
    K_gw_gprMdl_co2 = load(REGR_MDL_PATH +"/gprMdl_K_gw_co2.mat");
    K_wo_gprMdl_h2 = load(REGR_MDL_PATH +"/gprMdl_K_wo_h2.mat");
    K_wo_gprMdl_co2 = load(REGR_MDL_PATH +"/gprMdl_K_wo_co2.mat");
    K_wo_gprMdl_oct = load(REGR_MDL_PATH +"/gprMdl_K_wo_oct.mat");
    K_wo_gprMdl_h2o = load(REGR_MDL_PATH +"/gprMdl_K_wo_h2o.mat");
    K_wo_gprMdl_meoh = load(REGR_MDL_PATH +"/gprMdl_K_wo_meoh.mat");

    % Destruct from loading structure
    K_gw_gprMdl_h2 = K_gw_gprMdl_h2.complete_mdl;
    K_gw_gprMdl_co2 = K_gw_gprMdl_co2.complete_mdl;
    K_wo_gprMdl_h2 = K_wo_gprMdl_h2.complete_mdl;
    K_wo_gprMdl_co2 = K_wo_gprMdl_co2.complete_mdl;
    K_wo_gprMdl_oct = K_wo_gprMdl_oct.complete_mdl;
    K_wo_gprMdl_h2o = K_wo_gprMdl_h2o.complete_mdl;
    K_wo_gprMdl_meoh = K_wo_gprMdl_meoh.complete_mdl;

    % Save to return structure
    K_gprMdls.K_gw_gprMdl_h2 = K_gw_gprMdl_h2;
    K_gprMdls.K_gw_gprMdl_co2 = K_gw_gprMdl_co2;
    K_gprMdls.K_wo_gprMdl_h2 = K_wo_gprMdl_h2;
    K_gprMdls.K_wo_gprMdl_co2 = K_wo_gprMdl_co2;
    K_gprMdls.K_wo_gprMdl_oct = K_wo_gprMdl_oct;
    K_gprMdls.K_wo_gprMdl_h2o = K_wo_gprMdl_h2o;
    K_gprMdls.K_wo_gprMdl_meoh = K_wo_gprMdl_meoh;

end


function [K_gw_h2, K_gw_co2, K_wo_h2, K_wo_co2, K_wo_oct, ...
    K_wo_h2o, K_wo_meoh] = get_K_i(T, p, x_h2, x_co2, x_oct, x_h2o, x_meoh, K_gprMdls)
    
    % gets distribution coefficient K values for all necessary substances
    % (gw: h2, co2 // wo: h2, co2, oct, h2o, meoh) 
    %   
    % Inputs:
    %   T           :   Temperature of CSTR [K]
    %   p           :   Pressure of CSTR [Pa]
    %   x_i         :   mole fraction of h2, co2, oct, h2o, meoh [mol/mol]
    %   K_gprMdls   :   Structure of Gauß models for K [-]

    %
    % Outputs:
    %   distribution coefficients c_gas/c_water and c_org/c_water 

    % Predict
    K_gw_h2 = predict(K_gprMdls.K_gw_gprMdl_h2, [T, p, x_h2, x_co2, x_oct, x_h2o, x_meoh]);
    K_gw_co2 = predict(K_gprMdls.K_gw_gprMdl_co2, [T, p, x_h2, x_co2, x_oct, x_h2o, x_meoh]);
    K_wo_h2 = predict(K_gprMdls.K_wo_gprMdl_h2, [T, p, x_h2, x_co2, x_oct, x_h2o, x_meoh]);
    K_wo_co2 = predict(K_gprMdls.K_wo_gprMdl_co2, [T, p, x_h2, x_co2, x_oct, x_h2o, x_meoh]);
    K_wo_oct = predict(K_gprMdls.K_wo_gprMdl_oct, [T, p, x_h2, x_co2, x_oct, x_h2o, x_meoh]);
    K_wo_h2o = predict(K_gprMdls.K_wo_gprMdl_h2o, [T, p, x_h2, x_co2, x_oct, x_h2o, x_meoh]);
    K_wo_meoh = predict(K_gprMdls.K_wo_gprMdl_meoh, [T, p, x_h2, x_co2, x_oct, x_h2o, x_meoh]);

end




%% Molar Density
function cross_validation_rho(config, phase)
    %% crossvalidation // create gaussian process models for molar density // 
    %% delete all files in regr-models-K and regr-models-K/best- models before running the script

    %n=10; %default
    n=1;
    modelName=["rho_liq [mol/m^3_phase]", "rho_vap [mol/m^3_phase]"]';
    % get dataframes
    [df_rho_liq, df_rho_vap] = get_df_rho(config.PRJ_PATH, phase);
    
    str="rho_liq [mol/m^3_phase]";
    df=df_rho_liq;
    train_regr_mdls_rho(n,df,str,config);
    close all;

    str="rho_vap [mol/m^3_phase]";
    df=df_rho_vap;
    train_regr_mdls_rho(n,df,str,config);
    close all;
    

    %% train final models
    [df_rho_liq, df_rho_vap] = get_df_rho(config.PRJ_PATH);
    train_final_regr_mdls_rho(df_rho_liq, df_rho_vap, config.INPUT_PATH);

end

function df_rho = get_df_rho(PRJ_PATH, phase)

    % ADDME  Imports the molar density database from rho_three_phases.xlsx
    % 
    % Important info: Needs package 'DataFrame - TimeFrame' by Benjamin
    % Gaudin V3.1.1
    %   
    % Inputs:
    %   PRJ_PATH   : project-path                       [-]
    %   phase      : aggr state "vapor", "liquid_h2o", "liquid_oct" [-]
    %
    % Outputs:
    %   df_rho_... : data frame with temperature, pressure, mole fractions and 
    %                   data for H2, CO2, Octanol, H2O and Methanol
    %                   
    %   
    
    % load excel sheet
    rho_arr = readmatrix(PRJ_PATH + "/input/rho_" + phase + ".xlsx");
   
    % remove -1 values
    %rho_arr(any(isnan(rho_arr), 2), :) = [];
    rowidx = (rho_arr(:,8) < 0);
    rho_arr = rho_arr(~rowidx,:);

    
    % Transform temperature and tension data in DataFrame
    df_rho_T_p = frames.DataFrame(rho_arr, [], ...
        ["temperature [K]", "pressure [Pa]", ...
        "x_cstr_h2 [mol/mol]", "x_cstr_co2 [mol/mol]", ...
        "x_cstr_oct [mol/mol]", "x_cstr_h2o [mol/mol]", "x_cstr_meoh [mol/mol]", ...
        "rho_liq [mol/m^3_phase]", "rho_vap [mol/m^3_phase]"]);

    % Return rho
    if phase == "vapor"
        df_rho = df_rho_T_p.loc(:,["temperature [K]", ...
            "pressure [Pa]", "x_cstr_h2 [mol/mol]", "x_cstr_co2 [mol/mol]", ...
            "x_cstr_oct [mol/mol]", "x_cstr_h2o [mol/mol]", "x_cstr_meoh [mol/mol]", ...
            "rho_vap [mol/m^3_phase]"]);
    elseif phase == "liquid_h2o" || phase == "liquid_oct"
        df_rho = df_rho_T_p.loc(:,["temperature [K]", ...
            "pressure [Pa]", "x_cstr_h2 [mol/mol]", "x_cstr_co2 [mol/mol]", ...
            "x_cstr_oct [mol/mol]", "x_cstr_h2o [mol/mol]", "x_cstr_meoh [mol/mol]", ...
            "rho_liq [mol/m^3_phase]"]);
    else
        
    end
end


function rho_gprMdls = load_rho_gprMdls(REGR_MDL_PATH) 

    % ADDME  Uses GPR models trained for rho_three_phases.xlsx to get 
    % rho of liquid and vapor phases for given temperature, pressure and
    % concentration
    %   
    % Inputs:
    %   REGR_MDL_PATH   :   Path of GPR models [-]
    %
    % Outputs:   
    %   rho_gprMdls       :   Structure with Gauß models for molar density [-]

    % Setup return structure
    rho_gprMdls = struct();

    % Load models
    gprMdl_rho_vapor_struct = load(REGR_MDL_PATH +"/gprMdl_rho_vap.mat");
    gprMdl_rho_liquid_h2o_struct = load(REGR_MDL_PATH +"/gprMdl_rho_liquid_h2o.mat");
    gprMdl_rho_liquid_oct_struct = load(REGR_MDL_PATH +"/gprMdl_rho_liquid_oct.mat");

    % Destruct from loading structure (by name given when model was trained)
    gprMdl_rho_vap = gprMdl_rho_vapor_struct.complete_mdl;
    gprMdl_rho_liq_h2o = gprMdl_rho_liquid_h2o_struct.complete_mdl;
    gprMdl_rho_liq_oct = gprMdl_rho_liquid_oct_struct.complete_mdl;

    % Save to return structure
    rho_gprMdls.gprMdl_rho_vapor = gprMdl_rho_vap;
    rho_gprMdls.gprMdl_rho_liquid_h2o = gprMdl_rho_liq_h2o;
    rho_gprMdls.gprMdl_rho_liquid_oct = gprMdl_rho_liq_oct;

end


function rho_M_phase = get_molar_density(T, p, x_h2, x_co2, x_oct, x_h2o, x_meoh, rho_gprMdls, options)
    arguments
        T
        p
        x_h2
        x_co2
        x_oct
        x_h2o
        x_meoh
        rho_gprMdls
        options.state string = "liquid_h2o"
    end
    
    % Gets molar density for liquid or vapor phase
    %   
    % Inputs:
    %   T            :   Temperature of CSTR [K]
    %   p            :   Pressure of CSTR [Pa]
    %   x_i          :   mole fraction of h2, co2, oct, h2o, meoh [mol/mol]
    %   rho_gprMdls  :   Structure of Gauß models for rho [-]
    %   options.state           :   aggregate state of the phase [-]

    %
    % Outputs:
    %   rho_M_phase     :   molar density of the phase [mol/m^3_phase]

    % Predict
    if strcmp(options.state,"liquid_h2o")
        rho_M_phase = predict(rho_gprMdls.gprMdl_rho_liquid_h2o, [T, p, x_h2, x_co2, x_oct, x_h2o, x_meoh]);
    elseif strcmp(options.state,"liquid_oct")
        rho_M_phase = predict(rho_gprMdls.gprMdl_rho_liquid_oct, [T, p, x_h2, x_co2, x_oct, x_h2o, x_meoh]);
    elseif strcmp(options.state,"vapor")
        rho_M_phase = predict(rho_gprMdls.gprMdl_rho_vapor, [T, p, x_h2, x_co2, x_oct, x_h2o, x_meoh]);
    else
        error("Please set state to 'liquid_h2o', 'liquid_oct' or 'vapor'.")
    end

end