%% Set up - General
% Clear workspace and command window
clear;
clc;
clear dae_system; % Delete persistance variables

% Import not official MATLAB packages (folders with a '+' sign)
import logging.*;
import frames.*;

% --------------------- SET USER INPUT IN LINE 38 --------------------- 
%
% Package needed by Add-ons Installation
%
% Already in folder with '+'-prefix: (No installation needed)
% 'DataFrame - TimeFrame' by Benjamin Gaudin V3.1.1
% 'Logging'
%
% To be installed:
% 'Symbolic Math Toolbox' Version R2021b

%% Initialize
% Set up Configuration
config = config();

% Set KineticFit structure
kinetic_fit = struct();
kinetic_fit.isKineticFit = false;

% Switch for reflux pump
CSTR_sets = struct();
CSTR_sets.withReflux = true;

% Load Parameters into Workspace
optim = optim_parameters;
optim.isOptimization = true;
params = parameter(kinetic_fit.isKineticFit, optim);

%% User input: Set optimization set
% optim.sensitivityAnalysis, optim.doe and optim.finalOptim are implemented with 8 and 5 optimisation parameters, respectively
% see: optim_parameters
evalDoE = optim.finalOptim;

%% Set up - Logging
% get the global LogManager
logManager = LogManager.getLogManager();
logManager.resetAll(); % Clean refresh, otherwise both loggers output on console

% add a console handler
consoleHandler = ConsoleHandler();
consoleHandler.setLevel(Level.FINE);
console_logger = Logger.getLogger('Console_Logger'); % creates a new logger object
console_logger.addHandler(consoleHandler);

% add a file handler to the root logger
fileHandler = FileHandler('./main_Optimisation.log');
fileHandler.setLevel(Level.FINE);
file_logger = Logger.getLogger('File_Logger');
file_logger.reset(); % delete RootConsoleHandler from our standard logger
file_logger.addHandler(fileHandler);

% Test
console_logger.info('Starting main.m ...');
console_logger.fine('Test for fine log');
file_logger.info('Starting main.m ...');
file_logger.info('Hi, this is info!');
file_logger.warning('Hi, this is warning!');
file_logger.fine('Hi, this is fine!');

%% Load Scripts
dae = struct();
dae.mole_fractions = mole_fractions;
dae.concentrations = concentrations;
dae.volumes = volumes;
dae.reaction_kinetics = reaction_kinetics;
dae.reaction_equilibrium = reaction_equilibrium;
dae.liquid_level = liquid_level;
dae.diffusion = diffusion_eqs;
dae.densities = densities;
dae.viscosity = viscosity;
dae.db = database;
dae.iter_solver_p = iter_solver_fnc_pressure;

% disp('please stop');
for j=[4 5 6 7] %1:size(evalDoE,1)
    console_logger.info("Starting simulation : " + j + "/" + size(evalDoE,1) + " ...");
    %% set optimisation parameters
    if isequal(evalDoE, optim.doe)
        optim.T = optim.doe(j,1);
        optim.p = optim.doe(j,2);
        optim.h2_input_vapor_fraction = optim.doe(j,3); 
        optim.V_dot_aq_in_fraction = optim.doe(j,4);
        optim.phi_liq_o = optim.doe(j,5);
    elseif isequal(evalDoE, optim.sensitivityAnalysis)
        optim.T = optim.sensitivityAnalysis(j,1);
        optim.p = optim.sensitivityAnalysis(j,2);
        optim.h2_input_vapor_fraction = optim.sensitivityAnalysis(j,3);
        optim.V_dot_aq_in_fraction = optim.sensitivityAnalysis(j,4);
        optim.phi_liq_o = optim.sensitivityAnalysis(j,5);
        optim.N_imp = optim.sensitivityAnalysis(j,6);
    elseif isequal(evalDoE, optim.finalOptim)
        optim.h2_input_vapor_fraction = optim.finalOptim(j,1);
        optim.phi_liq_o = optim.finalOptim(j,2);
    end

    if ~kinetic_fit.isKineticFit
        try
            params = parameter(kinetic_fit.isKineticFit, optim);
        catch ME
            disp(ME.message);
            continue;
        end
    end

    %% Initial Conditions
    inits = struct();
    
    % Moles at t0
    inits.n_g_0 = 0.37;                     % [mol] Was optimized with kinetic fit solver to meet pressure of 100 bar
    inits.n_g_h2_0 = ...
        inits.n_g_0 * params.h2_input_vapor_fraction;       % [mol]
    inits.n_g_co2_0 = ...
        inits.n_g_0 - inits.n_g_h2_0;         % [mol]
    inits.n_aq_0 = 7.5;                     % [mol]
    inits.n_aq_h2_0 = 0;                      % [mol]
    inits.n_aq_co2_0 = 0;                     % [mol]
    inits.n_aq_oct_0 = 0;                     % [mol]
    inits.n_aq_meoh_0 = 0;                    % [mol]
    inits.n_aq_h2o_0 = inits.n_aq_0 - ...
        (inits.n_aq_h2_0 + inits.n_aq_co2_0 + ...
        inits.n_aq_oct_0 + inits.n_aq_meoh_0); % [mol]
    inits.n_o_0 = 2.027;                          % [mol]
    inits.n_o_h2_0 = 0;                       % [mol]
    inits.n_o_co2_0 = 0;                      % [mol]
    inits.n_o_h2o_0 = 0;                      % [mol]
    inits.n_o_meoh_0 = 0;                     % [mol]
    inits.n_o_oct_0 = inits.n_o_0 - ...
        (inits.n_o_h2_0 + inits.n_o_co2_0 + ...
        inits.n_o_h2o_0 + inits.n_o_meoh_0);  % [mol]
    
    
    % Mole Fractions
    inits.x_g_h2_0 = dae.mole_fractions.mole_fraction(inits.n_g_h2_0, inits.n_g_0);
    inits.x_g_co2_0 = dae.mole_fractions.mole_fraction(inits.n_g_co2_0, inits.n_g_0);
    inits.x_g_oct_0 = 0; % no octanol in gas phase
    inits.x_g_h2o_0 = 0; % no water in gas phase
    inits.x_g_meoh_0 = 0; % no MeOH in gas phase
    inits.x_aq_h2_0 = dae.mole_fractions.mole_fraction(inits.n_aq_h2_0, inits.n_aq_0);
    inits.x_aq_co2_0 = dae.mole_fractions.mole_fraction(inits.n_aq_co2_0, inits.n_aq_0);
    inits.x_aq_oct_0 = dae.mole_fractions.mole_fraction(inits.n_aq_oct_0, inits.n_aq_0);
    inits.x_aq_h2o_0 = dae.mole_fractions.mole_fraction(inits.n_aq_h2o_0, inits.n_aq_0);
    inits.x_aq_meoh_0 = dae.mole_fractions.mole_fraction(inits.n_aq_meoh_0, inits.n_aq_0);
    inits.x_o_h2_0 = dae.mole_fractions.mole_fraction(inits.n_o_h2_0, inits.n_o_0);
    inits.x_o_co2_0 = dae.mole_fractions.mole_fraction(inits.n_o_co2_0, inits.n_o_0);
    inits.x_o_oct_0 = dae.mole_fractions.mole_fraction(inits.n_o_oct_0, inits.n_o_0);
    inits.x_o_h2o_0 = dae.mole_fractions.mole_fraction(inits.n_o_h2o_0, inits.n_o_0);
    inits.x_o_meoh_0 = dae.mole_fractions.mole_fraction(inits.n_o_meoh_0, inits.n_o_0);
        
    %% CSTR Inputs
    inputs = struct();
    
    % To be optimized
    inputs.x_g_h2_in = params.h2_input_vapor_fraction; % [mol/mol]
    inputs.x_g_co2_in = 1-inputs.x_g_h2_in;       % [mol/mol]
    
    % Actually quantities from refluxes
    inputs.x_aq_h2_in = 0;                        % [mol/mol]
    inputs.x_aq_co2_in = 0;                       % [mol/mol]
    inputs.x_aq_oct_in = 0;                       % [mol/mol]
    inputs.x_aq_h2o_in = 1;                       % [mol/mol]
    inputs.x_aq_meoh_in = 0;                      % [mol/mol]
    
    inputs.x_o_h2_in_P2 = 0.0;                     % [mol/mol]
    inputs.x_o_co2_in_P2 = 0.0;                    % [mol/mol]
    inputs.x_o_h2o_in_P2 = 0.0;                    % [mol/mol]
    inputs.x_o_meoh_in_P2 = 0.0;                   % [mol/mol]
    inputs.x_o_oct_in_P2 = 1-(inputs.x_o_h2_in_P2 + ...
        inputs.x_o_co2_in_P2 + ...
        inputs.x_o_h2o_in_P2 + ...
        inputs.x_o_meoh_in_P2);                     % [mol/mol]
    
    
    %% More passing arguments computed before solving due to high CPU
    
    % Interfacial Tension
    df_sigma_T = dae.db.get_df_sigma_T(config.PRJ_PATH);
    
    % Viscosity
    nist_viscosity_gprMdls = dae.db.load_nist_viscosity_gprMdls(config.REGR_MDL_PATH);
    
    % Partition/Distribution Coefficient K models
    K_gprMdls = dae.db.load_K_gprMdls(config.REGR_MDL_PATH); 
    
    % Molar Density models
    rho_gprMdls = dae.db.load_rho_gprMdls(config.REGR_MDL_PATH);
    
    % More inputs
    inputs.rho_M_g_in = dae.db.get_molar_density(params.T, params.p, ...
        inputs.x_g_h2_in, inputs.x_g_co2_in, 0, 0, 0, rho_gprMdls, state="vapor");
    inputs.rho_M_aq_in = dae.db.get_molar_density(params.T, params.p, ...
        inputs.x_aq_h2_in, inputs.x_aq_co2_in, inputs.x_aq_oct_in, ...
        inputs.x_aq_h2o_in, inputs.x_aq_meoh_in, rho_gprMdls, state="liquid_h2o");
    inputs.rho_M_o_in = dae.db.get_molar_density(params.T, params.p, ...
        inputs.x_o_h2_in_P2, inputs.x_o_co2_in_P2, inputs.x_o_oct_in_P2, ...
        inputs.x_o_h2o_in_P2, inputs.x_o_meoh_in_P2, rho_gprMdls, state="liquid_oct");

    % iteratively find inits for gas
    [inits, phi_cstr_l_is, phi_liq_o_is] = it_inits(dae, params, inits, optim, rho_gprMdls);

    y0 = [inits.n_g_0
          inits.n_g_h2_0
          inits.n_aq_0
          inits.n_aq_h2_0
          inits.n_aq_co2_0
          inits.n_aq_oct_0
          inits.n_aq_meoh_0
          inits.n_o_0
          inits.n_o_h2_0
          inits.n_o_co2_0
          inits.n_o_h2o_0
          inits.n_o_meoh_0];


    %% Solver
    file_logger.info('Starting solver ...');
    console_logger.info('Starting solver ...');
    
    CSTR_sets.t_end = 100000;
    
    opts = odeset('InitialStep',1e-9, 'RelTol',1e-9, 'AbsTol',1e-6); % , 'NonNegative', 1:12);
    tspan = [0 CSTR_sets.t_end];
    
    try
        [T, Y] = ode23t(@(t,y)dae_system(t,y,config,params,inputs,inits,dae, ...
            file_logger, df_sigma_T,nist_viscosity_gprMdls,K_gprMdls, ...
            rho_gprMdls, kinetic_fit, CSTR_sets), tspan, y0, opts);
    catch ME
        disp(ME.message);
        disp('Trying ode23tb solver instead of ode23t ...')
        try
            [T, Y] = ode23tb(@(t,y)dae_system(t,y,config,params,inputs,inits,dae, ...
                file_logger, df_sigma_T,nist_viscosity_gprMdls,K_gprMdls, ...
                rho_gprMdls, kinetic_fit, CSTR_sets), tspan, y0, opts);
        catch ME
            disp('... ode 23tb also failed. Trying ode15s ...')
            try
                [T, Y] = ode15s(@(t,y)dae_system(t,y,config,params,inputs,inits,dae, ...
                    file_logger, df_sigma_T,nist_viscosity_gprMdls,K_gprMdls, ...
                    rho_gprMdls, kinetic_fit, CSTR_sets), tspan, y0, opts);
            catch ME        
            disp('... solver failed again. Continue to next DoE experiment.');
            fprintf('⠀⠀⠀⠀  ⣠⠤⠖⠚⠛⠉⠛⠒⠒⠦⢤\n');
            fprintf('⠀⠀⠀⠀⣠⠞⠁⠀⠀⠠⠒⠂⠀⠀⠀⠀⠀⠉⠳⡄\n');
            fprintf('⠀⠀⠀⢸⠇⠀⠀⠀⢀⡄⠤⢤⣤⣤⡀⢀⣀⣀⣀⣹⡄\n');
            fprintf('⠀⠀⠀⠘⢧⠀⠀⠀⠀⣙⣒⠚⠛⠋⠁⡈⠓⠴⢿⡿⠁\n');
            fprintf('⠀⠀⠀⠀⠀⠙⠒⠤⢀⠛⠻⠿⠿⣖⣒⣁⠤⠒⠋\n');
            fprintf('⠀⠀⠀⠀⠀⢀⣀⣀⠼⠀⠈⣻⠋⠉⠁\n');
            fprintf('⠀⠀⠀⡴⠚⠉⠀⠀⠀⠀⠀⠈⠀⠐⢦\n');
            fprintf('⠀⠀⣸⠃⠀⡴⠋⠉⠀⢄⣀⠤⢴⠄⠀⡇\n');
            fprintf('⠀⢀⡏⠀⠀⠹⠶⢀⡔⠉⠀⠀⣼⠀⠀⡇\n');
            fprintf('⠀⣼⠁⠀⠙⠦⣄⡀⣀⡤⠶⣉⣁⣀⠘\n');
            fprintf('⢀⡟⠀⠀⠀⠀⠀⠁⠀⠀⠀⠀⣽\n');
            fprintf('⢸⠇⠀⠀⠀⢀⡤⠦⢤⡄⠀⠀⡟\n');
            fprintf('⢸⠀⠀⠀⠀⡾⠀⠀⠀⡿⠀⠀⣇⣀⣀\n');
            fprintf('⢸⠀⠀⠈⠉⠓⢦⡀⢰⣇⡀⠀⠉⠀⠀⣉⠇\n');
            fprintf('⠈⠓⠒⠒⠀⠐⠚⠃⠀⠈⠉⠉⠉⠉⠉⠁\n');
            continue;
            end
        end
    end
    
    file_logger.info('... Finished solver');
    console_logger.info('... Finished solver');
    
    
    %% Compute quantities for xlsx with algebraic and diff variables of solver
    load (config.OUTPUT_PATH + "/solver_res_alg_var.mat");
    
    % Adapt large t of solver_res_alg_var.mat to small t of T
    final_idxs(1,1) = 0;
    for i = 1:size(T,1)
        [idx, ~] = find(solver_res_alg_var.t == T(i), 1, 'last');
        final_idxs(i, 1) = idx;
    end
    
solver_result = [T, Y, ...
    solver_res_alg_var.N_dot_g_out(final_idxs,1), ...
    solver_res_alg_var.N_dot_aq_out(final_idxs,1), ...
    solver_res_alg_var.N_dot_o_out(final_idxs,1), ...
    solver_res_alg_var.N_dot_g_in(final_idxs,1), ...
    solver_res_alg_var.N_dot_aq_in(final_idxs,1), ...
    solver_res_alg_var.N_dot_o_in(final_idxs,1), ...
    solver_res_alg_var.N_dot_g_out(final_idxs,1) .* solver_res_alg_var.M_g_avg(final_idxs,1), ...
    solver_res_alg_var.N_dot_aq_out(final_idxs,1) .* solver_res_alg_var.M_aq_avg(final_idxs,1), ...
    solver_res_alg_var.N_dot_o_out(final_idxs,1) .* solver_res_alg_var.M_o_avg(final_idxs,1), ...
    solver_res_alg_var.N_dot_g_in(final_idxs,1) .* (params.M_h2 * params.h2_input_vapor_fraction + params.M_co2 * (1-params.h2_input_vapor_fraction)), ...
    solver_res_alg_var.N_dot_aq_in(final_idxs,1) .* params.M_h2o, ...
    solver_res_alg_var.N_dot_o_in(final_idxs,1) .* solver_res_alg_var.M_o_avg_in(final_idxs,1), ...
    solver_res_alg_var.x_o_h2_in(final_idxs,1), ...
    solver_res_alg_var.x_o_co2_in(final_idxs,1), ...
    solver_res_alg_var.x_o_oct_in(final_idxs,1), ...
    solver_res_alg_var.x_o_h2o_in(final_idxs,1), ...
    solver_res_alg_var.x_o_meoh_in(final_idxs,1), ...
    solver_res_alg_var.V_dot_g_in(final_idxs,1), ...
    solver_res_alg_var.V_dot_g_out(final_idxs,1), ...
    solver_res_alg_var.V_dot_aq_out(final_idxs,1), ...
    solver_res_alg_var.V_dot_o_out(final_idxs,1), ...
    solver_res_alg_var.V_dot_purge(final_idxs,1), ...
    solver_res_alg_var.V_g(final_idxs,1), ...
    solver_res_alg_var.V_aq(final_idxs,1), ...
    solver_res_alg_var.V_o(final_idxs,1), ...
    solver_res_alg_var.p_is(final_idxs,1), ...
    solver_res_alg_var.reached_max_co2_limit(final_idxs,1), ...
    solver_res_alg_var.n_o_meoh_reaction(final_idxs,1), ...
    Y(:,7)./solver_res_alg_var.V_aq(final_idxs,1), ...
    Y(:,12)./solver_res_alg_var.V_o(final_idxs,1), ...
    solver_res_alg_var.rho_M_g(final_idxs,1), ...
    solver_res_alg_var.rho_M_aq(final_idxs,1), ...
    solver_res_alg_var.rho_M_o(final_idxs,1) ...
    solver_res_alg_var.rho_M_o_in(final_idxs,1)];

df_solver_result = frames.DataFrame(solver_result ,[], ...
    [" time [sec]" ...
    "n_g [mol]" "n_g_h2 [mol]" ...
    "n_aq [mol]" "n_aq_h2 [mol]" ...
    "n_aq_co2 [mol]" "n_aq_oct [mol]" ...
    "n_aq_meoh [mol]" "n_o [mol]" ...
    "n_o_h2 [mol]" "n_o_co2 [mol]" ...
    "n_o_h2o [mol]" "n_o_meoh [mol]" ...
    "N_dot_g_out [mol/s]" "N_dot_aq_out [mol/s]" ...
    "N_dot_o_out [mol/s]" "N_dot_g_in [mol/s]" ...
    "N_dot_aq_in [mol/s]" "N_dot_o_in [mol/s]" ...
    "M_dot_g_out [kg/s]" "M_dot_aq_out [kg/s]" ...
    "M_dot_o_out [kg/s]" "M_dot_g_in [kg/s]" ...
    "M_dot_aq_in [kg/s]" "M_dot_o_in [kg/s]" ...
    "x_o_h2_in [mol/mol]" "x_o_co2_in [mol/mol]" "x_o_oct_in [mol/mol]" ...
    "x_o_h2o_in [mol/mol]" "x_o_meoh_in [mol/mol]" ...
    "V_dot_g_in [m3/s]" ...
    "V_dot_g_out [m3/s]" "V_dot_aq_out [m3/s]" ...
    "V_dot_o_out [m3/s]" "V_dot_purge [m3/s]" ...
    "V_g [m3]" "V_aq [m3]" ...
    "V_o [m3]" "p_is [Pa]" ...
    "reached_max_co2_limit" ...
    "n_o_meoh_reaction [mol]" ...
    "c_aq_meoh [mol/m3]" ...
    "c_o_meoh [mol/m3]" ...
    "rho_M_g [mol/m^3]" ...
    "rho_M_aq [mol/m^3]" ...
    "rho_M_o [mol/m^3]" ...
    "rho_M_o_in [mol/m^3]"]);

    solver_params = [params.k_fw, ...
        params.K_R, ...
        params.V_dot_o_in_Feed_Fraction, ...
        params.V_dot_o_in_Recycle_Fraction, ...
        params.V_tot ...
        inputs.rho_M_g_in ...
        inputs.rho_M_aq_in];
    
    df_solver_params = frames.DataFrame(solver_params ,[], ...
        ["k_fw [m^3/(mol*s)]" ...
        "K_R [-]" ...
        "V_dot_o_in_Feed_fraction [-]" ...
        "V_dot_o_in_Recycle_fraction [-]" ...
        "V_tot [m3]" ...
        "rho_M_g_in [mol/m^3]" ...
        "rho_M_aq_in [mol/m^3]"]);

    solver_optimparams = [params.T, ...
        params.p, ...
        params.h2_input_vapor_fraction, ...
        params.V_dot_aq_in_fraction, ...
        optim.phi_liq_o, ...
        params.N_imp, ...
        max(Y(:,7)./solver_res_alg_var.V_aq(final_idxs,1)), ...
        Y(end,7)/solver_res_alg_var.V_aq(final_idxs(end,1),1), ...
        solver_res_alg_var.N_dot_aq_out(final_idxs(end,1),1)*(Y(end,7)/Y(end,3))];

    df_solver_optimparams = frames.DataFrame(solver_optimparams ,[], ...
        ["T [K]" ...
        "p [Pa]" ...
        "h2_input_vapor_fraction [mol/mol]" ...
        "V_dot_aq_in_fraction [-]"...
        "phi_liq_o [-]" ...
        "N_imp [1/s]", ...
        "max c_aq_meoh [mol/m^3]" ...
        "end c_aq_meoh [mol/m^3]" ...
        "n_not_meoh_aq_end_out [mol/s]"]);

    %% create dynamic folders and write results to .xlsx
    % get time and date
    date = clock;
    
    % create string for save path
    if j < 10
        strDate = strcat("DoE_00", num2str(j), "_", num2str(date(3)), "_", num2str(date(2)), "_", ...
            num2str(date(1)), "_", num2str(date(4)), "_", num2str(date(5)));
    elseif j < 100
        strDate = strcat("DoE_0", num2str(j), "_", num2str(date(3)), "_", num2str(date(2)), "_", ...
            num2str(date(1)), "_", num2str(date(4)), "_", num2str(date(5)));
    else
        strDate = strcat("DoE_", num2str(j), "_", num2str(date(3)), "_", num2str(date(2)), "_", ...
            num2str(date(1)), "_", num2str(date(4)), "_", num2str(date(5)));
    end
    savePath = strcat(config.OUTPUT_PATH + "/Optimisation/" + strDate);
    
    % create folder and add to path
    mkdir(savePath);
    addpath(savePath);
    
    % write solver results to excel
    writetable(df_solver_result.t, savePath + "/solver_result_" + strDate + ".xlsx", 'Sheet', 'CSTR-Results');
    writetable(df_solver_params.t, savePath + "/solver_result_" + strDate + ".xlsx", 'Sheet', 'Parameter');
    writetable(df_solver_optimparams.t, savePath + "/solver_result_" + strDate + ".xlsx", 'Sheet', 'OptimParameter');
    
    %% Plot Results
    
    fig_gas = figure;
    plot(df_solver_result.data(:,1),df_solver_result.data(:,2:3), '-x');
    hold on;
    plot(df_solver_result.data(:,1),(df_solver_result.data(:,2)-df_solver_result.data(:,3)), '-x');
    legend("n_g [mol]", "n_g_h2 [mol]", "n_g_co2 [mol]");
    
    save(savePath + "/solver_result_gas_" + strDate + ".mat","fig_gas");

    fig_aq = figure;
    plot(df_solver_result.data(:,1),df_solver_result.data(:,4:8), '-x');
    hold on;
    plot(df_solver_result.data(:,1),...
        (df_solver_result.data(:,4)-(df_solver_result.data(:,5)...
        +df_solver_result.data(:,6)+df_solver_result.data(:,7)...
        +df_solver_result.data(:,8))), '-x');
    legend("n_aq [mol]", "n_aq_h2 [mol]", "n_aq_co2 [mol]", "n_aq_oct [mol]", ...
        "n_aq_meoh [mol]", "n_aq_h2o [mol]");

    save(savePath + "/solver_result_aq_" + strDate + ".mat","fig_aq");
    
    fig_o = figure;
    plot(df_solver_result.data(:,1),df_solver_result.data(:,9:13), '-x');
    hold on;
    plot(df_solver_result.data(:,1),...
        (df_solver_result.data(:,9)-(df_solver_result.data(:,10)...
        +df_solver_result.data(:,11)+df_solver_result.data(:,12)...
        +df_solver_result.data(:,13))), '-x');
    legend("n_o [mol]", "n_o_h2 [mol]", "n_o_co2 [mol]", "n_o_h2o [mol]", ...
        "n_o_meoh [mol]", "n_o_oct [mol]");

    save(savePath + "/solver_result_o_" + strDate + ".mat","fig_o");
    
    % status of simulation
    console_logger.info("... finished simulation : " + j + "/" + size(evalDoE,1));

    %% prevent overwriting of variables
    clear final_idxs;
    clear solver_result;
    clear solver_params;
    clear solver_optimparams;
    clear df_solver_result;
    clear df_solver_params;
    clear df_solver_optimparams;

    delete(config.OUTPUT_PATH + "/solver_res_alg_var.mat");

end
%% Shutdown

% Close all handlers
console_logger.info('... Finished main.m');
file_logger.info('... Finished main.m');
logManager.resetAll();