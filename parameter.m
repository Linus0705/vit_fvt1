function params = parameter(isKineticFit, optim)

%% Setup of return as structure
params = struct();

%% Model Parameters (Constant Values)
% physical constants
params.g = 9.81;                           % gravitational acceleration [m/s^2]

% Molar Mass
params.M_h2 = 2.015 * 1e-3;                % [kg/mol]
params.M_co2 = 44.01 * 1e-3;               % [kg/mol]
params.M_oct = 130.229 * 1e-3;             % [kg/mol]
params.M_h2o = 18.015 * 1e-3;              % [kg/mol]
params.M_meoh = 32.04 * 1e-3;              % [kg/mol]

% Reaction
params.nu_h2 = -3;                         % [-]
params.nu_co2 = -1;                        % [-]
params.nu_h2o = 1;                         % [-]
params.nu_meoh = 1;                        % [-]
params.K_R = 40;                           % [-]

% geometric parameters
if isKineticFit
    % For autoklave of Schieweck (2020)
    params.T_vessel = 0.0199648;           % vessel diameter [m]
    params.H_vessel = 0.06389;             % vessel height [m]
    params.d_imp = 0.5 * params.T_vessel;  % impeller diameter [m]

else
    % For miniplant at AVT
    params.T_vessel = 0.0914156;           % vessel diameter [m]
    params.H_vessel = 0.0914156;           % vessel height [m]
    params.d_imp = 42 * 1e-3;              % impeller diameter of 'Schrägblattrührer' [m]

end

params.phi_cstr_l = 2/3;                   % phase fraction liquid in cstr [m^3_liq/m^3_cstr]
params.alpha_imp = 0.87;                   % critical region velocity corrector [-]
params.Xi_c = 0.79;                        % dimensionless critical radius [-]
params.C_imp = 0.03;                       % impeller clearance [m]
params.C_kL_imp = 8.0;                     % Constant for Rushton-Turbine (Prasher, 1973) [-]
params.V_tot = pi/4 * params.T_vessel^2 ...
    * params.H_vessel;                     % from geometry parameters

% Coefficients for Wilke-Chang
params.x_s_h2o = 2.6;                      % Association Factor for Water [-]
params.x_s_oct = 1.3;                      % Association Factor for Octanol [-]

% Critical Molar Volume
params.V_c_h2 = 64.147 * 1e-6;            % [m^3/mol] approved
params.V_c_co2 = 94 * 1e-6;               % [m^3/mol] approved
params.V_c_oct = 509 * 1e-6;              % [m^3/mol] approved
params.V_c_h2o = 55.86 * 1e-6;            % [m^3/mol] approved
params.V_c_meoh = 117 * 1e-6;             % [m^3/mol] approved

% Molar Volume at Normal Boiling Point at 1 atm
% Check consistency with Tyn & Calus
param_volumes = volumes;
params.V_b_h2 = param_volumes.molar_volume_at_normal_boiling_point(params.V_c_h2);     % [m^3/mol]
params.V_b_co2 = param_volumes.molar_volume_at_normal_boiling_point(params.V_c_co2);   % [m^3/mol]
params.V_b_oct = param_volumes.molar_volume_at_normal_boiling_point(params.V_c_oct);   % [m^3/mol]
params.V_b_h2o = param_volumes.molar_volume_at_normal_boiling_point(params.V_c_h2o);   % [m^3/mol]
params.V_b_meoh = param_volumes.molar_volume_at_normal_boiling_point(params.V_c_meoh); % [m^3/mol]

%% Differentiation between operating parameters and Schieweck parameters
if ~isKineticFit
    
    % Reaction kinetic
    params.k_fw = 2.9804 * 1e-07;             % [m^3/(mol*s)]

    % Set optimization/signleRun parameters
    params.T = optim.T;
    params.p = optim.p;
    params.h2_input_vapor_fraction = optim.h2_input_vapor_fraction;
    params.V_dot_aq_in_fraction = optim.V_dot_aq_in_fraction;
    params.N_imp = optim.N_imp;
    
    % Boundaries of real valves
    params.V_dot_g_in_P_4_MAX_LIQ = 10 * 1e-6/60;        % [m^3/s] Liquid CO2 valve
    params.V_dot_g_in_V19_MAX = 2 * 1e-3/60;             % [m^3/s] Gaseous H2 valve

    % Transform liquid CO2 flow into vapour CO2 flow for handy model
    % computations
    params.rho_M_co2_liquid = 1.4598e+04;                % [mol/m^3] @T=298.15K and p=60bar
    n_dot_co2_MAX = ...
        params.rho_M_co2_liquid * ...
        params.V_dot_g_in_P_4_MAX_LIQ;                   % [mol/s] 
    rho_M_co2_vapor = 4.9200e+03;                        % [mol/m^3] @T=393.15K and p=120bar
    params.V_dot_g_in_P_4_MAX_VAP = ...
        n_dot_co2_MAX / rho_M_co2_vapor;                 % [m^3/s] Vapour CO2 volume flow

    params.V_dot_g_in_MAX = ...
        params.V_dot_g_in_P_4_MAX_VAP + ...
        params.V_dot_g_in_V19_MAX;                       % [m^3/s] Control valve for CSTR gas input
    
    % Product Phase
    params.V_dot_aq_in_P_1_MAX = 50 * 1e-6/60;           % [m^3/s]
    params.V_dot_aq_in_P_1_MIN = 5 * 1e-6/60;            % [m^3/s]

    params.V_dot_aq_in = optim.V_dot_aq_in_fraction * params.V_dot_aq_in_P_1_MAX; % [m^3/s]
    
    % Reaction Phase
    params.V_dot_o_in_P_2_MAX = 10 * 1e-6/60;            % [m^3/s] Cat. Phase
    params.V_dot_o_in_P_2_MIN = 1 * 1e-6/60;             % [m^3/s]
    params.V_dot_o_in_P_3_MAX = 50 * 1e-6/60;            % [m^3/s] Recyclepump
    params.V_dot_o_in_P_3_MIN = 5 * 1e-6/60;             % [m^3/s]
    
    params.V_dot_o_in = (optim.phi_liq_o * params.V_dot_aq_in)/(1-optim.phi_liq_o);
    params.V_dot_o_in_Recycle_Fraction = (params.V_dot_o_in-params.V_dot_o_in_P_2_MIN)/params.V_dot_o_in;
    params.V_dot_o_in_Feed_Fraction = params.V_dot_o_in / params.V_dot_o_in_P_2_MAX;

    % Decanter Dead-Time
    params.V_decanter = 4e-4;                       % volume of decanter [m^3]
    params.decanter_t_dead = params.V_decanter / ...
        (params.V_dot_o_in + params.V_dot_aq_in);           % [sec] From Residence Time of Decanter
    
    % Degaser
    params.V_degaser = 0.0005;                      % [m^3]
    params.T_degaser = 298.15;                      % Degaser Temperature [K] (to be optimized)
    params.p_degaser = 500000;                      % Degaser pressure [Pa] (to be optimized)
    
    % Error gets catched in main_Optimization
    if params.V_dot_o_in < 0.999*(params.V_dot_o_in_P_2_MIN + params.V_dot_o_in_P_3_MIN)
        ME = MException('MATLAB:pumpLimit', ...
            'Organic pump limit is undershot. Decrease params.V_dot_aq_in or increase optim.phi_liq_o');
        throw(ME)
    elseif params.V_dot_o_in > 1.001*params.V_dot_o_in_P_2_MAX
        ME = MException('MATLAB:pumpLimit', ...
            'Organic pump limit is overshot. Increase params.V_dot_aq_in or decrease optim.phi_liq_o');
        throw(ME)
    elseif (params.V_dot_o_in * (1 - params.V_dot_o_in_Recycle_Fraction) < 0.999*params.V_dot_o_in_P_2_MIN)
        ME = MException('MATLAB:pumpLimit', ...
            'Minimum volume flow of pump P2 is undershot. Decrease optim.V_dot_o_in_Recycle_Fraction.');
        throw(ME)
    end
elseif isKineticFit
    params.N_imp = 25;                          % impeller rotational speed [1/s] in Schieweck (2020)
    params.T = 393.15;                          % temperature Schieweck (2020) [K]
end
