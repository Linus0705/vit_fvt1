% Degaser
clear;
clc;
close all;

%% load environment and functions
config = config();
mole_fractions = mole_fractions;
degaser = degaser_Tpflash;
optim = optim_parameters;
params = parameter(false, optim);

%% User input
% Set excel spreadsheet to evaluate
evalFold = strcat(config.OUTPUT_PATH,"/Limitation/25_1_2022_22_25_optimizedBenchmark");
evalSheet = strcat(evalFold,"/solver_result_25_1_2022_22_25.xlsx");

%% mole fraction in equilibrium
fprintf('Starting degaser calculation ...');
solver_result = readmatrix(evalSheet,'Sheet','CSTR-Results');
parameter = readtable(evalSheet,'Sheet','OptimParameter');
T = params.T_degaser;
p = params.p_degaser;
molecules = config.SAFTSUITE_MOLECULES;
eos = config.EOS;
V_decanter = params.V_decanter;
V_degaser = params.V_degaser;

flash_result = degaser.Tpflash(T, p, solver_result, molecules, eos, mole_fractions, V_decanter, V_degaser);

df_flash_result = frames.DataFrame(flash_result, [], ...
        [" time[sec]" ...
        "x_aq_h2 [mol]" ...
        "x_aq_co2 [mol]" "x_aq_oct [mol]" ...
        "x_aq_h2o [mol]" "x_aq_meoh [mol]" ...
        "x_g_h2 [mol]" "x_g_co2 [mol]" ...
        "x_g_oct [mol]" "x_g_h2o [mol]" "x_g_meoh [mol]" ...
        "N_dot_aq_out [mol/s]" "N_dot_g_out [mol/s]" ...
        "N_dot_aq_h2 [mol/s]" ...
        "N_dot_aq_co2 [mol/s]" "N_dot_aq_oct [mol]" ...
        "N_dot_aq_h2o [mol/s]" "N_dot_aq_meoh [mol]" ...
        "N_dot_g_h2 [mol/s]" "N_dot_g_co2 [mol/s]" ...
        "N_dot_g_oct [mol/s]" "N_dot_g_h2o [mol/s]" "N_dot_g_meoh [mol/s]" ...
        "n_aq/n_ges [mol/mol]" "n_g/n_ges [mol/mol]"]);

writetable(df_flash_result.t, evalSheet, 'Sheet', 'Degaser-Results');
fprintf('... degaser calculation finished');