function funs = reaction_kinetics
  funs.volumetric_reaction_rate=@volumetric_reaction_rate;
  funs.forward_reaction_rate_constant=@forward_reaction_rate_constant;
end

function R_0_V = volumetric_reaction_rate(k_hin, K_R, c_o_h2, c_o_co2, c_o_meoh, c_o_h2o)
    % ADDME  Computes the general volumetric reation rate.
    % Assumption: Reaction of first order
    %   
    % Inputs:
    %   k_hin       : forward reaction rate constant [m^3/(mol*s)]
    %   K_R         : equilibrium constant of reaction [-]
    %   c_o_h2      : concentration of H2 in organic phase [mol/m^3]
    %   c_o_co2     : concentration of H2 in organic phase [mol/m^3]
    %   c_o_meoh    : concentration of H2 in organic phase [mol/m^3]
    %   c_o_h2o     : concentration of H2 in organic phase [mol/m^3]
    %
    % Outputs:
    %   R_0_V       : general volumetric reaction rate  [mol/(s*m^3)]
    %
    R_0_V = k_hin * c_o_h2 * c_o_co2 - (k_hin / K_R) * c_o_meoh * c_o_h2o;
end


function k_hin = forward_reaction_rate_constant()
    % ADDME  Gets forward reaction rate constant k_hin from database.
    %   
    % Inputs:
    %
    % Outputs:
    %   k_hin       : forward reaction rate constant [m^3/(mol*s)]
    %
    E_A_hin = -16.6 * 1e3; % [J]
    R = 8.314; % [J/(mol*K)]
    T = 393.15; % [K]
    k_0 = 1.03 * 1e7; % [m^3/(mol*s)]
    k_hin = k_0 * exp(E_A_hin/(R*T)); %!!!!!!!!!!!--Database--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end

