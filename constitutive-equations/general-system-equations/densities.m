function funs = densities
    funs.mass_density=@mass_density;
    funs.molar_density=@molar_density;
    funs.pressure=@pressure;
    % PC_SAFT_debugging();
end

function rho = mass_density(rho_M, M)
    % ADDME  Computes mass density.
    %   
    % Inputs:
    %   rho_M   :   molar density given by PCP-SAFT [mol/m^3]
    %   M       :   molar mass [kg/mol]
    %
    % Outputs:
    %   rho     :   mass density [kg/m^3]
    %
    rho = rho_M * M;
end


function rho_M_phase = molar_density(EOS, SAFTSUITE_MOLECULES, T, p, ...
    x_h2, x_co2, x_oct, x_h2o, x_meoh, options)
    arguments
        EOS
        SAFTSUITE_MOLECULES
        T
        p
        x_h2
        x_co2
        x_oct
        x_h2o
        x_meoh
        options.state string = "liquid"
    end

    % ADDME  Computes molar density of the phase using PC-SAFT
    %   
    % Inputs:
    %   EOS                     :   PCP-SAFT instance [-]
    %   SAFTSUITE_MOLECULES     :   Java SaftSuite Molecules Objects
    %   T                       :   temperature in CSTR [K]
    %   p                       :   pressure in CSTR [Pa]
    %   x_i                     :   mol fraction of component i in phase [mol_i/mol_phase]
    %   options.state           :   aggregate state of the phase [-]
    %
    % Outputs:
    %   rho_M_phase     :   molar density of the phase [mol/m^3_phase]
    %

    % Total Composition in CSTR
    x_phase = saftsuite.core.Composition();
    x_phase.put(SAFTSUITE_MOLECULES.h2,x_h2);
    x_phase.put(SAFTSUITE_MOLECULES.co2,x_co2);
    x_phase.put(SAFTSUITE_MOLECULES.oct,x_oct);
    x_phase.put(SAFTSUITE_MOLECULES.h2o,x_h2o);
    x_phase.put(SAFTSUITE_MOLECULES.meoh,x_meoh);

    % Molar Density
    if strcmp(options.state,"liquid")
        rho_M_phase = EOS.rhoLiquid(T,p,x_phase); % [mol/m^3]
    elseif strcmp(options.state,"vapor")
        try
            rho_M_phase = EOS.rhoVapor(T,p,x_phase); % [mol/m^3]
        catch exception
            % Liquid instead of vapor due to supercritical state
            rho_M_phase = EOS.rhoLiquid(T,p,x_phase); % [mol/m^3]
        end
    else
        error("Please set state to 'liquid' or 'vapor'.")
    end

end

function p = pressure(EOS, SAFTSUITE_MOLECULES, T, rho, ...
    x_h2, x_co2, x_oct, x_h2o, x_meoh)

    % ADDME  Computes pressure of the phase using PC-SAFT
    % (used as debug check in dae_system)
    %   
    % Inputs:
    %   EOS                     :   PCP-SAFT instance [-]
    %   SAFTSUITE_MOLECULES     :   Java SaftSuite Molecules Objects
    %   T                       :   temperature in CSTR [K]
    %   rho                     :   molar density in CSTR [mol/m^3]
    %   x_i                     :   mol fraction of component i in phase [mol_i/mol_phase]
    %   options.state           :   aggregate state of the phase [-]
    %
    % Outputs:
    %   rho_M_phase     :   molar density of the phase [mol/m^3_phase]
    %

    % Total Composition in CSTR
    x_phase = saftsuite.core.Composition();
    x_phase.put(SAFTSUITE_MOLECULES.h2,x_h2);
    x_phase.put(SAFTSUITE_MOLECULES.co2,x_co2);
    x_phase.put(SAFTSUITE_MOLECULES.oct,x_oct);
    x_phase.put(SAFTSUITE_MOLECULES.h2o,x_h2o);
    x_phase.put(SAFTSUITE_MOLECULES.meoh,x_meoh);

    % Pressure
    p = EOS.p(T,rho,x_phase); % [Pa]
end











%% For DEBUGGING


function debugging_output = PC_SAFT_debugging()
    % Debugging for PCP-SAFTSuite
    clear all
    clear java
    clc
    close all % schließt alle figures
    PRJ = matlab.project.rootProject;
    PRJ_PATH = PRJ.RootFolder;
    SAFTSUITE_PATH = PRJ_PATH + "\saftsuite";
    INPUT_PATH = PRJ_PATH + "\input";
    OUTPUT_PATH = PRJ_PATH + "\output";
    CONST_EQ_PATH = PRJ_PATH + "\constitutive-equations";

    % Needs Package 'Optimization Toolbox R2021b' 

    % input
    T = 403.15; %[K]
    p = 100 * 1e5; %[Pa]
    
    % feed
    x(1) = 0.15; % H2
    x(2) = 0.15; % CO2
    x(3) = 0.3; % Oct
    x(4) = 0.3; % H2O
    x(5) = 0.1; % MeOH
    
    %initialize SAFTsuite
    % add SAFTsuite jar-file to classpath
    javaaddpath(SAFTSUITE_PATH + '\1_Tp-Flash\SAFTsuite_202012042120_r51.jar');
    
    java_saftsuite.xml_name = SAFTSUITE_PATH + "\1_Tp-Flash\PCP_SAFT_ViT.xml";
    java_saftsuite.eos = 'PCP-SAFT';
    java_saftsuite.path = SAFTSUITE_PATH + '\1_Tp-Flash\SAFTsuite_202012042120_r51.jar';
    
    % get PCP-SAFT instance
    eos = saftsuite.core.EOSRegistry.getInstance.getEOS(java_saftsuite.eos);
    
    % load project file
    % error message: "Dot indexing is not supported for variables of this
    % type." --> name of xml-file =/= projectfile.read(java.io.File(...)
    projectfile = saftsuite.core.data.ProjectFile();
    projectfile.read(java.io.File(java_saftsuite.xml_name));
    
    
    % get molecules from project file
    h2 = projectfile.getComponent('h2').getMolecule("Ghosh_et_al");
    co2 = projectfile.getComponent('co2').getMolecule("Fu_et_al");
    oct = projectfile.getComponent('octanol').getMolecule("Umer et al._octanol_2B");
    h2o = projectfile.getComponent('water').getMolecule("TeKa_2020_water_4C");
    meoh = projectfile.getComponent('methanol').getMolecule("Mourah_et_al_3B_Version2");
    
    %% Definition of Feed
    z = saftsuite.core.Composition();
    
    z.put(h2,x(1));
    z.put(co2,x(2));
    z.put(oct,x(3));  
    z.put(h2o,x(4));
    z.put(meoh,x(5));
    z.normalize;
    
    %% Definition of initial values
    
    %polar phase (=heavy phase) - Water-Phase
    xstart(1) = saftsuite.core.Composition();
    xstart(1).put(h2,0.00001);
    xstart(1).put(co2,0.0004);
    xstart(1).put(oct,0.0004);
    xstart(1).put(h2o,0.999);
    xstart(1).put(meoh,0.0001);
    xstart(1).normalize;
    
    %nonpolar phase (=light phase) - Octanol-Phase (Reaktionphase)
    xstart(2) = saftsuite.core.Composition();
    xstart(2).put(h2,0.0040);
    xstart(2).put(co2,0.08);
    xstart(2).put(oct,0.8);
    xstart(2).put(h2o,0.0004);
    xstart(2).put(meoh,0.2);
    xstart(2).normalize;
    
    % supercritical (liquid) phase
    xstart(3) = saftsuite.core.Composition();
    xstart(3).put(h2,0.5);
    xstart(3).put(co2,0.5);
    xstart(3).put(oct,0.0001);
    xstart(3).put(h2o,0.0001);
    xstart(3).put(meoh,0.0001);
    xstart(3).normalize;
    
    %% Flash-Calculation and processing results
    rhoLstart(1) = eos.rhoLiquid(T,p,xstart(1)); % [mol/m^3]
    rhoLstart(2) = eos.rhoLiquid(T,p,xstart(2));
    % rhoLstart(3) = eos.rhoVapor(T,p,xstart(3)) %vapor 
    rhoLstart(3) = eos.rhoLiquid(T,p,xstart(3)) % supercritical density behaves like liquid density

    

    [x_java, flag, phi] = flashTp3Ph(T, p, z, eos, xstart);
    
    for i = 1:3
        x_eq(1,i) = x_java(i).get(h2);
        x_eq(2,i) = x_java(i).get(co2);
        x_eq(3,i) = x_java(i).get(oct);
        x_eq(4,i) = x_java(i).get(h2o);
        x_eq(5,i) = x_java(i).get(meoh);
    end

    x_eq
    phi

    for i = 1:3
        x_eq_comp(i) = saftsuite.core.Composition();
        x_eq_comp(i).put(h2,x_eq(1,i));
        x_eq_comp(i).put(co2,x_eq(2,i));
        x_eq_comp(i).put(oct,x_eq(3,i));
        x_eq_comp(i).put(h2o,x_eq(4,i));
        x_eq_comp(i).put(meoh,x_eq(5,i));
    end

    rho_eq(1) = eos.rhoLiquid(T,p,x_eq_comp(1)); % [mol/m^3]
    rho_eq(2) = eos.rhoLiquid(T,p,x_eq_comp(2));
    rho_eq(3) = eos.rhoLiquid(T,p,x_eq_comp(3)) % supercritical density behaves like liquid density


%     x_eq_h2_phase1 = saftsuite.core.Composition();
%     x_eq_h2_phase1.put(h2,x_eq(1,1));
%     x_eq_h2_phase2 = saftsuite.core.Composition();
%     x_eq_h2_phase2.put(h2,x_eq(1,2));
%     fugacity_coeff = eos.ln_phi(T, 1000, x_eq_h2_phase1, h2);
%     fugacity_coeff = eos.ln_phi(T, 1000, x_eq_h2_phase2, h2);


end