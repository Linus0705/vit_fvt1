function funs = concentrations
  funs.molar_concentration=@molar_concentration;
end

function c_i = molar_concentration(n_i, V_phase)
    % ADDME  Computes Molar Concentration.
    %   
    % Inputs:
    %   n_i     :   mol of the component [mol]
    %   V_phase :   volume of the phase [m^3_phase]
    %
    % Outputs:
    %   c_i :   molar concentration of the component [mol/m^3]
    %
    c_i = n_i / V_phase;
end
