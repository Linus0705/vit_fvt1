function funs = liquid_level
  funs.level_at_rest=@level_at_rest;
end


function H_liquid_0 = level_at_rest(V_liquid, D_vessel)
    % ADDME  Computes liquid level at rest.
    %   
    % Inputs:
    %   V_liquid       : volume of liquid [m^3]
    %   D_vessel       : diameter of vessel [m^2]
    %
    % Outputs:
    %   H_liquid_0     : level of liquid in CSTR at rest [m]
    %
    A_vessel = (pi/4) * D_vessel^2;
    H_liquid_0 = V_liquid / A_vessel;
end
