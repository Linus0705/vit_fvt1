function funs = volumes
  funs.volume=@volume;
  funs.molar_volume_at_normal_boiling_point=@molar_volume_at_normal_boiling_point;
end


function V_phase = volume(n_phase, molar_density)
    % ADDME  Computes volume of the phase.
    %   
    % Inputs:
    %   n_phase :   mol of the phase [mol]
    %   molar_density :   computed by PCP-SAFT [mol_phase/m^3_phase]
    %
    % Outputs:
    %   V_phase :   volume of the phase [m^3]
    %

    V_phase = n_phase / molar_density;
end


function V_b_i = molar_volume_at_normal_boiling_point(V_c_i)
    % ADDME  Computes molar volume at normal boiling temperature using the
    % critical volume (Tyn & Calus, 1975)
    %   
    % Inputs:
    %   V_c_i :   critical molar volume [m^3/mol]
    %
    % Outputs:
    %   V_b_i :   molar volume at normal boiling temperature [m^3/mol]
    %

    % Adjust units for correlation
    V_c_i = V_c_i * 10^6; % [cm^3/mol]

    % Tyn & Calus Correlation
    V_b_i = 0.285 * V_c_i^1.048; % [cm^3/mol]

    % Adjust unit for SI-Unit output
    V_b_i = V_b_i * 10^(-6); % [m^3/mol]
end
