function funs = mole_fractions
  funs.mole_fraction=@mole_fraction;
  funs.fun2=@fun2;
end

% Computes Mole Fraction
function x_i = mole_fraction(n_i, n_phase)
    % ADDME  Computes mole fraction of the component.
    %   
    % Inputs:
    %   n_i     :   mol of component i [mol]
    %   n_phase :   mol of phase [mol_phase]
    %
    % Outputs:
    %   x_i :   mole fraction of component i in phase [mol/mol]
    %
    x_i = n_i / n_phase;
end

function z=fun2
  z=1;
end
