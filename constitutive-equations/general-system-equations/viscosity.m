function funs = viscosity
    funs.eta_mixt=@eta_mixt;
end


function eta_mixt = eta_mixt(eta_h2, eta_co2, eta_oct, eta_h2o, eta_meoh, ...
    x_h2, x_co2, x_oct, x_h2o, x_meoh) 

    % ADDME  Classic Grunberg-Nissan (1949) mixing rule for liquids 
    %   
    % Inputs:
    %   eta_i           :   dynamic viscosity of component i [kg/(m*s)]
    %   x_i             :   mol fraction of component i [mol_i/mol_phase]
    %
    % Outputs:   
    %   eta_mixt        :   dynamic viscosity of the mixture [kg/(m*s)]

    % Mixing rule
    eta_mixt = exp(...
        x_h2 * log(eta_h2) + ...
        x_co2 * log(eta_co2) + ...
        x_oct * log(eta_oct) + ...
        x_h2o * log(eta_h2o) + ...
        x_meoh * log(eta_meoh));

end