function funs = diffusion_eqs
  funs.diff_flux=@diff_flux;
  funs.beta=@beta;
  funs.k_L=@k_L;
  funs.k_d=@k_d;
  funs.k_c=@k_c;
  funs.diff_coefficient=@diff_coefficient;
end


function n_dot_a_i = diff_flux(beta, c_aq_i, c_second_phase_i, K_i)
    % ADDME  Computes area specific diffusion flow, also called 'flux' then.
    %   
    % Inputs:
    %   beta            :   mass transfer (Uebergangs) coefficient [mol/(s*m^2)]
    %   x_aq_i          :   mole fraction in aqeous phase [mol_aq_i/mol_aq]
    %   x_second_phase_i:   mole fraction in second phase [mol_2nd_i/mol_2nd]
    %   K_i             :   distribution coefficient [mol_aq/mol_2nd]
    %
    % Outputs:
    %   n_dot_a_i       :   diffusion flux [mol/(s*m^2)]
    %

    n_dot_a_i = beta * (c_aq_i * K_i - c_second_phase_i);
end


function beta_i = beta(k_second_phase_i, k_aq_i, K_i)
    % ADDME  Computes overall mass transfer (Uebergangs) coefficient beta.
    %        Approach of two-film-theorie.
    %   
    % Inputs:
    %   roh_M_aq          :   computed by PCP-SAFT [mol_phase/m^3_phase]
    %   roh_M_second_phase:   computed by PCP-SAFT [mol_phase/m^3_phase]
    %   k_second_phase_i  :   one sided mass transfer coefficient [m/s]
    %   k_aq_i            :   one sided mass transfer coefficient [m/s]
    %   K_i               :   distribution coefficient [mol_aq/mol_2nd]
    %
    % Outputs:
    %   beta_i            :   mass transfer (Uebergangs) coefficient [mol/(s*m^2)]
    %

    beta_i = ((1 / (k_second_phase_i))+(K_i / (k_aq_i)))^(-1);
end


function k_L_i = k_L(eta_aq, roh_aq, D_diff_i_in_h2o, C_kL_imp, N_imp, d_imp, T_vessel, H_liquid_0)
    % ADDME  Computes gas-liquid mass transfer coefficient k_L by Prasher (1973) 
    %   
    % Inputs:
    %   eta_aq          :   viscosity of aqeous phase [kg/(m*s)]
    %   roh_aq          :   mass density of aqeous phase [kg/m^3]
    %   D_diff_i_in_h2o :   diffusion coefficient of component i in water [m^2/s]
    %   C_kL_imp        :   constant for rushton-impeller [-]
    %   N_imp           :   impeller speed [rps]
    %   d_imp           :   impeller diameter [m]
    %   T_vessel        :   vessel diameter [m]
    %   H_liquid_0      :   liquid level in CSTR at rest [m]
    %
    % Outputs:
    %   k_L_i           :   liquid-gas mass transfer coefficient of liquid side [m/s]
    %
    
    % 
    Sc_i = eta_aq / (roh_aq * D_diff_i_in_h2o); % Schmidt-Number [-]
    nu_aq = eta_aq / roh_aq; % kinematic viscosity [m^2/s]
    eps = (C_kL_imp * N_imp^3 * d_imp^5) / (T_vessel^2 * H_liquid_0); % Energy of dissipation by turbulence per unit mass [m^2/s^3]

    % Fitting Parameters
    C1 = 0.4;
    C2 = -1/2;
    C3 = 1/4;

    % Final Correlation
    k_L_i = C1 * Sc_i^(C2) * (eps * nu_aq)^(C3);
end


function k_d_i = k_d(D_diff_i_in_oct, d32)
    % ADDME  Computes liquid-liquid mass transfer coefficient on disperse
    % side by modified Kronig & Brink (Azizi, 2011), (Henschke, 2003)
    %   
    % Inputs:
    %   D_diff_i_in_oct:   diffusion coefficient of component i in octanol [m^2/s]
    %   d32            :   sauter diameter [m]
    %
    % Outputs:
    %   k_d_i          :   liquid-liquid mass transfer coefficient of disperse side [m/s]
    %

    C1 = 17.7;
    k_d_i = C1 * (D_diff_i_in_oct / d32);
end


function k_c_i = k_c(eta_aq, roh_aq, D_diff_i_in_h2o, d_imp, N_imp, g, roh_o, sigma, T_vessel, phi, d32)
    % ADDME  Computes liquid-liquid mass transfer coefficient on continuous
    % side by Skelland & Moe (1990)
    %   
    % Inputs:
    %   eta_aq          :   viscosity of aqeous phase [kg/(m*s)]
    %   roh_aq          :   mass density of aqeous phase [kg/m^3]
    %   D_diff_i_in_h2o :   diffusion coefficient of component i in water [m^2/s]
    %   d_imp           :   impeller diameter [m]
    %   N_imp           :   impeller speed [rps]
    %   g               :   gravitational force [m/s^2]
    %   roh_o           :   mass density of organic phase [kg/m^3]
    %   sigma           :   interfacial tension between aqeous and organic phase [N/m = kg/s^2]
    %   T_vessel        :   vessel diameter [m]
    %   phi             :   hold-up = disperse fraction [m^3_disperse/(m^3_conti + m^3_disperse)]
    %   d32             :   sauter diameter [m]
    %
    % Outputs:
    %   k_c_i           :   liquid-liquid mass transfer coefficient of continuous side [m/s]
    %

    % Dimensionless Numbers
    Sc = eta_aq / (roh_aq * D_diff_i_in_h2o);
    Re = (d_imp^2 * N_imp * roh_aq) / eta_aq;
    Fr = (d_imp * N_imp^2) / g;
    We = (roh_o * d32^2 * g) / sigma;

    % Final Correlation
    Sh_wo_c = 1.237 * 10^(-5) * Sc^(1/3) * Re^(2/3) * Fr^(5/12) ...
        * (d_imp / d32)^2 * (d32 / T_vessel)^(1/2) * We^(5/4) * phi^(-1/2);

    % Get Mass Transfer Coefficient from Sherwood-Number
    k_c_i = (Sh_wo_c * D_diff_i_in_h2o) / d32;
    
    % NOTE CHRISTOPH: PLS FIT ME
    % Correction - Order of magnitude should be 1e-4 m/s
    k_c_i = k_c_i * 1e2;
end


function D_diff_i_in_solv = diff_coefficient(x_s_solv, M_solv, T, eta_solv, V_b_i)
    % ADDME  Computes diffusion coefficient by Wilke-Chang (1955) which is
    % an empirical modification of the Stokes-Einstein relation
    %   
    % Inputs:
    %   x_s_solv        :   Association Factor [-]
    %   M_solv          :   molar mass of the solvent [kg/mol]
    %   T               :   temperature [Kelvin]
    %   eta_solv        :   viscosity of the solvent [kg/(m*s)]
    %   V_b_i           :   molar volume of component i at normal boiling [m^3/mol]
    %
    % Outputs:
    %   D_diff_i_in_solv:   diffusion coefficient of component i in solvent [m^2/s]
    %

    % Adjust units for correlation
    M_solv = M_solv * 1e3; % [g/mol]
    eta_solv = eta_solv * 1e3; % [g/(m*s)]
    V_b_i = V_b_i * 1e6; % [cm^3/mol]
    
    % Final Correlation
    C1 = 7.4 * 1e-8;
    D_diff_i_in_solv = C1 * (((x_s_solv * M_solv)^0.5 * T)/ (eta_solv * V_b_i^0.6)); % [cm^2/s]

    % Adjust unit
    D_diff_i_in_solv = D_diff_i_in_solv * 10^-4; % [m^2/s]
end

