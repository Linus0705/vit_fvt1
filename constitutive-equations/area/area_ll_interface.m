function [S_a_wo, d_32] = area_ll_interface(D, N, V_o, V_aq, rho_aq, sigma)

% ADDME  Computes the area of the liquid-gas interface.
    %   
    % Inputs:
    %   V_o         : volume of organic (dispersed) phase [m^3]
    %   V_aq        : volume of aqueous (conti) phase [m^3]
    %   D           : impeller diameter [m]
    %   N           : impeller rotational speed [s^-1]
    %   rho_aq      : density of aqueous phase [mol/m^3]
    %   sigma       : interfacial tension [N/m]
    %
    % Outputs:
    %   S_a_wo      : area of the liquid-gas interface  [m^2]
    %
    
    %% Input data
    % volume fraction of dispersed phase (hold-up) [-]
    Phi = V_o/(V_o + V_aq);
    % Weber number [-]
    We = (N^2 * D^3 * rho_aq)/sigma;
    
    %% sauter-diameter (Coulaloglou and Travlarides(1976)) [m]
    d_32 = D * 0.081*(1 + 4.47*Phi)*(We^(-0.6));
    
    %% calculation of droplet geometry
    % droplet surface [m^2]
    A_dr = pi * (d_32^2);
    % droplet volume [m^3]
    V_dr = (pi/6) * (d_32^3);
    
    %% calculation of ll-interface 
    % number of dispersed droplets [-]
    N_dr = V_o/V_dr;
    % liquid-liquid-interface [m^2]
    S_a_wo = N_dr * A_dr;

end
    
    