function S_a_gw = area_gl_interface(g, N, D, T, C, H_0, alpha, Xi_c)


    %% ADDME  Computes the area of the liquid-gas interface.
    %   
    % Inputs:
    
    %   g           : gravitational acceleration [m/s^2]
    %   D           : impeller diameter [m]
    %   N           : impeller rotational speed [s^-1]
    %   T           : vessel diameter [m]
    %   H_0         : liquid level at rest [m]
    %   C           : impeller clearance [m]
    %   alpha       : critical region velocity corrector [-]
    %   Xi_c        : dimensionless critical radius [-]
    %
    % Outputs:
    %   S_a_gw      : area of the liquid-gas interface  [m^2]
    %
    
    %% Input data
    % dimensionless vessel radius (-)
    Xi_w = (T/2)/(D/2);
    % Froude number (-)
    N_Fr = ((N^2)*D)/g;
    % impeller number (-)
    beta = ((alpha*pi*Xi_c)^2)/2;
    % dimensionless liquid level at rest (-)
    Psi_0 = H_0;
    % dimensionless liquid level at the vessel wall (-)
    Psi_w = Psi_0 + beta*(Xi_c^2/Xi_w^2)*((1/2) - log(Xi_c^2/Xi_w^2))*N_Fr*D;
    % dimensionless liquid level at the vessel wall (-)
    Psi_b = Psi_0 + beta*(Xi_c^2/Xi_w^2)*((3/2) - log(Xi_c^2/Xi_w^2) - 2*(Xi_w^2/Xi_c^2))*N_Fr*D;

    %% axial coordinate (-)
    syms Xi
    Psi1 = @(Xi) (Psi_w - beta*(2 - (Xi_c^2/Xi_w^2) - (Xi.^2/Xi_c^2))*N_Fr*D);
    Psi2 = @(Xi) (Psi_w - beta*(((Xi_c^2)/(Xi.^2)) - (Xi_c^2/Xi_w^2))*N_Fr*D);
    
    %% numerical computation of the length of vortex shape
    % array with dimensionless radii
    x1 = linspace(0, Xi_c, 500);
    x2 = linspace(Xi_c, Xi_w, 500);
    % step width x1
    d_x1 = (Xi_c - 0)/(length(x1)-1);
    % step width x2
    d_x2 = (Xi_w - Xi_c)/(length(x2)-1);
    % arrays for axial coordinates
    y1 = [];
    y2 = [];
    % arrays for length
    L1_arr = [];
    L2_arr = [];
    L1_arr1 = [];
    L2_arr1 = [];
    
    
    % loop 0 -> Xi_c
    for i=1:length(x1)
        x = x1(i);
        
        % axial coordinate for radius x
        pos = Psi1(x);
        y1 = [y1, pos];
        
        % calculation of the length  
        if i>1
            d_y = pos - y1(end-1);
            L_step = sqrt(d_y^2 + (d_x1*(D/2))^2);
            L1_arr = [L1_arr, L_step];
            % dimensionless
            L_step1 = sqrt(d_y^2 + (d_x1)^2);
            L1_arr1 = [L1_arr1, L_step1];
        end
    end
    
    % loop Xi_c -> Xi_w
    for i=1:length(x2)
        x = x2(i);
        % axial coordinate for radius x
        pos = Psi2(x);
        y2 = [y2, pos];
        
        % calculation of the length  
        if i>1
            d_y = pos - y2(end-1);
            L_step = sqrt(d_y^2 + (d_x2*(D/2))^2);
            L2_arr = [L2_arr, L_step];
            % dimensionless
            L_step1 = sqrt(d_y^2 + (d_x2)^2);
            L2_arr1 = [L2_arr1, L_step1];
        end
    end
    
%     x=[x1*(D/2),x2*(D/2)];
%     y=[y1,y2];
    % length of vortex shape 0 -> Xi_c
    L1 = sum(L1_arr);
    % length of vortex shape Xi_c -> Xi_w
    L2 = sum(L2_arr);
    % total length of vortex shape
    L = (L1+L2);
    % length of dimensionless vortex shape 0 -> Xi_c
    L11 = sum(L1_arr1);
    % length of dimensionless vortex shape Xi_c -> Xi_w
    L21 = sum(L2_arr1);
    
    %% calculation of the emphasis
    % 0 -> Xi_c
    L1_s = (1/L11) * integral(@(Xi) Xi.*sqrt(1+(-beta*(-(2*Xi/(Xi_c^2)))*N_Fr*D).^2), 0, Xi_c)*(D/2);
    % Xi_c -> Xi_w
    L2_s = (1/L21) * integral(@(Xi) Xi.*sqrt(1+(-beta*((-2*Xi_c^2)./(Xi.^3))*N_Fr*D).^2), Xi_c, Xi_w)*(D/2);
    
    %% area of the liquid-gas interface
    S_a_gw = 2*pi*(L1_s*L1 + L2_s*L2);

end
    
    