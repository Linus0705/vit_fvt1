%% function of this script:
% find good starting points for a ternary flash calculation
% - start with a binary mixture of the two substances with the highest mole amounts 
% - add the third frequent substance by increasing the mole amount step by step 
%   as long as the whole mole amount is added and the flash calculation to a correct result converges 
% - optional: plot the diagram which shows the step by step process of adding the third substance
% - return results of the last flash calculation as good starting points
%   for a ternary system with the three highest substances by mass
%%

function [xinit, molecules] = startwertsuche_instatStofftrans(T, p, n, z_0, inp, aggr_state, eos, array, phase, show_graph)

% startwert Suche nur in einem System mit fl�ssigen Komponenten

array(find(aggr_state == 'g')) = []; % bei Init-GGW muss Transferkomponente rausgenommen werden
inp(find(aggr_state == 'g'),:) = [];
n(find(aggr_state == 'g')) = [];
z_0(find(aggr_state == 'g')) = [];
q = length(array); % Zahl an Komponenten ohne Transferkomponente


% put molecules in a vector for easy iteration und rausfinden wo in
% molecules die einzelnen Hauptphasen sind
for i = 1:q
    molecules(i) = array(i).molecules; 
    if molecules(i) == phase (1)
        ph(1) = i;
    elseif molecules(i) == phase (2)
        ph(2) = i;
    elseif q > 2 % wenn mehr als zwei Komponenten wird der Startwert f�r eine dritte Komp (i.e. ph(3)) iterativ angepasst
        ph(3) = i;
    elseif q > 3 % wenn mehr als drei Komponenten wird die Startwertanpassung f�r die Komp mit der h�chsten Stoffmenge durchgef�hrt
        if n(ph(3)) < n(i)
            ph(3) = i;
        end
    end
end

%global composition
z = saftsuite.core.Composition(); % create composition object
z.put(molecules(ph(1)), ph(1));
z.put(molecules(ph(2)), ph(2));

%initial guesses for both phases
xstart(1) = saftsuite.core.Composition(); % create composition object
xstart(1).put(molecules(ph(1)), 0.95);
xstart(1).put(molecules(ph(2)), 0.05);

xstart(2) = saftsuite.core.Composition(); % create composition object
xstart(2).put(molecules(ph(1)), 0.05);
xstart(2).put(molecules(ph(2)), 0.95);


x = flashTp(T, p, z, eos, xstart);

if q == 2
    xinit(1,1) = x(1).get(molecules(1));
    xinit(2,1) = x(1).get(molecules(2));
    xinit(1,2) = x(2).get(molecules(1));
    xinit(2,2) = x(2).get(molecules(2));
    xinit(1:2,3) = [NaN NaN];
    molecules(3) = array(1).molecules;
else

% add component 3
x(1).put(molecules(ph(3)),0);
x(2).put(molecules(ph(3)),0);

delta = 0.01;
N = ceil(z_0(ph(3)) / delta);

%preallocate memory
x1_mainmolec1 = zeros(N,1);
x2_mainmolec1 = zeros(N,1);
x1_mainmolec2 = zeros(N,1);
x2_mainmolec2 = zeros(N,1);
z_mainmolec1 = zeros(N,1);
z_mainmolec2 = zeros(N,1);
flag = zeros(N,1);

%first point is result from binary system
x1_mainmolec1(1) = x(1).get(molecules(ph(1)));
x2_mainmolec1(1) = x(2).get(molecules(ph(1)));
x1_mainmolec2(1) = x(1).get(molecules(ph(2)));
x2_mainmolec2(1) = x(2).get(molecules(ph(2)));
z_mainmolec1(1) = z.get(molecules(ph(1)));
z_mainmolec2(1) = z.get(molecules(ph(2)));



for i = 2:N
    %start with result from previous phase equilibrium calculation
    xstart = x;
    
    %add a bit of component 3 to both phases
    for j = 1:2
        xstart(j).put(molecules(ph(3)),xstart(j).get(molecules(ph(3))) + delta);
        xstart(j).normalize;
    end
    
    %calculate new global composition for phi=0.5 (arbitrarily chosen)
    for k = 1:3
        z.put(molecules(ph(k)), (xstart(1).get(molecules(ph(k))) + xstart(2).get(molecules(ph(k))))/2 );
    end
    
    %calculate phase equilibrium
    [x, flag(i)] = flashTp(T, p, z, eos, xstart);
    
    %if 1==1
    if flag(i)>0
        z_mainmolec1(i) = z.get(molecules(ph(1)));
        z_mainmolec2(i) = z.get(molecules(ph(2)));
        x1_mainmolec1(i) = x(1).get(molecules(ph(1)));
        x2_mainmolec1(i) = x(2).get(molecules(ph(1)));
        x1_mainmolec2(i) = x(1).get(molecules(ph(2)));
        x2_mainmolec2(i) = x(2).get(molecules(ph(2)));
    else
        x1_mainmolec1(i) = NaN;
        x2_mainmolec1(i) = NaN;
        x1_mainmolec2(i) = NaN;
        x2_mainmolec2(i) = NaN;
    end
end

x1_mainmolec3 = 1 - x1_mainmolec1 - x1_mainmolec2;
x2_mainmolec3 = 1 - x2_mainmolec1 - x2_mainmolec2;

xinit(1:3,1) = [x1_mainmolec1(N) x1_mainmolec2(N) x1_mainmolec3(N)];
xinit(1:3,2) = [x2_mainmolec1(N) x2_mainmolec2(N) x2_mainmolec3(N)];

if show_graph == true
    %plot binodal
    figure(1)
    plot(x1_mainmolec2,x1_mainmolec3,x2_mainmolec2,x2_mainmolec3);
    hold on
    for i = 1:length(x1_mainmolec2)
        plot([x1_mainmolec2(i),x2_mainmolec2(i)], [x1_mainmolec3(i),x2_mainmolec3(i)], '--', 'Color',[0,0,0])
    end
    hold off
    xlabel('increasing continous component 1')
    ylabel('increasing third most component [-]')
    title('Ternery diagram of the two continous component an third most component (y-direction)')
    movegui('north')
end

K_hmf=x2_mainmolec3./x1_mainmolec3;
%plot((x1_hmf+x2_hmf)/2,K_hmf)

end
end