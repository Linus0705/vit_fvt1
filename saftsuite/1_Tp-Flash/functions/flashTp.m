function [x, flag, phi]=flashTp(T, p, z, eos, xstart)


rhoLstart(1) = eos.rhoLiquid(T,p,xstart(1));
% rhoLstart(2) = eos.rhoLiquid(T,p,xstart(2));
rhoLstart(2) = eos.rhoVapor(T,p,xstart(2));

%get molecules defined in global composition
%(indexing must remain the same!)
molecules = z.keySet.toArray;
%n is number of molecules
[n,~] = molecules.size;

y0 = zeros(2*n+1,1); % preallocate memory
for i = 1:2
    for j = 1:n
        y0((i-1)*n + j) = log(rhoLstart(i) * xstart(i).get(molecules(j)));
    end
end
%phase split factor
y0(2*n + 1) = 0.5;

%create a function f that takes only y as an argument
f = @(y) flashTpObj(y, T, p, z, eos, molecules); % function of dummy variable y

%set options for solver
options = optimoptions('fsolve');
options.Display='iter-detailed';
options.MaxFunctionEvaluations=1000000;
options.MaxIterations=500;
options.Algorithm='trust-region-dogleg';
%options.Algorithm='levenberg-marquardt';
%options.FiniteDifferenceType='central';
options.StepTolerance=1e-6;
options.FunctionTolerance=1e-6;

[out, fval, flag] = fsolve(f, y0, options);
x(1) = saftsuite.core.Composition();
x(2) = saftsuite.core.Composition();
for i = 1:2
    for j = 1:n
        y0((i-1)*n + j) = log(rhoLstart(i) * xstart(i).get(molecules(j)));
        x(i).put(molecules(j),exp(out((i-1)*n + j)));
    end
    x(i).normalize;
end

phi=out(2*n+1);