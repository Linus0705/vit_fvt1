%% function of this script:
% - code for input declaration
% - all changeable variables are listed
%%

function [T, p, p_part, java_saftsuite, inp, conti, V, xlsx, show_graph] = input_instatStofftrans()
%% temperature and pressure 
% maximale Temperatur, diejenige Temperatur bei der die beiden Hauptphasen
% ihre maximale Fl�ssig-Temperatur haben (bei geg. p)

T = 298.15; %[K]

p_part(1) = 30e5; % Partialdr�cke der Komponenten in [Pa]
p_part(2) = 60e5;

p = sum(p_part); %[Pa]

%% information for initializing the saftsuite

java_saftsuite.xml_name = ".\Parameterdatei_Meoh - Decan - co2 - OME - water - h2.xml";
java_saftsuite.eos = 'PCP-SAFT';
java_saftsuite.path = '.\SAFTsuite_201909262208_r47.jar';


%% declaration of gaseous component input
% inp(1,:) = ["co2"       "Fu_et_al"     p     'g'];   add pressure "p" in column 3 

inp(1,:) = ["co2"       "Fu_et_al"    p_part(1)       'g'];   % component 1 (n = 1)
inp(2,:) = ["h2"        "Teresa"      p_part(2)      'g'];   % component 6 (n = 6)

%% declaration of liquid component input
% for component n: 
% inp(n,1:4) = ["componentName of comp n" ...
%               "segmentName of comp n" ...
%                initial volume of comp n [m^3] ...
%                state of aggregation (g:gaseous l:liquid)]
% e.g.: 
% inp(end+1,:) = ["methanol"  "Kozlowska_et_al"       30e-6   'l'];

inp(end+1,:) = ["methanol"  "Kozlowska_et_al"       0.4e-3   'l'];   % component 2 (n = 2)
inp(end+1,:) = ["n-decane"  "Gross_et_al_n-decane"  0.3e-3   'l'];   % component 3 (n = 3)
inp(end+1,:) = ["OME"       "Kleiner_OME"           0.1e-3    'l'];   % component 4 (n = 4)
inp(end+1,:) = ["water"     "Kleiner_Water"         0.1e-3    'l'];   % component 5 (n = 5)

% conti(1) -> continous component of phase 1 (PAY ATTENTION: phase 1 is heavy phase)
% conti(2) -> continous component of phase 2 (PAY ATTENTION: phase 2 is light phase)
% see above for component numbering; for declarations only allowed to use liquid phases
% e.g.:
% conti(1) = 2; component 2 (n = 2) is continous component of phase 1 (heavy phase)
% conti(2) = 4; component 4 (n = 4) is continous component of phase 2 (light phase)

conti(1) = 3;     % continous component phase 1
conti(2) = 4;     % continous component phase 2
conti(3) = 5;     % transfer component
conti(4) = 1;     % gas component

%% boiling point of Substances (for Wilke-Chang)
% T_b = 273.15 + [NaN 65 174 42 100]; %CO2 has no BP at ambient conditions --> V_m in Mass Transfer directly out of Wilke-Chang paper


%% declaration reactor and stirrer data

% A_R = 3.5e-2; %0.04^2*pi/4;   % [m�] cross section reactor
V.Reaktor = 1.2e-3;%120e-6 % [m^3] volume reactor
% stirrer.n = 300; % [1/min]
% stirrer.d = 0.028; %[m]
% stirrer.v = stirrer.d * pi * stirrer.n / 60; % [m/s]
% stirrer.d32_drop = 0.89e-3; %[m]
% stirrer.d32_bubble = 8.9e-3; %[m]

%% decleration parameters for transfer component from phase 1 to phase 2  
% amount of the transfer component in phase 2 with values between [0,1] at t = 0
% trans_distribution = 0; 

% %% simulink simulation settings
% 
% simSet.simulinkName = 'model_instatStofftrans';
% simSet.solver = 'ode45';
% simSet.stopTime = '4000';
% simSet.simulationMode = "normal";
% 

%% graph at startwertsuche_instatStofftrans
% whether show graph or not

show_graph = false; 


%% declaration name input-sheet
% for creating an input shheet use template "input_template.xlsx"
% xlsx_name: file-name of the input measurements (also used for outputs)

xlsx.name = "neuerBeispieldatensatz_moha.xlsx";

%% further information
% molecular weight has to be [g/mol] -> in xml-file

% only ONE gaseous component at ambient conditions

% wenn gasf�rmige Komponente im input dann VLLE sonst LLE


if p > 1.5e5 && ~any(inp(:,4) == 'g')
    disp(['-------------------------------'])
    error('pressure too high for no gaseous component in the input. reduce pressure or add a gaseous component in _inp_')
end

end

