function [x, flag, phi]=flashTp3Ph(T, p, z, eos, xstart) % Partialdruck ?


rhoLstart(1) = eos.rhoLiquid(T,p,xstart(1));
rhoLstart(2) = eos.rhoLiquid(T,p,xstart(2));
% rhoLstart(3) = eos.rhoVapor(T,p,xstart(3)); %vapor 
rhoLstart(3) = eos.rhoLiquid(T,p,xstart(3)); % supercritical density behaves like liquid density

%get molecules defined in global composition
%(indexing must remain the same!)
molecules = z.keySet.toArray;
%n is number of molecules
[n,~] = molecules.size;

y0 = zeros(3*n+1,1); % preallocate memory
for i = 1:3
    for j = 1:n
        y0( (i-1)*n + j ) = log( rhoLstart(i) * xstart(i).get(molecules(j)) ); %partial densities
    end
end
%phase split factor - NOT the fugacity coefficient
% y0(3*n+1) = 0.3; default
% y0(3*n+2) = 0.3; default
y0(3*n+1) = 0.4;
y0(3*n+2) = 0.2;
%create a function f that takes only y as an argument
% f = @(y) flashTpObj3Ph(y, T, p, z, eos, molecules); % function of dummy variable y
f = @(y) flashTpObj3Ph(y, T, p, z, eos, molecules); % function of dummy variable y

%set options for solver
options = optimoptions('fsolve');
options.Display='final';
options.MaxFunctionEvaluations=1000000;
options.MaxIterations=300; %herabgestezt von 1000000 auf 100_Moritz
options.Algorithm='trust-region-dogleg';
%options.Algorithm='levenberg-marquardt';
%options.FiniteDifferenceType='central';
%options.StepTolerance=1e-12;
%options.FunctionTolerance=1e-12;
options.StepTolerance=1e-6;
options.FunctionTolerance=1e-6;

[out, fval, flag, output]=fsolve(f, y0, options);
it_steps=output.iterations;
x(1)=saftsuite.core.Composition();
x(2)=saftsuite.core.Composition();
x(3)=saftsuite.core.Composition();
for i = 1:3
    for j = 1:n
        y0((i-1)*n+j) = log(rhoLstart(i)*xstart(i).get(molecules(j)));
        x(i).put(molecules(j),exp(out((i-1)*n+j)));
    end
    x(i).normalize;
end
rho(1) = exp(out(1)) + exp(out(2)) + exp(out(3)) + exp(out(4)) + exp(out(5));
rho(2) = exp(out(6)) + exp(out(7)) + exp(out(8)) + exp(out(9)) + exp(out(10));
rho(3) = exp(out(11)) + exp(out(12)) + exp(out(13)) + exp(out(14)) + exp(out(15));
phi=[out(3*n+1) out(3*n+2)];

