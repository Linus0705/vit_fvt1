%% function of this script:
% calculate the liquid-liquid equilibrium at t < 0 (assumption: gaseous phase is added at t=0)
% - remove gaseous component out of vectors
% - execute function for automatized searching for good starting points
% - initialize saftsuite composition-objects
% - calculate LLE with flashTp
% - prepare calculation results for return to main function
%%

function [x_double, x, rho, n_eachphase, V_i, phi] = ggw0_instatStofftrans_VLLE(T, p, p_part, conti, inp, eos, array, V_R, show_graph)
% Berechnung des Stoffgleichgewichts zum Zeipunkt t = 0, vor Zugabe der Transferkomponente

% Definition der Hauptkomponenten der Phasen
phase(1) = array(conti(1)).molecules;
phase(2) = array(conti(2)).molecules;

% Entfernen der Transfer-Komponente
% inp(conti(3),:) = [];
% array(conti(3)) = []; % bei Init-GGW muss Transfer-Komponente rausgenommen werden

% Entfernen der Gas-Komponenten
inp_LLE0 = inp;
index_gas = find(inp(:,4)=='g');
inp_LLE0(index_gas,:) = []; % Herausnehmen der Gaskomponente
%array_VLLE = array; % Unbrauchbar?
array_LLE = array;
array_LLE(index_gas) = [];
array_VLLE_co2 = array;
array_VLLE_co2(2) = [];
array_VLLE_h2 = array;
array_VLLE_h2(1) = []; 

q = size(inp_LLE0,1); % Zahl der fl�ssigen Komponenten ohne Transferkomponente
k = size(inp,1)-size(inp_LLE0,1); %Zahl der Gasf�rmigen Komponenten Komponenten ohne Transferkomponente
xinit = zeros(q,3);

% Auslesen von inp nur ohne Transferkomponente
volumes_LLE = str2double(inp_LLE0(1:q,3));
aggr_state_LLE = inp_LLE0(1:q,4);

for i = 1:q
    rho_init(i) = eos.rhoLiquid(293.15,98880,array_LLE(i).molecules); % Dichte der Stoffe beim Einf�llen im Labor [20 �C, 98880 Pa (Luftdruck bei NGP2)
    n_LLE(i,1) = volumes_LLE(i) * rho_init(i); % Stoffmenge aller 'l'-Komp berechenbar
    xinit(i,3) = 0.05/q;
end

% Startzusammensetzung ohne Gas
z_0 = n_LLE./sum(n_LLE); % mole fraction
zstart = saftsuite.core.Composition();
for i = 1:q
        zstart.put(array_LLE(i).molecules, z_0(i));
end
zstart.normalize;

% GGW-Berechnung f�r Fl�ssig-Fl�ssig-System aus den zwei Konti-Stoffen und dem drittmeisten andere Stoff
% zur automatisierten Startwertsuche f�r folgende Flash-Berechnungen
[x3, molecules] = startwertsuche_instatStofftrans(T, p_part(1), n_LLE, z_0,inp, aggr_state_LLE, eos, array_LLE, phase, show_graph);

% Auslesen der Startwerte und abspeichern in Stoffmengenanteil-Initialiserungs-Vektor
for i = 1:q
    if array_LLE(i).molecules == molecules(1)
        xinit(i,1:2) = x3(1,1:2);
    elseif array_LLE(i).molecules == molecules(2)
        xinit(i,1:2) = x3(2,1:2);
    elseif array_LLE(i).molecules == molecules(3) %&& q > 3
        xinit(i,1:2) = x3(3,1:2);
    else
        xinit(i,1:2) = [0.05 0.05];
    end
end

% �bertragung des LLE auf PCP-SAFT: phase 1 (liquid) & phase 2 (liquid) & phase 3 (gas_co2)
xstart_co2(1) = saftsuite.core.Composition(); % create composition object
xstart_co2(2) = saftsuite.core.Composition(); % create composition object
xstart_co2(3) = saftsuite.core.Composition(); % create composition object

for i = 1:q
    xstart_co2(1).put(array_LLE(i).molecules, xinit(i,1)); 
    xstart_co2(2).put(array_LLE(i).molecules, xinit(i,2));
    xstart_co2(3).put(array_LLE(i).molecules, xinit(i,3));
end

 % Nur f�r CO2
xstart_co2(1).put(array_VLLE_co2(1).molecules, 0.05);
xstart_co2(2).put(array_VLLE_co2(1).molecules, 0.05);
xstart_co2(3).put(array_VLLE_co2(1).molecules, 0.95); % �ndern

xstart_co2(1).normalize;
xstart_co2(2).normalize;
xstart_co2(3).normalize;

% Aufsummieren des Fl�ssig-Volumen und berechnen der Gas-Dichte f�r
% Volumenbilanz des Reaktors
aggr_state = inp(1:q+k,4);
namen = inp(1:q+k,2);
volumes = str2double(inp(1:q+k,3));
V_L = 0; 
rho_gas_composition_co2 = saftsuite.core.Composition();
rho_gas_composition_h2 = saftsuite.core.Composition();

%Berechnung der Dichte f�r CO2 (erste Sch�tzung) H2 entfernen �ndert nichts

for i = 1:q+k
    if  namen(i) == "Fu_et_al"
        rho_gas_composition_co2.put(array(i).molecules, 1); % H2 entfernen? Nein!
    elseif aggr_state(i) == 'l'
        rho_gas_composition_co2.put(array(i).molecules, 0);
        V_L = V_L + volumes(i);
    else
        rho_gas_composition_co2.put(array(i).molecules, 0);
    end
end

rho_gaseous_co2 = eos.rhoVapor(T,p_part(1),rho_gas_composition_co2);

for i = 1:q+k
    if namen(i) == "Teresa"
        rho_gas_composition_h2.put(array(i).molecules, 1);
    elseif aggr_state(i) == 'l' 
        rho_gas_composition_h2.put(array(i).molecules, 0);
    else
        rho_gas_composition_h2.put(array(i).molecules, 0);
    end
end

rho_gaseous_h2 = eos.rhoVapor(T,p_part(2),rho_gas_composition_h2);

% Hinzuf�gen des Gases zur Anfangsstoffmenge
n = zeros(size(inp,1),1);
% n_gaseous = (V_R - V_L) * rho_gaseous_co2;

n(1) = (V_R - V_L) * rho_gaseous_co2; % Berechnung der Stoffmenge CO2
n(2) = (V_R - V_L) * rho_gaseous_h2; % Berechnung der Stoffmenege H2
n(k+1:q+k) = n_LLE;
n(2) = []; % Entferne H2 f�r Volumeniteration (nun k-1)

%Startzusammensetzung mit Gas (nur CO2)
z_0 = n./sum(n); % mole fraction
zstart = saftsuite.core.Composition();
for i = 1:q+k-1
        zstart.put(array_VLLE_co2(i).molecules, z_0(i)); 
end
zstart.normalize;



% Schranke f�r Volumen-Iteration: 1% des Gesamt-Reaktorvolumens (f�r CO2)
volume_invalid = true;
volume_iteration = 0;
epsilon = V_R / 10000;  
disp(['-------------------------------' newline 'calculation and volume iteration VLLE at t = 0:'])

while volume_invalid
    if volume_iteration ~= 0 % ersten Durchlauf ausschlie�en
        xstart_co2 = x;
        if phi(1,1) < 0 || phi(1,1) > 1 || phi(1,2) < 0 || phi(1,2) > 1 || sum(phi) > 1 || flag == 0
            n(conti(4)) = (volume_iteration + 1) * n(conti(4));
            z_0 = n./sum(n); % mole fraction
            for i = 1:q+k-1
                    zstart.put(array_VLLE_co2(i).molecules, z_0(i));
            end
            zstart.normalize;                

        elseif abs(V_ges - V_R) > epsilon
            n(conti(4)) = n(conti(4)) - (V_ges - V_R) * rho(3);
            z_0 = n./sum(n); % mole fraction
            for i = 1:q+k-1
                    zstart.put(array_VLLE_co2(i).molecules, z_0(i));
            end
            zstart.normalize;
        end
    end

    [x, flag, phi] = flashTp3Ph(T, p_part(1), zstart, eos, xstart_co2); 
    rho(1) = eos.rhoLiquid(T,p_part(1),x(1));
    rho(2) = eos.rhoLiquid(T,p_part(1),x(2));
    rho(3) = eos.rhoVapor(T,p_part(1),x(3)); % ?

    for j = 1:3  % j ist die Anzahl der Phasen
        V_m(j) = saftsuite.core.Phase(eos,T,rho(j),x(j)).getVolume;    
        for i = 1:q+k-1
            x_double(i,j) = x(j).get(array_VLLE_co2(i).molecules);
        end
    end
    
    n_eachphase = (x_double(1:q+k-1,1:3) \ n(1:q+k-1,1))'; 
    V_i = n_eachphase.*V_m;
    V_ges = sum(V_i);  % [m^3]

    if abs(V_ges - V_R) < epsilon
        volume_invalid = false;
    end

    volume_iteration = volume_iteration +1
end

[x, flag, phi] = flashTV3Ph(T,p_part(1), V_R, n, zstart, eos, xstart_co2); % Partialdruck %Christoph

% �bertragung des LLE auf PCP-SAFT: phase 1 (liquid) & phase 2 (liquid) & phase 3 (gas_co2)
xstart_h2(1) = saftsuite.core.Composition(); % create composition object
xstart_h2(2) = saftsuite.core.Composition(); % create composition object
xstart_h2(3) = saftsuite.core.Composition(); % create composition object

for i = 1:q
    xstart_h2(1).put(array_LLE(i).molecules, xinit(i,1)); 
    xstart_h2(2).put(array_LLE(i).molecules, xinit(i,2));
    xstart_h2(3).put(array_LLE(i).molecules, xinit(i,3));
end

 % Nur f�r CO2
xstart_h2(1).put(array_VLLE_h2(1).molecules, 0.05);
xstart_h2(2).put(array_VLLE_h2(1).molecules, 0.05);
xstart_h2(3).put(array_VLLE_h2(1).molecules, 0.95); % �ndern

xstart_h2(1).normalize;
xstart_h2(2).normalize;
xstart_h2(3).normalize;

n_co2 = n(1); % CO2 abspeichern
n(1) = (V_R - V_L) * rho_gaseous_h2; %CO2 durch H2 ersetzen

%Startzusammensetzung mit Gas (nur H2)
z_0 = n./sum(n); % mole fraction
zstart = saftsuite.core.Composition();
for i = 1:q+k-1
        zstart.put(array_VLLE_h2(i).molecules, z_0(i)); 
end
zstart.normalize;

% Volumeniteration f�r H2

volume_invalid = true;
volume_iteration = 0;
disp(['-------------------------------' newline 'calculation and volume iteration VLLE at t = 0:'])

while volume_invalid
    if volume_iteration ~= 0 % ersten Durchlauf ausschlie�en
        xstart_h2 = x;
        if phi(1,1) < 0 || phi(1,1) > 1 || phi(1,2) < 0 || phi(1,2) > 1 || sum(phi) > 1 || flag == 0
            n(conti(4)) = (volume_iteration + 1) * n(conti(4));
            z_0 = n./sum(n); % mole fraction
            for i = 1:q+k-1
                    zstart.put(array_VLLE_h2(i).molecules, z_0(i));
            end
            zstart.normalize;                

        elseif abs(V_ges - V_R) > epsilon
            n(conti(4)) = n(conti(4)) - (V_ges - V_R) * rho(3);
            z_0 = n./sum(n); % mole fraction
            for i = 1:q+k-1
                    zstart.put(array_VLLE_h2(i).molecules, z_0(i));
            end
            zstart.normalize;
        end
    end

    [x, flag, phi] = flashTp3Ph(T, p_part(2), zstart, eos, xstart_h2); % Partialdruck ?
    rho(1) = eos.rhoLiquid(T,p_part(2),x(1));
    rho(2) = eos.rhoLiquid(T,p_part(2),x(2));
    rho(3) = eos.rhoVapor(T,p_part(2),x(3));

    for j = 1:3  
        V_m(j) = saftsuite.core.Phase(eos,T,rho(j),x(j)).getVolume;    
        for i = 1:q+k-1
            x_double(i,j) = x(j).get(array_VLLE_h2(i).molecules);
        end
    end
    
    
    n_eachphase = (x_double(1:q+k-1,1:3) \ n(1:q+k-1,1))';
    V_i = n_eachphase.*V_m;
    V_ges = sum(V_i);  % [m^3]

    if abs(V_ges - V_R) < epsilon
        volume_invalid = false;
    end

    volume_iteration = volume_iteration +1
end

%Stoffmengenvektor zusammenf�gen

n_h2 = n(1);
n(1) = n_co2;
n(2) = n_h2;
n(k+1:q+k) = n_LLE;

%Berechnung der gesamten Gasdichte und der gesamten Zusammensetzung

% �bertragung des LLE auf PCP-SAFT: phase 1 (liquid) & phase 2 (liquid) & phase 3 (gas)
xstart(1) = saftsuite.core.Composition(); % create composition object
xstart(2) = saftsuite.core.Composition(); % create composition object
xstart(3) = saftsuite.core.Composition(); % create composition object

for i = 1:q
    xstart(1).put(array_LLE(i).molecules, xinit(i,1)); 
    xstart(2).put(array_LLE(i).molecules, xinit(i,2));
    xstart(3).put(array_LLE(i).molecules, xinit(i,3));
end

for i = 1:k 
    xstart(1).put(array(index_gas(i)).molecules, 0.05);
    xstart(2).put(array(index_gas(i)).molecules, 0.05);
    xstart(3).put(array(index_gas(i)).molecules, 0.95/k); 
end

xstart(1).normalize;
xstart(2).normalize;
xstart(3).normalize;

%Startzusammensetzung mit Gas
z_0 = n./sum(n); % mole fraction
zstart = saftsuite.core.Composition();
for i = 1:q+k
        zstart.put(array(i).molecules, z_0(i)); 
end
zstart.normalize;

    [x, flag, phi] = flashTp3Ph(T, p, zstart, eos, xstart); 
    rho(1) = eos.rhoLiquid(T,p,x(1));
    rho(2) = eos.rhoLiquid(T,p,x(2));
    rho(3) = eos.rhoVapor(T,p,x(3));

    for j = 1:3  
        V_m(j) = saftsuite.core.Phase(eos,T,rho(j),x(j)).getVolume;    
        for i = 1:q+k
            x_double(i,j) = x(j).get(array(i).molecules);
        end
    end
    
n_eachphase = (x_double(1:q+k,1:3) \ n(1:q+k,1))';    
V_i = n_eachphase.*V_m;

[x2, flag2, phi2] = flashTV3Ph(T, p, V, n_eachphase, zstart, eos, xstart);

% 
% if phi < 0 || phi > 1 || flag == 0
%     error('assumption of LLE for calculation at t = 0 failed');
% end
% 
% for j = 1:2
%     rho(j) = eos.rhoLiquid(T,p,x(j));
%     V_m(j) = saftsuite.core.Phase(eos,T,rho(j),x(j)).getVolume;
%     for i = 1:q
%         x_double(i,j) = x(j).get(array(i).molecules);
%     end
% end
% 
% n_eachphase = (x_double(1:2,1:2) \ n(1:2,1))';
% V_i = n_eachphase.*V_m;
% V_i(3) = V_R - sum(V_i(1:2)); % gaseous phase not yet existing
% x_double(1:q,3) = zeros(q,1);
% n_eachphase(3) = 0;
% 
% end
