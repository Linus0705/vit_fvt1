function F = flashTVObj3Ph(VAR, T, V, n_total, z, eos, molecules)

%n is number of molecules
[n,dummy] = molecules.size;

x1 = saftsuite.core.Composition(); % create composition object
x2 = saftsuite.core.Composition(); % create composition object
x3 = saftsuite.core.Composition(); % create composition object

%inputs are
% n    log partial density of one phase
% n    log partial density of other phase
% 1    phase fraction phi
% sum: 2*n+1

for i = 1:n
    x1.put(molecules(i),exp(VAR(i))); % first n entries are partial densities for phase 1
    x2.put(molecules(i),exp(VAR(n+i))); % entries n+1 till 2*n are partial densities for phase 2
    x3.put(molecules(i),exp(VAR(2*n+i))); % entries n+3 till 3*n are partial densities for phase 3
end
phi(1) = VAR(3*n+1);
phi(2) = VAR(3*n+2);

p = VAR(3*n+3);
%now x1 and x2 contain partial densities

%total density is sum of partial density
rho1 = x1.sum();
rho2 = x2.sum();
rho3 = x3.sum();

%normalization of partial densities yields mole fractions
x1.normalize();
x2.normalize();
x3.normalize();


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% equilibrium conditions %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%residuals are
% n    chemical potential
% 2    pressure
% n-1  mass balance
% sum: 2*n+1

%iterate over all species
for i = 1:n
    %chemical potential for all components must be equal in both phases
    try
          mu1 = eos.mures(T,rho1,x1,molecules(i))  + log(x1.get(molecules(i))*rho1);
          mu2 = eos.mures(T,rho2,x2,molecules(i))  + log(x2.get(molecules(i))*rho2);
          mu3 = eos.mures(T,rho3,x3,molecules(i))  + log(x3.get(molecules(i))*rho3);
          F(i) = mu1-mu2;
          F(i+n) = mu2-mu3;
          %F(i+2*n) = mu1-mu3;        
%         F(i) = ( eos.mures(T,rho1,x1,molecules(i)) ...
%                - eos.mures(T,rho2,x2,molecules(i)) ...
%                + log( x1.get(molecules(i)) / x2.get(molecules(i)) * rho1 / rho2 ) ...
%                );

        %molecules(i)
        %f1=eos.f(T,rho1,x1,molecules(i))
        %f2=eos.f(T,rho2,x2,molecules(i))
        %F(i) = (f1-f2);
    catch
        F(i) = NaN;
        F(i+n) = NaN;
        %F(i+2*n) = NaN;
    end
end

%pressure in both phases must be equal to given pressure
try
    p1=eos.p(T,rho1,x1);
    F(2*n+1) = (p - p1) * 1e-5;
catch
    F(2*n+1) = NaN;
end
try
    p2=eos.p(T,rho2,x2);
    F(2*n+2) = (p - p2) * 1e-5;
catch
    F(2*n+2) = NaN;
end

try
    p3=eos.p(T,rho3,x3);
    F(2*n+3) = (p - p3) * 1e-5;
catch
    F(2*n+3) = NaN;
end

%volume balance
try
    F(2*n+4) = (V - sum(n_total).*((phi(1)/rho1) - (phi(2)/rho2) - ((1-phi(1)-phi(2))/rho3))) / V;
catch
    F(2*n+4) = NaN;
end

%mole balance must be fulfilled for n-1 components
for i = 1:n-1
    try
        F(2*n+4+i) = (phi(1)*x1.get(molecules(i)) + phi(2)*x2.get(molecules(i)) + (1-phi(1)-phi(2))*x3.get(molecules(i)) - z.get(molecules(i)))*1e2;
    catch
        F(2*n+4+i) = NaN;
    end
end


