function funs = gen_pressure_database(nCstrConc, config, phase)
    funs.rhocalc=@rhocalc;
    
    % ADDME  Generates Pressure database.
    %   
    % generates pressure of octanol-phase, water-phase and vapour using SAFTSuite
    % calculation and writes in excel sheet 'pressure_<phase>.xlsx'. Inputs
    % are temperature, molar density and mole fractions.
    % 
    % works for Windows and Linux
    % needs 'Optimization Toolbox' and 'DataFrame - TimeFrame'
    % for 'Parallel Computing Toolbox' optimized, but not necessary
    % 
    % Outputs:
    %   'input\pressure_<phase>.xlsx'
  
    % get PCP-SAFT instance
    eos = saftsuite.core.EOSRegistry.getInstance.getEOS(config.JAVA_SAFTSUITE.eos);
    projectfile = saftsuite.core.data.ProjectFile();
    projectfile.read(java.io.File(config.JAVA_SAFTSUITE.xml_name));

    % get molecules from project file
    h2 = projectfile.getComponent('h2').getMolecule("Ghosh_et_al");
    co2 = projectfile.getComponent('co2').getMolecule("Fu_et_al");
    oct = projectfile.getComponent('octanol').getMolecule("Umer et al._octanol_2B");
    h2o = projectfile.getComponent('water').getMolecule("TeKa_2020_water_4C");
    meoh = projectfile.getComponent('methanol').getMolecule("Mourah_et_al_3B_Version2");
    
    %% create random bounded feed composition
    % create random matrix
    rn = rand(100000,5);
    
    % Mole Fractions of H2, CO2, Oct, H2O, MeOH
    if phase=="vapor" 
        % define mole fraction boundary for Vapour
        lb = [0 0 0 0 0];
        ub = [1 1 0 0 0];
    elseif phase=="liquid_h2o" 
        % define mole fraction boundary for Liquid - H2O
        lb = [0 0 0 0.6 0];
        ub = [0.2 0.2 0.1 1 0.2];
    elseif phase=="liquid_oct"
        % define mole fraction boundary for Liquid - Octanol
        lb = [0 0 0.5 0 0];
        ub = [0.2 0.2 1 0.3 0.2];
    end

    db = ub-lb;
    
    % get random values in boundary
    rn= lb+db.*rn;
    
    % check sum of mole fractions
    csum = sum(rn,2);
    
    % kill rowsums below 0.95 and above 1.05
    [hit,~] = find(csum>0.95 & csum<1.05);
    rn=rn(hit,:);
    csum = sum(rn,2);
    size(rn,1);
    
    % normalize mole fraction to rowsum of 1
    rn= rn./csum;

    % Check mins and maxs
    struct_min = struct();
    struct_max = struct();
    struct_min.h2 = min(rn(1:100,1));
    struct_max.h2 = max(rn(1:100,1));
    struct_min.co2 = min(rn(1:100,2));
    struct_max.co2 = max(rn(1:100,2));
    struct_min.oct = min(rn(1:100,3));
    struct_max.oct = max(rn(1:100,3));
    struct_min.h2o = min(rn(1:100,4));
    struct_max.h2o = max(rn(1:100,4));
    struct_min.meoh = min(rn(1:100,5));
    struct_max.meoh = max(rn(1:100,5));

    % Add fraction for pure water and for max value in liquid h2o phase
    if phase=="liquid_h2o"
        [~, idx] = max(rn(:,4));
        rn = [rn(idx,:); rn];
        rn = [0 0 0 1 0; rn];
    end

    % Add fraction for pure octanol and for max value in liquid oct phase
    if phase=="liquid_oct"
        [~, idx] = max(rn(:,3));
        rn = [rn(idx,:); rn];
        rn = [0 0 1 0 0; rn];
    end

    
    %% initialize matrix p_ij (first row gets deleted in line 121)
    p_ij=([0 0 0 0 0 0 0 0]);
    
    %% create equilibrium mole fractions
    iend=3;
    jend=10;
    for i=1:iend %1:9 --> 5*i
        for j=1:jend %1:10 --> 10*j
            parfor k=1:nCstrConc % size(rn,1)
                % input
                T = 383.15+10*i; % [K]
                if phase=="vapor"
                    rho_M_i = (1500 + j*600); % [mol/m^3_phase]
                elseif phase=="liquid_h2o"
                    rho_M_i = (20000 + j*3500); % [mol/m^3_phase]
                elseif phase=="liquid_oct"
                    rho_M_i = (3000 + j*500); % [mol/m^3_phase]
                end 
                
                
                % feed
                x = [rn(k,1) rn(k,2) rn(k,3) rn(k,4) rn(k,5)]; % H2 in mol/mol

                p = pcalc(T,rho_M_i,x,config);
                
                % write pressure in matrix K_ij
                p_ij_add = ([T  rho_M_i  x(1)  x(2)  x(3)  x(4)  x(5)  ...
                    p]);

                p_ij=[p_ij; p_ij_add];
                
                t=datetime('now');

                L=[datestr(t),'    : ',num2str(num2str(k+(j-1)*nCstrConc+(i-1)*jend*nCstrConc)),'/',num2str(nCstrConc*iend*jend),' finished'];
                disp(L);
            end
        end
    end
    
    % create dataframe
    p_ij(1,:)=[];
    df_p = frames.DataFrame(p_ij , ...
                    [],["temperature [K]" "molar_density [mol/m^3_phase]" "x_cstr_h2 [mol/mol]" "x_cstr_co2 [mol/mol]" "x_cstr_oct [mol/mol]" "x_cstr_h2o [mol/mol]" "x_cstr_meoh [mol/mol]" ...
                    "p [Pa]"]);
    
    if isfile(config.INPUT_PATH + '\pressure_' + phase + '.xlsx') & ispc
        delete (config.INPUT_PATH + '\pressure_' + phase + '.xlsx');
    elseif isfile(config.INPUT_PATH + '/pressure_' + phase + '.xlsx') & isunix
        delete(config.INPUT_PATH + '/pressure_' + phase + '.xlsx');
    end

    writetable(df_p.t, config.INPUT_PATH + '/pressure_' + phase + '.xlsx');

end
 
function p = pcalc(T,rho_M_i,x,config)

    % ADDME  Computes pressure for oct or h2o liquid phase or vapour.
    %   
    % Inputs:
    %   T           :   temperature in [K]
    %   rho_M_i     :   molar density of the phase [mol/m^3_phase]
    %   x(i)        :   feed mole fractions with i=5 [mol/mol]
    %   config      :   structure with config variables
    %
    % Outputs:
    %   p_i         :   pressure of phase i [Pa]
    %
    % Needs Package 'Optimization Toolbox R2021b'
    
    % get PCP-SAFT instance
    javaaddpath(config.SAFTSUITE_PATH + "/1_Tp-Flash/SAFTsuite_202012042120_r51.jar");
    eos = saftsuite.core.EOSRegistry.getInstance.getEOS(config.JAVA_SAFTSUITE.eos);
    projectfile = saftsuite.core.data.ProjectFile();
    projectfile.read(java.io.File(config.JAVA_SAFTSUITE.xml_name));
    
     % get molecules from project file
    h2 = projectfile.getComponent('h2').getMolecule("Ghosh_et_al");
    co2 = projectfile.getComponent('co2').getMolecule("Fu_et_al");
    oct = projectfile.getComponent('octanol').getMolecule("Umer et al._octanol_2B");
    h2o = projectfile.getComponent('water').getMolecule("TeKa_2020_water_4C");
    meoh = projectfile.getComponent('methanol').getMolecule("Mourah_et_al_3B_Version2");
    
    %% Definition of Feed
    x_phase = saftsuite.core.Composition();    
    x_phase.put(h2,x(1));
    x_phase.put(co2,x(2));
    x_phase.put(oct,x(3));  
    x_phase.put(h2o,x(4));
    x_phase.put(meoh,x(5));
    x_phase.normalize;
    
    %% Pressure-Calculation 
    try
        p = eos.p(T,rho_M_i,x_phase); % [Pa]
    catch exception
        p = NaN;
    end
    
end