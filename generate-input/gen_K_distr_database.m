function funs = gen_K_distr_database(nCstrConc, config)
    funs.flashcalc=@flashcalc;
    funs.getDensity=@getDensity;
    
    % ADDME  Computes equilibrium of feed at specific T,p.
    %   
    % generates distribution coefficients K using Tp-Flash (3Ph) SAFTSuite
    % calculation and writes it in excel sheet 'distribution_coefficients_K.xlsx'
    % 
    % works for Windows and Linux
    % needs 'Optimization Toolbox' and 'DataFrame - TimeFrame'
    % for 'Parallel Computing Toolbox' optimized, but not necessary
    % 
    % Outputs:
    %   'input\distribution_coefficients_K.xlsx
    
    % get PCP-SAFT instance
%     eos = saftsuite.core.EOSRegistry.getInstance.getEOS(config.JAVA_SAFTSUITE.eos);
%     projectfile = saftsuite.core.data.ProjectFile();
%     projectfile.read(java.io.File(config.JAVA_SAFTSUITE.xml_name));
% 
%     % get molecules from project file
%     h2 = config.SAFTSUITE_PROJECTFILE.getComponent('h2').getMolecule("Ghosh_Chapman_2003_h2");
%     co2 = config.SAFTSUITE_PROJECTFILE.getComponent('co2').getMolecule("Khalifa_2018_co2_2B");
%     oct = config.SAFTSUITE_PROJECTFILE.getComponent('octanol').getMolecule("Roman-Ramirez_2010_octanol_2B");
%     h2o = config.SAFTSUITE_PROJECTFILE.getComponent('water').getMolecule("Rehner_2020_water_2B");
%     meoh = config.SAFTSUITE_PROJECTFILE.getComponent('methanol').getMolecule("Khalifa_2018_MeOH_2B");

    %% create random bounded feed composition
    % create random matrix
    rn = rand(10000,5);
    
    % define boundary - H2, CO2, Oct, H2O, MeOH
    lb = [0    0    0.05 0.75 0   ];
    ub = [0.15 0.1  0.15 0.9  0.05];
    db = ub-lb;
    
    % get random values in boundary
    rn= lb+db.*rn;
    
    % check sum of mole fractions
    csum = sum(rn,2);
    
    % kill rowsums below 0.95 and above 1.05
    [hit,~] = find(csum>0.95 & csum<1.05);
    rn=rn(hit,:);
    csum = sum(rn,2);
    size(rn,1);
    
    % normalize mole fraction to rowsum of 1
    rn= rn./csum;
    rn
    
    %% initialize matrix K_ij (first row gets deleted in line 121)
    K_ij=([0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]);
    
    %% create equilibrium mole fractions
    iend=3;
    jend=10;
    for i=1:iend %1:9 --> 5*i
        for j=1:jend %1:10 --> 10*j
            parfor k=1:nCstrConc % size(rn,1)
                % input
                T = 383.15+10*i; %[K]
                p = (50+j*10) * 1e5; %[Pa]
                .
                .
                .
                
                % feed
                x = [rn(k,1) rn(k,2) rn(k,3) rn(k,4) rn(k,5)]; % H2 in mol/mol

                [xeq, rho1, rho2, rho3] = flashcalc(T,p,x,config); % rho1 - water, rho2 - organic, rho3 - gas

                try
                    K_wo_h2 = (rho2*xeq(1,2))/(rho1*xeq(1,1));
                    K_wo_co2 = (rho2*xeq(2,2))/(rho1*xeq(2,1));
                    K_wo_oct = (rho2*xeq(3,2))/(rho1*xeq(3,1));
                    K_wo_h2o = (rho2*xeq(4,2))/(rho1*xeq(4,1));
                    K_wo_meoh = (rho2*xeq(5,2))/(rho1*xeq(5,1));
    
                    K_gw_h2 = (rho3*xeq(1,3))/(rho1*xeq(1,1));
                    K_gw_co2 = (rho3*xeq(2,3))/(rho1*xeq(2,1));
                    K_gw_oct = (rho3*xeq(3,3))/(rho1*xeq(3,1));
                    K_gw_h2o = (rho3*xeq(4,3))/(rho1*xeq(4,1));
                    K_gw_meoh = (rho3*xeq(5,3))/(rho1*xeq(5,1));

                catch exception
                    % Set all values to NaN
                    K_wo_h2 = NaN;
                    K_wo_co2 = NaN;
                    K_wo_oct = NaN;
                    K_wo_h2o = NaN;
                    K_wo_meoh = NaN;
                    K_gw_h2 = NaN;
                    K_gw_co2 = NaN;
                    K_gw_oct = NaN;
                    K_gw_h2o = NaN;
                    K_gw_meoh = NaN;
                end
                
               % write distribution coefficients K in matrix K_ij
               K_ij_add = ([T  p  x(1)  x(2)  x(3)  x(4)  x(5)  ...
                    K_wo_h2 K_wo_co2 K_wo_oct K_wo_h2o K_wo_meoh ...
                    K_gw_h2 K_gw_co2 K_gw_oct K_gw_h2o K_gw_meoh]);

                K_ij=[K_ij; K_ij_add];
                
                t=datetime('now');
                L=[datestr(t),'    : ',num2str(num2str(k+(j-1)*nCstrConc+(i-1)*jend*nCstrConc)),'/',num2str(nCstrConc*iend*jend),' finished'];
                disp(L);
             end
        end
    end
    
    % create dataframe
    K_ij(1,:)=[];
    df_K = frames.DataFrame(K_ij , ...
                    [],["temperature [K]" "pressure [Pa]" "x_cstr_h2 [mol/mol]" "x_cstr_co2 [mol/mol]" "x_cstr_oct [mol/mol]" "x_cstr_h2o [mol/mol]" "x_cstr_meoh [mol/mol]" ...
                    "K_wo_h2 [c_org/c_water]" "K_wo_co2 [c_org/c_water]" "K_wo_oct [c_org/c_water]" "K_wo_h2o [c_org/c_water]" "K_wo_meoh [c_org/c_water]" ...
                    "K_gw_h2 [c_gas/c_water]" "K_gw_co2 [c_gas/c_water]" "K_gw_oct [c_gas/c_water]" "K_gw_h2o [c_gas/c_water]" "K_gw_meoh [c_gas/c_water]"]);
    
    if isfile(config.INPUT_PATH + '\distribution_coefficients_K.xlsx') & ispc
        delete (config.INPUT_PATH + '\distribution_coefficients_K.xlsx');
    elseif isfile(config.INPUT_PATH + '/distribution_coefficients_K.xlsx') & isunix
        delete(config.INPUT_PATH + '/distribution_coefficients_K.xlsx');
    end

    writetable(df_K.t, config.INPUT_PATH + '/distribution_coefficients_K.xlsx');

end
 
function [comp, rho1, rho2, rho3] = flashcalc(T,p,x, config)

    % ADDME  Computes equilibrium of feed at specific T,p.
    %   
    % Inputs:
    %   T           :   temperature in [K]
    %   p           :   pressure [Pa]
    %   x(i)        :   feed mole fractions with i=5 [mol/mol]
    %   config      :   structure with config variables
    %
    % Outputs:
    %   x_eq(i,j)   :   mole fraction of component i in phase j as simple matrix [mol/mol]
    %
    % Needs Package 'Optimization Toolbox R2021b'
    
    % get PCP-SAFT instance
    javaaddpath(config.SAFTSUITE_PATH + "/1_Tp-Flash/SAFTsuite_202012042120_r51.jar");
    eos = saftsuite.core.EOSRegistry.getInstance.getEOS(config.JAVA_SAFTSUITE.eos);
    projectfile = saftsuite.core.data.ProjectFile();
    projectfile.read(java.io.File(config.JAVA_SAFTSUITE.xml_name));
    
    % get molecules from project file
%     h2 = projectfile.getComponent('h2').getMolecule("Ghosh_Chapman_2003_h2");
%     co2 = projectfile.getComponent('co2').getMolecule("Khalifa_2018_co2_2B");
%     oct = projectfile.getComponent('octanol').getMolecule("Roman-Ramirez_2010_octanol_2B");
%     h2o = projectfile.getComponent('water').getMolecule("Rehner_2020_water_2B");
%     meoh = projectfile.getComponent('methanol').getMolecule("Khalifa_2018_MeOH_2B");
    h2 = projectfile.getComponent('h2').getMolecule(config.SAFTSUITE_MOLECULES.strname_h2);
    co2 = projectfile.getComponent('co2').getMolecule(config.SAFTSUITE_MOLECULES.strname_co2);
    oct = projectfile.getComponent('octanol').getMolecule(config.SAFTSUITE_MOLECULES.strname_oct);
    h2o = projectfile.getComponent('water').getMolecule(config.SAFTSUITE_MOLECULES.strname_h2o);
    meoh = projectfile.getComponent('methanol').getMolecule(config.SAFTSUITE_MOLECULES.strname_meoh);
    
    
    %% Definition of Feed
    z = saftsuite.core.Composition();
    
    z.put(h2,x(1));
    z.put(co2,x(2));
    z.put(oct,x(3));  
    z.put(h2o,x(4));
    z.put(meoh,x(5));
    z.normalize;
    
    %% Definition of initial values
    
    %polar phase (=heavy phase) - Water-Phase
    xstart(1) = saftsuite.core.Composition();
    xstart(1).put(h2,0.00001);
    xstart(1).put(co2,0.0004);
    xstart(1).put(oct,0.0004);
    xstart(1).put(h2o,0.999);
    xstart(1).put(meoh,0.0001);
    xstart(1).normalize;
    
    %nonpolar phase (=light phase) - Octanol-Phase (Reaktionphase)
    xstart(2) = saftsuite.core.Composition();
    xstart(2).put(h2,0.0040);
    xstart(2).put(co2,0.08);
    xstart(2).put(oct,0.8);
    xstart(2).put(h2o,0.0004);
    xstart(2).put(meoh,0.2);
    xstart(2).normalize;
    
    % supercritical (liquid) phase
    xstart(3) = saftsuite.core.Composition();
    xstart(3).put(h2,0.5);
    xstart(3).put(co2,0.5);
    xstart(3).put(oct,0.0001);
    xstart(3).put(h2o,0.0001);
    xstart(3).put(meoh,0.0001);
    xstart(3).normalize;
    
    %% Flash-Calculation and processing results
    rhoLstart(1) = eos.rhoLiquid(T,p,xstart(1)); % [mol/m^3]
    rhoLstart(2) = eos.rhoLiquid(T,p,xstart(2));
    % rhoLstart(3) = eos.rhoVapor(T,p,xstart(3)) %vapor 
    rhoLstart(3) = eos.rhoVapor(T,p,xstart(3)); % supercritical density behaves like liquid density

    try
        [x_java, flag, phi] = flashTp3Ph(T, p, z, eos, xstart);
        for i = 1:3
            x_eq(1,i) = x_java(i).get(h2);
            x_eq(2,i) = x_java(i).get(co2);
            x_eq(3,i) = x_java(i).get(oct);
            x_eq(4,i) = x_java(i).get(h2o);
            x_eq(5,i) = x_java(i).get(meoh);
        end
    
    catch CalculationFailedException
        for i = 1:3
            disp( getReport( CalculationFailedException, 'extended', 'hyperlinks', 'on' ) )
            x_eq(1,i) = NaN;
            x_eq(2,i) = NaN;
            x_eq(3,i) = NaN;
            x_eq(4,i) = NaN;
            x_eq(5,i) = NaN;
        end
    end
    
    if flag==0
        disp("flag="+flag+": prematurely finished!");
        for i = 1:3
            x_eq(1,i) = -1;
            x_eq(2,i) = -1;
            x_eq(3,i) = -1;
            x_eq(4,i) = -1;
            x_eq(5,i) = -1;
        end
    end
    
    x_eq_comp1=saftsuite.core.Composition();
    x_eq_comp2=saftsuite.core.Composition();
    x_eq_comp3=saftsuite.core.Composition();
    x_eq_comp1.put(h2,x_eq(1,1));
    x_eq_comp1.put(co2,x_eq(2,1));
    x_eq_comp1.put(oct,x_eq(3,1));
    x_eq_comp1.put(h2o,x_eq(4,1));
    x_eq_comp1.put(meoh,x_eq(5,1));
    x_eq_comp2.put(h2,x_eq(1,2));
    x_eq_comp2.put(co2,x_eq(2,2));
    x_eq_comp2.put(oct,x_eq(3,2));
    x_eq_comp2.put(h2o,x_eq(4,2));
    x_eq_comp2.put(meoh,x_eq(5,2));
    x_eq_comp3.put(h2,x_eq(1,3));
    x_eq_comp3.put(co2,x_eq(2,3));
    x_eq_comp3.put(oct,x_eq(3,3));
    x_eq_comp3.put(h2o,x_eq(4,3));
    x_eq_comp3.put(meoh,x_eq(5,3));
    
    if x_eq_comp1.get(h2)<0   %if iteration prematurely finished
        rho1 = 1;
        rho2 = -1;
        rho3 = 1;
    elseif isnan(x_eq_comp1.get(h2))
        rho1 = NaN;
        rho2 = NaN;
        rho3 = NaN;
    else
        rho1 = eos.rhoLiquid(T,p,x_eq_comp1); %water
        rho2 = eos.rhoLiquid(T,p,x_eq_comp2); %organic
        rho3 = eos.rhoVapor(T,p,x_eq_comp3);  %gas
    end

    comp = x_eq;
    
end