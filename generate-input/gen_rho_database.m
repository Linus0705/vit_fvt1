function funs = gen_rho_database(nCstrConc, config, phase)
    funs.rhocalc=@rhocalc;
    
    % ADDME  Computes molar density.
    %   
    % generates molar density of liquid and vapour using SAFTSuite
    % calculation and writes in excel sheet 'rho_<phase>.xlsx'
    % 
    % works for Windows and Linux
    % needs 'Optimization Toolbox' and 'DataFrame - TimeFrame'
    % for 'Parallel Computing Toolbox' optimized, but not necessary
    % 
    % Outputs:
    %   'input\rho_<phase>.xlsx'
    
    %% create random bounded feed composition
    % create random matrix
    rn = rand(100000,5);
    
    if phase=="vapor" 
        % define boundary for Vapour
        lb = [0 0 0 0 0];
        ub = [1 1 0 0 0];
    elseif phase=="liquid_h2o" 
        % define boundary for Liquid - H2O
        lb = [0 0 0 0.6 0];
        ub = [0.2 0.2 0.1 1 0.2];
    elseif phase=="liquid_oct"
        % define boundary for Liquid - Octanol
        lb = [0 0 0.5 0 0];
        ub = [0.2 0.2 1 0.3 0.2];
    end

    db = ub-lb;
    
    % get random values in boundary
    rn= lb+db.*rn;
    
    % check sum of mole fractions
    csum = sum(rn,2);
    
    % kill rowsums below 0.95 and above 1.05
    [hit,~] = find(csum>0.95 & csum<1.05);
    rn=rn(hit,:);
    csum = sum(rn,2);
    size(rn,1);
    
    % normalize mole fraction to rowsum of 1
    rn= rn./csum;

    % Check mins and maxs
    struct_min = struct();
    struct_max = struct();
    struct_min.h2 = min(rn(1:100,1));
    struct_max.h2 = max(rn(1:100,1));
    struct_min.co2 = min(rn(1:100,2));
    struct_max.co2 = max(rn(1:100,2));
    struct_min.oct = min(rn(1:100,3));
    struct_max.oct = max(rn(1:100,3));
    struct_min.h2o = min(rn(1:100,4));
    struct_max.h2o = max(rn(1:100,4));
    struct_min.meoh = min(rn(1:100,5));
    struct_max.meoh = max(rn(1:100,5));
    struct_min
    struct_max

    % Add fraction for pure water and for max value in liquid h2o phase
    if phase=="liquid_h2o"
        [~, idx] = max(rn(:,4));
        rn = [rn(idx,:); rn];
        rn = [0 0 0 1 0; rn];
    end

    % Add fraction for pure octanol and for max value in liquid oct phase
    if phase=="liquid_oct"
        [~, idx] = max(rn(:,3));
        rn = [rn(idx,:); rn];
        rn = [0 0 1 0 0; rn];
    end

    
    %% initialize matrix rho_ij (first row gets deleted in line 121)
    rho_ij=([0 0 0 0 0 0 0 0 0]);
    
    %% create equilibrium mole fractions
    iend=3;
    jend=10;
    for i=1:iend %1:9 --> 5*i
        for j=1:jend %1:10 --> 10*j
            parfor k=1:nCstrConc % size(rn,1)
                % input
                T = 383.15+10*i; %[K]
                p = (50+j*10) * 1e5; %[Pa]
                
                % feed
                x = [rn(k,1) rn(k,2) rn(k,3) rn(k,4) rn(k,5)]; % H2 in mol/mol

                [rho_liq, rho_vap] = rhocalc(T,p,x,config,phase);
                
                % write computed molar density in matrix K_ij
                rho_ij_add = ([T  p  x(1)  x(2)  x(3)  x(4)  x(5)  ...
                    rho_liq rho_vap]);

                rho_ij=[rho_ij; rho_ij_add];
                
                t=datetime('now');

                [rho_liq, rho_vap]

                L=[datestr(t),'    : ',num2str(num2str(k+(j-1)*nCstrConc+(i-1)*jend*nCstrConc)),'/',num2str(nCstrConc*iend*jend),' finished'];
                disp(L);
             end
        end
    end
    
    % create dataframe
    rho_ij(1,:)=[];
    df_rho = frames.DataFrame(rho_ij , ...
                    [],["temperature [K]" "pressure [Pa]" "x_cstr_h2 [mol/mol]" "x_cstr_co2 [mol/mol]" "x_cstr_oct [mol/mol]" "x_cstr_h2o [mol/mol]" "x_cstr_meoh [mol/mol]" ...
                    "rho_liq [mol/m^3_phase]" "rho_vap [mol/m^3_phase]"]);
    
    if isfile(config.INPUT_PATH + '\rho_' + phase + '.xlsx') & ispc
        delete (config.INPUT_PATH + '\rho_' + phase + '.xlsx');
    elseif isfile(config.INPUT_PATH + '/rho_' + phase + '.xlsx') & isunix
        delete(config.INPUT_PATH + '/rho_' + phase + '.xlsx');
    end

    writetable(df_rho.t, config.INPUT_PATH + '/rho_' + phase + '.xlsx');

end
 
function [rho_liq, rho_vap] = rhocalc(T,p,x,config,phase)

    % ADDME  Computes molar density for liquid and vapour.
    %   
    % Inputs:
    %   T           :   temperature in [K]
    %   p           :   pressure [Pa]
    %   x(i)        :   feed mole fractions with i=5 [mol/mol]
    %   config      :   structure with config variables
    %
    % Outputs:
    %   rho_i       :   molar density of phase i [mol/m^3_phase]
    %
    % Needs Package 'Optimization Toolbox R2021b'
    
    % get PCP-SAFT instance
    javaaddpath(config.SAFTSUITE_PATH + "/1_Tp-Flash/SAFTsuite_202012042120_r51.jar");
    eos = saftsuite.core.EOSRegistry.getInstance.getEOS(config.JAVA_SAFTSUITE.eos);
    projectfile = saftsuite.core.data.ProjectFile();
    projectfile.read(java.io.File(config.JAVA_SAFTSUITE.xml_name));
    
%     h2 = projectfile.getComponent('h2').getMolecule("Ghosh_Chapman_2003_h2");
%     co2 = projectfile.getComponent('co2').getMolecule("Khalifa_2018_co2_2B");
%     oct = projectfile.getComponent('octanol').getMolecule("Roman-Ramirez_2010_octanol_2B");
%     h2o = projectfile.getComponent('water').getMolecule("Rehner_2020_water_2B");
%     meoh = projectfile.getComponent('methanol').getMolecule("Khalifa_2018_MeOH_2B");
    h2 = projectfile.getComponent('h2').getMolecule(config.SAFTSUITE_MOLECULES.strname_h2);
    co2 = projectfile.getComponent('co2').getMolecule(config.SAFTSUITE_MOLECULES.strname_co2);
    oct = projectfile.getComponent('octanol').getMolecule(config.SAFTSUITE_MOLECULES.strname_oct);
    h2o = projectfile.getComponent('water').getMolecule(config.SAFTSUITE_MOLECULES.strname_h2o);
    meoh = projectfile.getComponent('methanol').getMolecule(config.SAFTSUITE_MOLECULES.strname_meoh);
    
    %% Definition of Feed
    x_phase = saftsuite.core.Composition();    
    x_phase.put(h2,x(1));
    x_phase.put(co2,x(2));
    x_phase.put(oct,x(3));  
    x_phase.put(h2o,x(4));
    x_phase.put(meoh,x(5));
    x_phase.normalize;
    
    %% Rho-Calculation
    if phase=="liquid_h2o" || phase=="liquid_oct"
        try
            rho_liq = eos.rhoLiquid(T,p,x_phase); % [mol/m^3]
        catch exception
            rho_liq = NaN;
        end
        rho_vap = NaN;
    end

    if phase=="vapor" 
        try
            rho_vap = eos.rhoVapor(T,p,x_phase); % [mol/m^3]
        catch exception
            rho_vap = NaN;
        end
        
        rho_liq = NaN;
    end
    
end