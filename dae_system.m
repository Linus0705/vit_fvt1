function dy = dae_system(t,y,config,params,inputs,inits,dae,file_logger,df_sigma_T,nist_viscosity_gprMdls,K_gprMdls,rho_gprMdls, kinetic_fit, CSTR_sets)

%file_logger.info('Starting dae_system ...');
file_logger.info(['t: ' num2str(t)]);
t;
%% Clear dae_system workspace
% if t == 0
%     clear t_preval
%     clear n_o_preval
%     clear n_o_h2_preval
%     clear n_o_co2_preval
%     clear n_o_h2o_preval
%     clear n_o_meoh_preval
% end


%% Initial values
n_g = y(1);
n_g_h2 = y(2);
n_aq = y(3);
n_aq_h2 = y(4);
n_aq_co2 = y(5);
n_aq_oct = y(6);
n_aq_meoh = y(7);
n_o = y(8);
n_o_h2 = y(9);
n_o_co2 = y(10);
n_o_h2o = y(11);
n_o_meoh = y(12);

if kinetic_fit.isKineticFit
    % Algebraic variable pressure is part of output in solver
    p = y(13);
    p_old = p; % for writing the algebraic equation
end

persistent solver_res_alg_var

if isempty(solver_res_alg_var) 
    solver_res_alg_var = struct();
    solver_res_alg_var.t(1,1) = 0;

    solver_res_alg_var.N_dot_g_out(1,1) = 0;
    solver_res_alg_var.N_dot_aq_out(1,1) = 0;
    solver_res_alg_var.N_dot_o_out(1,1) = 0;
    solver_res_alg_var.N_dot_g_in(1,1) = 0;
    solver_res_alg_var.N_dot_aq_in(1,1) = 0;
    solver_res_alg_var.N_dot_o_in(1,1) = 0;

    solver_res_alg_var.M_g_avg(1,1) = 0;
    solver_res_alg_var.M_aq_avg(1,1) = 0;
    solver_res_alg_var.M_o_avg(1,1) = 0;
    solver_res_alg_var.M_o_avg_in(1,1) = 0;

    solver_res_alg_var.x_o_h2_in(1,1) = 0;
    solver_res_alg_var.x_o_co2_in(1,1) = 0;
    solver_res_alg_var.x_o_oct_in(1,1) = 0;
    solver_res_alg_var.x_o_h2o_in(1,1) = 0;
    solver_res_alg_var.x_o_meoh_in(1,1) = 0;
    
    solver_res_alg_var.V_dot_g_in(1,1) = 0;
    solver_res_alg_var.V_dot_g_out(1,1) = 0;
    solver_res_alg_var.V_dot_aq_out(1,1) = 0;
    solver_res_alg_var.V_dot_o_out(1,1) = 0;
    solver_res_alg_var.V_dot_purge(1,1) = 0;

    solver_res_alg_var.V_g(1,1) = 0;
    solver_res_alg_var.V_aq(1,1) = 0;
    solver_res_alg_var.V_o(1,1) = 0;
    solver_res_alg_var.p_is(1,1) = 0;
    solver_res_alg_var.n_o_meoh_reaction(1,1) = 0;
    solver_res_alg_var.reached_max_co2_limit(1,1) = 0;

    solver_res_alg_var.rho_M_g(1,1) = 0;
    solver_res_alg_var.rho_M_aq(1,1) = 0;
    solver_res_alg_var.rho_M_o(1,1) = 0;
    solver_res_alg_var.rho_M_o_in(1,1) = 0;
end





%% Get algebraic variables - Stated with a constitutive equation

% Closing Conditions
n_g_co2 = n_g - n_g_h2;
n_aq_h2o = n_aq - (n_aq_h2 + n_aq_co2 + n_aq_oct + n_aq_meoh);
n_o_oct = n_o - (n_o_h2 + n_o_co2 + n_o_h2o + n_o_meoh);

% Interfacial Tension
sigma = dae.db.get_sigma(df_sigma_T, params.T);

% Total moles in CSTR
n_cstr = n_g + n_aq + n_o;
n_cstr_h2 = n_g_h2 + n_aq_h2 + n_o_h2;
n_cstr_co2 = n_g_co2 + n_aq_co2 + n_o_co2;
n_cstr_oct = n_aq_oct + n_o_oct;
n_cstr_h2o = n_aq_h2o + n_o_h2o;
n_cstr_meoh = n_aq_meoh + n_o_meoh;

% Mole Fractions
x_g_h2 = dae.mole_fractions.mole_fraction(n_g_h2, n_g);
x_g_co2 = dae.mole_fractions.mole_fraction(n_g_co2, n_g);
x_g_oct = 0; % no octanol in gas phase
x_g_h2o = 0; % no water in gas phase
x_g_meoh = 0; % no MeOH in gas phase
x_aq_h2 = dae.mole_fractions.mole_fraction(n_aq_h2, n_aq);
x_aq_co2 = dae.mole_fractions.mole_fraction(n_aq_co2, n_aq);
x_aq_oct = dae.mole_fractions.mole_fraction(n_aq_oct, n_aq);
x_aq_h2o = dae.mole_fractions.mole_fraction(n_aq_h2o, n_aq);
x_aq_meoh = dae.mole_fractions.mole_fraction(n_aq_meoh, n_aq);
x_o_h2 = dae.mole_fractions.mole_fraction(n_o_h2, n_o);
x_o_co2 = dae.mole_fractions.mole_fraction(n_o_co2, n_o);
x_o_oct = dae.mole_fractions.mole_fraction(n_o_oct, n_o);
x_o_h2o = dae.mole_fractions.mole_fraction(n_o_h2o, n_o);
x_o_meoh = dae.mole_fractions.mole_fraction(n_o_meoh, n_o);
x_cstr_h2 = dae.mole_fractions.mole_fraction(n_cstr_h2, n_cstr);
x_cstr_co2 = dae.mole_fractions.mole_fraction(n_cstr_co2, n_cstr);
x_cstr_oct = dae.mole_fractions.mole_fraction(n_cstr_oct, n_cstr);
x_cstr_h2o = dae.mole_fractions.mole_fraction(n_cstr_h2o, n_cstr);
x_cstr_meoh = dae.mole_fractions.mole_fraction(n_cstr_meoh, n_cstr);

% Console Logging
% format short g;
% format long;
% curr_n = [n_g, n_aq, n_o]
% x_g = [x_g_h2, x_g_co2, x_g_oct, x_g_h2o, x_g_meoh]
% x_aq = [x_aq_h2, x_aq_co2, x_aq_oct, x_aq_h2o, x_aq_meoh]
% x_o = [x_o_h2, x_o_co2, x_o_oct, x_o_h2o, x_o_meoh]
% x_cstr = [x_cstr_h2, x_cstr_co2, x_cstr_oct, x_cstr_h2o, x_cstr_meoh]

% Averaged Molar Mass
M_g_avg = x_g_h2 * params.M_h2 + x_g_co2 * params.M_co2;
M_aq_avg = x_aq_h2 * params.M_h2 + x_aq_co2 * params.M_co2 ...
    + x_aq_oct * params.M_oct + x_aq_h2o * params.M_h2o + x_aq_meoh * params.M_meoh;
M_o_avg = x_o_h2 * params.M_h2 + x_o_co2 * params.M_co2 ...
    + x_o_oct * params.M_oct + x_o_h2o * params.M_h2o + x_o_meoh * params.M_meoh;


if kinetic_fit.isKineticFit
    % Solver for pressure computation
    p_solverinit = p; % current p is initial value
    lsqopt = optimset('Display','notify', ...
        'TolFun',1e-7);
    p = fminsearch(@(p)dae.iter_solver_p.delta_V(params.T, ...
        p, x_g_h2, x_g_co2, x_g_oct, x_g_h2o, ...
        x_g_meoh, x_aq_h2, x_aq_co2, x_aq_oct, ...
        x_aq_h2o, x_aq_meoh, x_o_h2, x_o_co2, ...
        x_o_oct, x_o_h2o, x_o_meoh, rho_gprMdls, ...
        n_g, n_aq, n_o, params.V_tot, dae), ...
        p_solverinit, lsqopt);

    % Molar Density by constant pressure given in parameters
    rho_M_g = dae.db.get_molar_density(params.T, p, ...
        x_g_h2, x_g_co2, x_g_oct, x_g_h2o, x_g_meoh, rho_gprMdls, state="vapor");
    rho_M_aq = dae.db.get_molar_density(params.T, p, ...
        x_aq_h2, x_aq_co2, x_aq_oct, x_aq_h2o, x_aq_meoh, rho_gprMdls, state="liquid_h2o");
    rho_M_o = dae.db.get_molar_density(params.T, p, ...
        x_o_h2, x_o_co2, x_o_oct, x_o_h2o, x_o_meoh, rho_gprMdls, state="liquid_oct");
else
    p_solverinit = params.p; % constant p is initial value
    lsqopt = optimset('Display','notify', ...
        'TolFun',1e-7);
    p_is = fminsearch(@(p)dae.iter_solver_p.delta_V(params.T, ...
        p, x_g_h2, x_g_co2, x_g_oct, x_g_h2o, ...
        x_g_meoh, x_aq_h2, x_aq_co2, x_aq_oct, ...
        x_aq_h2o, x_aq_meoh, x_o_h2, x_o_co2, ...
        x_o_oct, x_o_h2o, x_o_meoh, rho_gprMdls, ...
        n_g, n_aq, n_o, params.V_tot, dae), ...
        p_solverinit, lsqopt);

    % Molar Density by constant pressure given in parameters
    rho_M_g = dae.db.get_molar_density(params.T, params.p, ...
        x_g_h2, x_g_co2, x_g_oct, x_g_h2o, x_g_meoh, rho_gprMdls, state="vapor");
    rho_M_aq = dae.db.get_molar_density(params.T, params.p, ...
        x_aq_h2, x_aq_co2, x_aq_oct, x_aq_h2o, x_aq_meoh, rho_gprMdls, state="liquid_h2o");
    rho_M_o = dae.db.get_molar_density(params.T, params.p, ...
        x_o_h2, x_o_co2, x_o_oct, x_o_h2o, x_o_meoh, rho_gprMdls, state="liquid_oct");
end

% rho_M_g = dae.densities.molar_density(config.EOS, config.SAFTSUITE_MOLECULES, ...
%     params.T, params.p, x_g_h2, x_g_co2, x_g_oct, x_g_h2o, x_g_meoh, state="vapor");
% rho_M_aq = dae.densities.molar_density(config.EOS, config.SAFTSUITE_MOLECULES, ...
%     params.T, params.p, x_aq_h2, x_aq_co2, x_aq_oct, x_aq_h2o, x_aq_meoh, state="liquid");
% rho_M_o = dae.densities.molar_density(config.EOS, config.SAFTSUITE_MOLECULES, ...
%     params.T, params.p, x_o_h2, x_o_co2, x_o_oct, x_o_h2o, x_o_meoh, state="liquid");
rho_g = dae.densities.mass_density(rho_M_g, M_g_avg);
rho_aq = dae.densities.mass_density(rho_M_aq, M_aq_avg);
rho_o = dae.densities.mass_density(rho_M_o, M_o_avg);

% Pressure Check
% p_g = dae.densities.pressure(config.EOS, config.SAFTSUITE_MOLECULES, ...
%      params.T, rho_M_g, x_g_h2, x_g_co2, x_g_oct, x_g_h2o, x_g_meoh);
% p_aq = dae.densities.pressure(config.EOS, config.SAFTSUITE_MOLECULES, ...
%      params.T, rho_M_aq, x_aq_h2, x_aq_co2, x_aq_oct, x_aq_h2o,
%      x_aq_meoh);
% p_o = dae.densities.pressure(config.EOS, config.SAFTSUITE_MOLECULES, ...
%      params.T, rho_M_o, x_o_h2, x_o_co2, x_o_oct, x_o_h2o, x_o_meoh);

% Viscosities
if kinetic_fit.isKineticFit
    % Use optimized pressure
    [eta_h2, eta_co2, eta_h2o] = dae.db.get_eta_h2_co2_h2o(params.T, p, nist_viscosity_gprMdls);
else
    % Use constant pressure
    [eta_h2, eta_co2, eta_h2o] = dae.db.get_eta_h2_co2_h2o(params.T, params.p, nist_viscosity_gprMdls);
end
eta_oct = 0.2450 * 1e-3; % Missing xlsx data
eta_meoh = 0.1800 * 1e-3; % Missing xlsx data
eta_aq = dae.viscosity.eta_mixt(eta_h2, eta_co2, eta_oct, eta_h2o, eta_meoh, ...
    x_aq_h2, x_aq_co2, x_aq_oct, x_aq_h2o, x_aq_meoh);
eta_o = dae.viscosity.eta_mixt(eta_h2, eta_co2, eta_oct, eta_h2o, eta_meoh, ...
    x_o_h2, x_o_co2, x_o_oct, x_o_h2o, x_o_meoh);



% Volumes
V_aq = dae.volumes.volume(n_aq, rho_M_aq); % non-ideal mixture
V_o = dae.volumes.volume(n_o, rho_M_o); % non-ideal mixture
V_g = dae.volumes.volume(n_g, rho_M_g); % non-ideal mixture
% V_g = params.V_tot - (V_aq + V_o); % error is in the unimportant gas phase
if kinetic_fit.isKineticFit
    delta_V = params.V_tot - (V_aq + V_o + V_g);
end

V_liq = V_aq + V_o;
phi = V_o / V_liq; % Hold-Up or disperse volume fraction [-]

% Concentrations (not necessary for model but for insight in physical behaviour)
c_g_h2 = dae.concentrations.molar_concentration(n_g_h2, V_g);
c_g_co2 = dae.concentrations.molar_concentration(n_g_co2, V_g);
c_aq_h2 = dae.concentrations.molar_concentration(n_aq_h2, V_aq);
c_aq_co2 = dae.concentrations.molar_concentration(n_aq_co2, V_aq);
c_aq_oct = dae.concentrations.molar_concentration(n_aq_oct, V_aq);
c_aq_h2o = dae.concentrations.molar_concentration(n_aq_h2o, V_aq);
c_aq_meoh = dae.concentrations.molar_concentration(n_aq_meoh, V_aq);
c_o_h2 = dae.concentrations.molar_concentration(n_o_h2, V_o);
c_o_co2 = dae.concentrations.molar_concentration(n_o_co2, V_o);
c_o_oct = dae.concentrations.molar_concentration(n_o_oct, V_o);
c_o_h2o = dae.concentrations.molar_concentration(n_o_h2o, V_o);
c_o_meoh = dae.concentrations.molar_concentration(n_o_meoh, V_o);

% Liquid Level
H_liquid_0 = dae.liquid_level.level_at_rest(V_liq, params.T_vessel);

% Interfacial Areas and Sauter Diameter
S_a_gw = area_gl_interface(params.g, params.N_imp, params.d_imp, params.T_vessel, params.C_imp, H_liquid_0, params.alpha_imp, params.Xi_c);
[S_a_wo, d_32] = area_ll_interface(params.d_imp, params.N_imp, V_o, V_aq, rho_aq, sigma);

% Reaction
if kinetic_fit.isKineticFit
    k_fw = kinetic_fit.k_fw;
    K_R = params.K_R;
else
    k_fw = params.k_fw;
    K_R = params.K_R;
end
R_0_V = dae.reaction_kinetics.volumetric_reaction_rate(k_fw, K_R, c_o_h2, c_o_co2, c_o_meoh, c_o_h2o);

% Distribution Coefficients
if kinetic_fit.isKineticFit
    [K_gw_h2, K_gw_co2, K_wo_h2, K_wo_co2, K_wo_oct, K_wo_h2o, K_wo_meoh] ...
        = dae.db.get_K_i(params.T, p, x_cstr_h2, x_cstr_co2, x_cstr_oct, x_cstr_h2o, x_cstr_meoh, K_gprMdls);
else
    [K_gw_h2, K_gw_co2, K_wo_h2, K_wo_co2, K_wo_oct, K_wo_h2o, K_wo_meoh] ...
        = dae.db.get_K_i(params.T, params.p, x_cstr_h2, x_cstr_co2, x_cstr_oct, x_cstr_h2o, x_cstr_meoh, K_gprMdls);
end

% Diffusion
D_diff_h2_in_h2o = dae.diffusion.diff_coefficient(params.x_s_h2o, params.M_h2o, params.T, eta_h2o, params.V_b_h2);
D_diff_co2_in_h2o = dae.diffusion.diff_coefficient(params.x_s_h2o, params.M_h2o, params.T, eta_h2o, params.V_b_co2);
D_diff_oct_in_h2o = dae.diffusion.diff_coefficient(params.x_s_h2o, params.M_h2o, params.T, eta_h2o, params.V_b_oct);
D_diff_meoh_in_h2o = dae.diffusion.diff_coefficient(params.x_s_h2o, params.M_h2o, params.T, eta_h2o, params.V_b_meoh);
D_diff_h2_in_oct = dae.diffusion.diff_coefficient(params.x_s_oct, params.M_oct, params.T, eta_oct, params.V_b_h2);
D_diff_co2_in_oct = dae.diffusion.diff_coefficient(params.x_s_oct, params.M_oct, params.T, eta_oct, params.V_b_co2);
D_diff_h2o_in_oct = dae.diffusion.diff_coefficient(params.x_s_oct, params.M_oct, params.T, eta_oct, params.V_b_h2o);
D_diff_meoh_in_oct = dae.diffusion.diff_coefficient(params.x_s_oct, params.M_oct, params.T, eta_oct, params.V_b_meoh);
k_gw_aq_h2 = dae.diffusion.k_L(eta_aq, rho_aq, D_diff_h2_in_h2o, params.C_kL_imp, params.N_imp, params.d_imp, params.T_vessel, H_liquid_0);
k_gw_aq_co2 = dae.diffusion.k_L(eta_aq, rho_aq, D_diff_co2_in_h2o, params.C_kL_imp, params.N_imp, params.d_imp, params.T_vessel, H_liquid_0);
k_gw_g_h2 = inf; % Assumption: no gas-side resistence
k_gw_g_co2 = inf; % Assumption: no gas-side resistence
k_wo_o_h2 = dae.diffusion.k_d(D_diff_h2_in_oct, d_32);
k_wo_o_co2 = dae.diffusion.k_d(D_diff_co2_in_oct, d_32);
k_wo_o_oct = inf; % Octanol in Octanol has no resistance
k_wo_o_h2o = dae.diffusion.k_d(D_diff_h2o_in_oct, d_32);
k_wo_o_meoh = dae.diffusion.k_d(D_diff_meoh_in_oct, d_32);
k_wo_aq_h2 = dae.diffusion.k_c(eta_aq, rho_aq, D_diff_h2_in_h2o, params.d_imp, params.N_imp, params.g, rho_o, sigma, params.T_vessel, phi, d_32);
k_wo_aq_co2 = dae.diffusion.k_c(eta_aq, rho_aq, D_diff_co2_in_h2o, params.d_imp, params.N_imp, params.g, rho_o, sigma, params.T_vessel, phi, d_32);
k_wo_aq_h2o = inf; % Water in Water has no resistance
k_wo_aq_oct = dae.diffusion.k_c(eta_aq, rho_aq, D_diff_oct_in_h2o, params.d_imp, params.N_imp, params.g, rho_o, sigma, params.T_vessel, phi, d_32);
k_wo_aq_meoh = dae.diffusion.k_c(eta_aq, rho_aq, D_diff_meoh_in_h2o, params.d_imp, params.N_imp, params.g, rho_o, sigma, params.T_vessel, phi, d_32);
beta_gw_h2 = dae.diffusion.beta(k_gw_g_h2, k_gw_aq_h2, K_gw_h2);
beta_gw_co2 = dae.diffusion.beta(k_gw_g_co2, k_gw_aq_co2, K_gw_co2);
%beta_gw_h2 = 1e-8;
%beta_gw_co2 = 1e-8;
beta_wo_h2 = dae.diffusion.beta(k_wo_o_h2, k_wo_aq_h2, K_wo_h2);
beta_wo_co2 = dae.diffusion.beta(k_wo_o_co2, k_wo_aq_co2, K_wo_co2);
beta_wo_oct = dae.diffusion.beta(k_wo_o_oct, k_wo_aq_oct, K_wo_oct);
beta_wo_h2o = dae.diffusion.beta(k_wo_o_h2o, k_wo_aq_h2o, K_wo_h2o);
beta_wo_meoh = dae.diffusion.beta(k_wo_o_meoh, k_wo_aq_meoh, K_wo_meoh);
% %% Limitation of Diffusion
% beta_gw_h2 = beta_gw_h2 * 50;
% beta_gw_co2 = beta_gw_co2 * 50;
% beta_wo_h2 = beta_wo_h2 * 50;
% beta_wo_co2 = beta_wo_co2 * 50;
% beta_wo_oct = beta_wo_oct * 50;
% beta_wo_h2o = beta_wo_h2o * 50;
% beta_wo_meoh = beta_wo_meoh * 50;
n_dot_a_gw_h2 = dae.diffusion.diff_flux(beta_gw_h2, c_aq_h2, c_g_h2, K_gw_h2);
n_dot_a_gw_co2 = dae.diffusion.diff_flux(beta_gw_co2, c_aq_co2, c_g_co2, K_gw_co2);
n_dot_a_wo_h2 = dae.diffusion.diff_flux(beta_wo_h2, c_aq_h2, c_o_h2, K_wo_h2);
n_dot_a_wo_co2 = dae.diffusion.diff_flux(beta_wo_co2, c_aq_co2, c_o_co2, K_wo_co2);
n_dot_a_wo_oct = dae.diffusion.diff_flux(beta_wo_oct, c_aq_oct, c_o_oct, K_wo_oct);
n_dot_a_wo_h2o = dae.diffusion.diff_flux(beta_wo_h2o, c_aq_h2o, c_o_h2o, K_wo_h2o);
n_dot_a_wo_meoh = dae.diffusion.diff_flux(beta_wo_meoh, c_aq_meoh, c_o_meoh, K_wo_meoh);

if kinetic_fit.isKineticFit
    % Reactor is a Batch
    N_dot_g_in = 0;
    N_dot_aq_in = 0;
    N_dot_o_in = 0;

    N_dot_g_out = 0;
    N_dot_aq_out = 0;
    N_dot_o_out = 0;

elseif ~kinetic_fit.isKineticFit
    if CSTR_sets.withReflux
        % Save values of previous step for decanter dead-time delayed input flows
        persistent t_preval
        persistent n_o_preval
        persistent n_o_h2_preval
        persistent n_o_co2_preval
        persistent n_o_h2o_preval
        persistent n_o_meoh_preval

        
        
        % First time-step
        if isempty(t_preval) 
            t_preval(1,1) = 0;
            n_o_preval(1,1) = 0;
            n_o_h2_preval(1,1) = 0;
            n_o_co2_preval(1,1) = 0;
            n_o_h2o_preval(1,1) = 0;
            n_o_meoh_preval(1,1) = 0;
        end
    
        % Set input flows with reflux
        if t > params.decanter_t_dead
    
            % Find time point older than decanter dead-time
            [idx, ~] = find(t_preval < (t - params.decanter_t_dead), 1, 'last'); 
    
            % Mole Fractions of prevals
            x_o_h2_preval = n_o_h2_preval(idx, 1) / n_o_preval(idx, 1);
            x_o_co2_preval = n_o_co2_preval(idx, 1) / n_o_preval(idx, 1);
            x_o_h2o_preval = n_o_h2o_preval(idx, 1) / n_o_preval(idx, 1);
            x_o_meoh_preval = n_o_meoh_preval(idx, 1) / n_o_preval(idx, 1);
    
            % Interpolation for mole fractions of reflux at t - params.decanter_t_dead
            x_interp_h2 =  x_o_h2_preval + ...
                (x_o_h2 - x_o_h2_preval)/(t-t_preval(idx,1))*((t-params.decanter_t_dead) - t_preval(idx,1));
            x_interp_co2 =  x_o_co2_preval + ...
                (x_o_co2 - x_o_co2_preval)/(t-t_preval(idx,1))*((t-params.decanter_t_dead) - t_preval(idx,1));
            x_interp_h2o =  x_o_h2o_preval + ...
                (x_o_h2o - x_o_h2o_preval)/(t-t_preval(idx,1))*((t-params.decanter_t_dead) - t_preval(idx,1));
            x_interp_meoh =  x_o_meoh_preval + ...
                (x_o_meoh - x_o_meoh_preval)/(t-t_preval(idx,1))*((t-params.decanter_t_dead) - t_preval(idx,1));
    
            % Set input mole fractions of organic phase
            inputs.x_o_h2_in = ...
                ((1-params.V_dot_o_in_Recycle_Fraction) * params.V_dot_o_in_Feed_Fraction) ...
                * inputs.x_o_h2_in_P2 + ...
                params.V_dot_o_in_Recycle_Fraction * x_interp_h2;  
            inputs.x_o_co2_in = ...
                ((1-params.V_dot_o_in_Recycle_Fraction) * params.V_dot_o_in_Feed_Fraction) ...
                * inputs.x_o_co2_in_P2 + ...
                params.V_dot_o_in_Recycle_Fraction * x_interp_co2;
            inputs.x_o_h2o_in = ...
                ((1-params.V_dot_o_in_Recycle_Fraction) * params.V_dot_o_in_Feed_Fraction) ...
                * inputs.x_o_h2o_in_P2 + ...
                params.V_dot_o_in_Recycle_Fraction * x_interp_h2o;
            inputs.x_o_meoh_in = ...
                ((1-params.V_dot_o_in_Recycle_Fraction) * params.V_dot_o_in_Feed_Fraction) ...
                * inputs.x_o_meoh_in_P2 + ...
                params.V_dot_o_in_Recycle_Fraction * x_interp_meoh;
            inputs.x_o_oct_in = 1 - (inputs.x_o_h2_in + inputs.x_o_co2_in + inputs.x_o_h2o_in + inputs.x_o_meoh_in);

            

        else
            % No mixing in V-21 with reflux of P-3 - feed flow from Pump P-2 only
            inputs.x_o_h2_in = inputs.x_o_h2_in_P2;
            inputs.x_o_co2_in = inputs.x_o_co2_in_P2;
            inputs.x_o_h2o_in = inputs.x_o_h2o_in_P2;
            inputs.x_o_meoh_in = inputs.x_o_meoh_in_P2;
            inputs.x_o_oct_in = 1 - (inputs.x_o_h2_in + inputs.x_o_co2_in + inputs.x_o_h2o_in + inputs.x_o_meoh_in);
        end

    elseif ~CSTR_sets.withReflux
        % Simple CSTR process without any reflux
        % No mixing in V-21 with reflux of P-3 - feed flow from Pump P-2 only
        inputs.x_o_h2_in = inputs.x_o_h2_in_P2;
        inputs.x_o_co2_in = inputs.x_o_co2_in_P2;
        inputs.x_o_h2o_in = inputs.x_o_h2o_in_P2;
        inputs.x_o_meoh_in = inputs.x_o_meoh_in_P2;
        inputs.x_o_oct_in = 1 - (inputs.x_o_h2_in + inputs.x_o_co2_in + inputs.x_o_h2o_in + inputs.x_o_meoh_in);

    end
    
    % recalculate rho_m_o_in
    inputs.rho_M_o_in = dae.db.get_molar_density(params.T, params.p, ...
        inputs.x_o_h2_in, inputs.x_o_co2_in, inputs.x_o_oct_in, inputs.x_o_h2o_in, inputs.x_o_meoh_in, rho_gprMdls, state="liquid_oct");

    % No matter of reflux - Controlled input flows of gas
    % Transform volume flows in mole flows
    N_dot_aq_in = inputs.rho_M_aq_in * params.V_dot_aq_in;
    N_dot_o_in = inputs.rho_M_o_in * params.V_dot_o_in;
    
    
    % Outlet volume flows CSTR to ensure positive moles in Phases
    d_V_g = inits.V_g_0 - V_g;
    d_V_aq = inits.V_aq_0 - V_aq;
    d_V_o = inits.V_o_0 - V_o;

    % d_V equals delta_V/delta_t since correction of volume to V_init
    % in every solver step --> d_V [m^3/s]
    % Input gas volume flow as control valve
    V_dot_g_in = d_V_g;

    % Check upper boundary
    if V_dot_g_in >  params.V_dot_g_in_MAX
        V_dot_g_in = params.V_dot_g_in_MAX;
    end

    % Check H2-CO2 Ratio
    reached_max_co2_limit = ...
        (V_dot_g_in * (1-params.h2_input_vapor_fraction) > params.V_dot_g_in_P_4_MAX_VAP);
    
    % If gas volume is lower than initial volume
    if d_V_g < 0
        % Input Valve closes
        V_dot_g_in = 0; 

        % Pressure Valve opens
        V_dot_g_out = d_V_g; 
    else
        % Pressure Valve remains closed
        V_dot_g_out = 0;
    end
    
    % Set molar flows
    N_dot_g_in = inputs.rho_M_g_in * V_dot_g_in;
    N_dot_g_out = rho_M_g * V_dot_g_out;

    % Liquid volumina in CSTR shall be constant
    V_dot_aq_out = params.V_dot_aq_in - d_V_aq;
    V_dot_o_out = params.V_dot_o_in - d_V_o;
    N_dot_aq_out = rho_M_aq * V_dot_aq_out;
    N_dot_o_out = rho_M_o * V_dot_o_out;
    
    % Catch errors
    if (V_dot_aq_out < 0 || V_dot_o_out < 0) && t > 5
        ME = MException('MATLAB:physicallyWrong', ...
        'Volume outflows are negative.', V_dot_aq_out, V_dot_o_out);
        throw(ME)
    end
    
    if t > 10
        if std(solver_res_alg_var.t(end-50:end)) < 0.005
            ME = MException('MATLAB:endlessIteration', ...
            'Iteration stops due to endless Iteration of time step: Might have physical background.');
            throw(ME)
        end
    end
    
    if CSTR_sets.withReflux
        if t > params.decanter_t_dead
            V_dot_purge = V_dot_o_out - ...
                params.V_dot_o_in_Recycle_Fraction * params.V_dot_o_in;
        else
            V_dot_purge = 0;
        end
    end
end

%% Differential Equations

% Timestep
t;

% Gaseous Phase
n_g = N_dot_g_in - N_dot_g_out + S_a_gw * (n_dot_a_gw_h2 + n_dot_a_gw_co2);
n_g_h2 = N_dot_g_in * inputs.x_g_h2_in - x_g_h2 * N_dot_g_out + S_a_gw * n_dot_a_gw_h2;

% Aqeuous Phase
n_aq = N_dot_aq_in - N_dot_aq_out + S_a_gw * (-n_dot_a_gw_h2 + (-n_dot_a_gw_co2)) ...
+ S_a_wo * ((-n_dot_a_wo_h2) + (-n_dot_a_wo_co2) + (-n_dot_a_wo_h2o) + (-n_dot_a_wo_oct) + (-n_dot_a_wo_meoh));
n_aq_h2 = -x_aq_h2 * N_dot_aq_out + S_a_gw * (-n_dot_a_gw_h2) + S_a_wo * (-n_dot_a_wo_h2);
n_aq_co2 = -x_aq_co2 * N_dot_aq_out + S_a_gw * (-n_dot_a_gw_co2) + S_a_wo * (-n_dot_a_wo_co2);
n_aq_oct = -x_aq_oct * N_dot_aq_out + S_a_wo * (-n_dot_a_wo_oct);
n_aq_meoh = -x_aq_meoh * N_dot_aq_out + S_a_wo * (-n_dot_a_wo_meoh);

% Organic Phase
n_o = N_dot_o_in - N_dot_o_out + S_a_wo ...
* ((n_dot_a_wo_h2) + (n_dot_a_wo_co2) + n_dot_a_wo_h2o + n_dot_a_wo_oct + n_dot_a_wo_meoh) ...
+ V_o * R_0_V * (params.nu_h2 + params.nu_co2 + params.nu_h2o + params.nu_meoh);
n_o_h2 = N_dot_o_in * inputs.x_o_h2_in - x_o_h2 * N_dot_o_out ...
+ S_a_wo * (n_dot_a_wo_h2) + V_o * params.nu_h2 * R_0_V;
n_o_co2 = N_dot_o_in * inputs.x_o_co2_in - x_o_co2 * N_dot_o_out ...
+ S_a_wo * (n_dot_a_wo_co2) + V_o * params.nu_co2 * R_0_V;
n_o_h2o = N_dot_o_in * inputs.x_o_h2o_in - x_o_h2o * N_dot_o_out ...
+ S_a_wo * n_dot_a_wo_h2o + V_o * params.nu_h2o * R_0_V;
n_o_meoh = N_dot_o_in * inputs.x_o_meoh_in - x_o_meoh * N_dot_o_out ...
+ S_a_wo * n_dot_a_wo_meoh + V_o * params.nu_meoh * R_0_V;


if kinetic_fit.isKineticFit

    % Algebraic equation for pressure (p_dot is set to zero in mass matrix)
    p_old;
    p_dot = p - p_old;

    % Set Differential Variables
    dy = [n_g; n_g_h2; n_aq; n_aq_h2; n_aq_co2; n_aq_oct; n_aq_meoh; ...
        n_o; n_o_h2; n_o_co2; n_o_h2o; n_o_meoh; p_dot];
else

    % Set Differential Variables
    dy = [n_g; n_g_h2; n_aq; n_aq_h2; n_aq_co2; n_aq_oct; n_aq_meoh; ...
    n_o; n_o_h2; n_o_co2; n_o_h2o; n_o_meoh];

    if CSTR_sets.withReflux
        % Save previous step
        t_preval(end+1, 1) = t;
        n_o_preval(end+1, 1) = y(8);
        n_o_h2_preval(end+1, 1) = y(9);
        n_o_co2_preval(end+1, 1) = y(10);
        n_o_h2o_preval(end+1, 1) = y(11);
        n_o_meoh_preval(end+1, 1) = y(12);
    end

    % Save algebraic variables as output
        solver_res_alg_var.t(end+1,1) = t;
        solver_res_alg_var.N_dot_g_out(end+1,1) = N_dot_g_out;
        solver_res_alg_var.N_dot_aq_out(end+1,1) = N_dot_aq_out;
        solver_res_alg_var.N_dot_o_out(end+1,1) = N_dot_o_out;
        solver_res_alg_var.N_dot_g_in(end+1,1) = N_dot_g_in;
        solver_res_alg_var.N_dot_aq_in(end+1,1) = N_dot_aq_in;
        solver_res_alg_var.N_dot_o_in(end+1,1) = N_dot_o_in;
        solver_res_alg_var.M_g_avg(end+1,1) = M_g_avg;
        solver_res_alg_var.M_aq_avg(end+1,1) = M_aq_avg;
        solver_res_alg_var.M_o_avg(end+1,1) = M_o_avg;
        solver_res_alg_var.M_o_avg_in(end+1,1) = (inputs.x_o_h2_in * params.M_h2 + ...
            inputs.x_o_co2_in * params.M_co2 + ...
            (1 - inputs.x_o_h2_in - inputs.x_o_co2_in - inputs.x_o_h2o_in - inputs.x_o_meoh_in) * params.M_oct ...
            + inputs.x_o_h2o_in * params.M_h2o + ...
            inputs.x_o_meoh_in * params.M_meoh);
        solver_res_alg_var.x_o_h2_in(end+1,1) = inputs.x_o_h2_in;
        solver_res_alg_var.x_o_co2_in(end+1,1) = inputs.x_o_co2_in;
        solver_res_alg_var.x_o_oct_in(end+1,1) = inputs.x_o_oct_in;
        solver_res_alg_var.x_o_h2o_in(end+1,1) = inputs.x_o_h2o_in;
        solver_res_alg_var.x_o_meoh_in(end+1,1) = inputs.x_o_meoh_in;
        solver_res_alg_var.V_dot_g_in(end+1,1) = V_dot_g_in;
        solver_res_alg_var.V_dot_g_out(end+1,1) = V_dot_g_out;
        solver_res_alg_var.V_dot_aq_out(end+1,1) = V_dot_aq_out;
        solver_res_alg_var.V_dot_o_out(end+1,1) = V_dot_o_out;
        solver_res_alg_var.V_dot_purge(end+1,1) = V_dot_purge;
        solver_res_alg_var.V_g(end+1,1) = V_g;
        solver_res_alg_var.V_aq(end+1,1) = V_aq;
        solver_res_alg_var.V_o(end+1,1) = V_o;
        solver_res_alg_var.p_is(end+1,1) = p_is;
        solver_res_alg_var.n_o_meoh_reaction(end+1,1) = V_o * params.nu_meoh * R_0_V;
        solver_res_alg_var.reached_max_co2_limit(end+1,1) = reached_max_co2_limit;
        solver_res_alg_var.rho_M_g(end+1,1) = rho_M_g;
        solver_res_alg_var.rho_M_aq(end+1,1) = rho_M_aq;
        solver_res_alg_var.rho_M_o(end+1,1) = rho_M_o;
        solver_res_alg_var.rho_M_o_in(end+1,1) = inputs.rho_M_o_in;
end

if any(~isfinite(dy))
    error("Cannot solve anymore: Inf in system!")
end

if any(isnan(dy))
    error("Cannot solve anymore: NaN in system!")
end

if ~kinetic_fit.isKineticFit
    if t > 0.95 * CSTR_sets.t_end
        % Save algebraic variable process to base workspace
        save (config.OUTPUT_PATH + "/solver_res_alg_var.mat", 'solver_res_alg_var');
    end
end

%file_logger.info('... Finished dae_system');

end