%% Evaluation of optimisation
clear;
clc;
close all;

%% User inputs
% Evaluation folder with 'output' as root folder
% Existing evaluation excel sheet will be deleted, if not renamed
evalFolder = '/Optimisation/finalOptimH2Ratio';

%% Get folder content
config = config();
evalFolderPath = strcat(config.OUTPUT_PATH, evalFolder);
dirList = dir(evalFolderPath);

% delete existing evaluation excel sheet
if strcmp(dirList(end).name,'evaluation-of-optimisation.xlsx')
    delete(strcat(evalFolderPath,"/evaluation-of-optimisation.xlsx"));
    dirList = dir(evalFolderPath);
end

%% Loop through excel data to get desired values
for i=3:size(dirList,1)
    excelPath = strcat(evalFolderPath,"/",dirList(i).name,"/solver_result_",dirList(i).name,".xlsx");
    evalT(i-2,:) = readtable(excelPath,'Sheet',3);
end

%% Write results in new excel
evalExcel = strcat(evalFolderPath,"/evaluation-of-optimisation.xlsx");
writetable(evalT,evalExcel);

%% Finalize script
dispout = strcat("Script is finished. Find the results in output",evalFolder);
disp(dispout);

