function optim = optim_parameters()
%% read me
% first: optimize T, p, phi_liq_o and N_imp
% second: optimize the remaining 4 variables
% else the doe matrix gets very large, which leads to very high computational
% times! 
%
% for main CSTR i.e. optim.T is used instead of optim.T_range

%% Setup of return as structure
optim = struct();

%% Operating Conditions - Temperature
optim.T = 393.15;                                       % [K]
% Optimization values for main_Optimization
optim.T_range = [393.15 403.15 413.15];
optim.T_range_sensAn = [383.15 393.15 403.15];

%% Operating Conditions - Pressure
optim.p = 110 * 1e5;                                % [Pa]
% Optimization values for main_Optimization
optim.p_range = [100e5 105e5 110e5];
optim.p_range_finalOptim = [70e5 80e5 90e5 95e5 100e5 105e5 110e5];
optim.p_range_sensAn = [60e5 80e5 100e5 120e5 140e5];

%% Transform liquid CO2 flow into vapour CO2 flow for handy model
optim.h2_input_vapor_fraction = 0.4;                % [mol/mol] Mole Fraction of H2 in vapor phase input
% Optimization values for main_Optimization
optim.h2_input_vapor_fraction_range = [0.3 0.35 0.4 0.45 0.5];
optim.h2_input_vapor_fraction_range_finalOptim = [0.1 0.2 0.3 0.32 0.34 0.36 0.38 0.4 0.45 0.5 0.55 0.6 0.7 0.8 0.9];
optim.h2_input_vapor_fraction_range_sensAn = [0.1 0.3 0.5 0.7 0.9];

%% Product Phase
optim.V_dot_aq_in_fraction = 0.12;                   % [-] fraction of maximal feed flow
%optim.V_dot_aq_in_fraction = 0.1;                   % [-] fraction of maximal feed flow
% Optimization values for main_Optimization
optim.V_dot_aq_in_fraction_range = [0.12 0.15 0.18];
optim.V_dot_aq_in_fraction_range_finalOptim = [0.11 0.12 0.13 0.14 0.15 0.16 0.17 0.18 0.19 0.2 0.21 0.23 0.27];
optim.V_dot_aq_in_fraction_range_sensAn = [0.12 0.15 0.18];

%% phase ratio
optim.phi_liq_o = 0.55;                       % phase fraction organic in liquid phase [-]
% Optimization values for main_Optimization
optim.phi_liq_o_range = [0.45 0.47 0.49 0.51 0.53 0.55];
optim.phi_liq_o_range_sensAn = [0.45 0.5 0.55];

%% impeller rotational speed (s^-1)
optim.N_imp = 25;                          % impeller rotational speed [1/s]
% Optimization values for main_Optimization
optim.N_imp_range_sensAn = [16 20 24];

%% Create DoE
% Set substruct for DoE
subStructDoE = struct();
subStructDoE.T = optim.T_range;
subStructDoE.p = optim.p_range;
subStructDoE.h2_input_vapor_fraction = optim.h2_input_vapor_fraction_range;
subStructDoE.V_dot_aq_in_fraction = optim.V_dot_aq_in_fraction_range;
subStructDoE.phi_liq_o = optim.phi_liq_o_range;
subStructDoE_names = fieldnames(subStructDoE);

% Substruct for sensitivity analysis
subStructSA = struct();
subStructSA.T = optim.T_range_sensAn;
subStructSA.p = optim.p_range_sensAn;
subStructSA.h2_input_vapor_fraction = optim.h2_input_vapor_fraction_range_sensAn;
subStructSA.V_dot_aq_in_fraction = optim.V_dot_aq_in_fraction_range_sensAn;
subStructSA.phi_liq_o = optim.phi_liq_o_range_sensAn;
subStructSA.N_imp = optim.N_imp_range_sensAn;
subStructSA_names = fieldnames(subStructSA);

% Set substruct for finalOptim
subStructFinalOptim = struct();
subStructFinalOptim.h2_input_vapor_fraction_range_finalOptim = optim.h2_input_vapor_fraction_range_finalOptim;
subStructFinalOptim.phi_liq_o = optim.phi_liq_o_range;
subStructFinalOptim_names = fieldnames(subStructFinalOptim);

% sensitivity analysis for optimization space
% with [T p h2_input_vapor_fraction V_dot_aq_in_fraction V_dot_o_in_Feed_Fraction V_dot_o_in_Recycle_Fraction]
% first line is benchmark setting

% Define sensitivity analysis
sensitivityAnalysis = [2 3 3 2 2 2; ...
                       1 3 3 2 2 2; ...
                       3 3 3 2 2 2; ...
                       2 1 3 2 2 2; ...
                       2 2 3 2 2 2; ...
                       2 4 3 2 2 2; ...
                       2 5 3 2 2 2; ...
                       2 3 1 2 2 2; ...
                       2 3 2 2 2 2; ...
                       2 3 4 2 2 2; ...
                       2 3 5 2 2 2; ...
                       2 3 3 1 2 2; ...
                       2 3 3 3 2 2; ...
                       2 3 3 2 1 2; ...
                       2 3 3 2 3 2; ...
                       2 3 3 2 2 1; ...
                       2 3 3 2 2 3];
sensitivityAnalysisSettings = [size(optim.T_range_sensAn,2) size(optim.p_range_sensAn,2) size(optim.h2_input_vapor_fraction_range_sensAn,2) ...
    size(optim.V_dot_aq_in_fraction_range_sensAn,2) size(optim.phi_liq_o_range_sensAn,2) size(optim.N_imp_range_sensAn,2)];

% Define doe
doeSettings = [size(optim.T_range,2) size(optim.p_range,2) size(optim.h2_input_vapor_fraction_range,2) ...
    size(optim.V_dot_aq_in_fraction_range,2) size(optim.phi_liq_o_range,2)];
doe = fullfact(doeSettings);

% H2 fraction final optimisation
finalOptim = [1 6 ; ...
                2 6 ; ...
                3 6 ; ...
                4 6 ; ...
                5 6 ; ...
                6 6 ; ...               
                7 6 ; ...
                8 6 ; ...
                9 6 ; ...
                10 6 ; ...
                11 6 ; ...
                12 6 ; ...
                13 6 ; ...
                14 6 ; ...
                15 6 ];

finalOptimSettings = [size(optim.h2_input_vapor_fraction_range_finalOptim,2) ...
    size(optim.phi_liq_o_range,2)];


% Write matrices for DoE and sensitivity analysis respectively
for i=1:size(doeSettings,2)
    for j=1:doeSettings(i)
        idx = ismember(doe(:,i),j);
        optim.doe(idx,i) = subStructDoE.(subStructDoE_names{i})(j);
    end
end
clear idx

for i=1:size(sensitivityAnalysisSettings,2)
    for j=1:sensitivityAnalysisSettings(i)
        idx = ismember(sensitivityAnalysis(:,i),j);
        optim.sensitivityAnalysis(idx,i) = subStructSA.(subStructSA_names{i})(j);
    end
end
clear idx

for i=1:size(finalOptimSettings,2)
    for j=1:finalOptimSettings(i)
        idx = ismember(finalOptim(:,i),j);
        optim.finalOptim(idx,i) = subStructFinalOptim.(subStructFinalOptim_names{i})(j);
    end
end
clear idx

% disp('please stop');


